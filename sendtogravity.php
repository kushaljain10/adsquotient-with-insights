<?php
$name = $_GET["Name"];
if (preg_match("/^[a-zA-Z ]*$/",$name)) 
{
	require('wp-config.php');
	//API for gravity form
	$api_key = '85917af7f6';
	$private_key = 'e46441357e29ccb';

	//set route
	$route = 'forms/1/entries';

	function calculate_signature( $string, $private_key ) {
	$hash = hash_hmac( 'sha1', $string, $private_key, true );
	$sig = rawurlencode( base64_encode( $hash ) );
	return $sig;
	}


	//creating request URL
	$expires = strtotime( '+60 mins' );
	$string_to_sign = sprintf( '%s:%s:%s:%s', $api_key, 'POST', $route, $expires );
	$sig = calculate_signature( $string_to_sign, $private_key );
	$url = get_site_url() . '/gravityformsapi/' . $route . '?api_key=' . $api_key . '&signature=' . $sig . '&expires=' . $expires;

	$entries = array(
	array(
		'date_created' => date('Y-m-d H:i:s'),
		'is_starred'   => 0,
		'is_read'      => 0,
		'ip'           => '::1',
		'source_url'   => $_GET['URL'],
		'currency'     => 'USD',
		'created_by'   => 1,
		'user_agent'   => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
		'status'       => 'active',
		'1'            => $_GET['Name'],
		'3'            => $_GET['Phone'],
		'2'            => $_GET['Email'],
		'4'            => $_GET['URL']
		)
	);

	//json encode array
	$entry_json = json_encode( $entries );

	//retrieve data
	$response = wp_remote_request( $url , array( 'method' => 'POST', 'body' => $entry_json, 'timeout' => 25 ) );
	/*if ( wp_remote_retrieve_response_code( $response ) != 200 || ( empty( wp_remote_retrieve_body( $response ) ) ) ){
	//http request failed
	die( 'There was an error attempting to access the API.' );
	}*/
	if ( (wp_remote_retrieve_response_code( $response ) != 200) || (  wp_remote_retrieve_body( $response ) =="" ) ){
	//http request failed
	/*echo "<br>wp_remote_retrieve_response_code :".wp_remote_retrieve_response_code( $response );
	echo "<br>wp_remote_retrieve_body :";
	print_r(wp_remote_retrieve_body( $response )); */
	die( 'There was an error attempting to access the API.' );

	}
	
	$name = $_GET['Name'];
	$phone = $_GET['Phone'];
	$email = $_GET['Email'];
	$source_url = $_GET['URL'];
	$today_time = date("M,d,Y h:i:s A");
	


    // Put all the form fields (names and values) in this array
    $body = array('Name' => $name, 'Phone' => $phone, 'Email' => $email, 'Source URL' => $source_url, 'Date of Enquiry' => $today_time);

    // Send the data to Google Spreadsheet via HTTP POST request
    $request = new WP_Http();
    $response = $request->request($post_url, array('method' => 'POST', 'sslverify' => false, 'body' => $body));

	//result is in the response "body" and is json encoded.
	$body = json_decode( wp_remote_retrieve_body( $response ), true );

	
	//Gravity form mail Notifications

	// Multiple recipients
	$to = 'suneil@socialbeat.in,rachna.ganatra@socialbeat.in,velmurugan@socialbeat.in';

	// Subject
	$subject = 'Ads Quotient Enquiry';
	// Message
	$message = '<html>
	<head>
	<title>Finolex - Enquiry</title>
	</head>
	<body>
	<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
	<tr><td bgcolor="#EAF2FA"><strong> Name:</strong></td></tr>
	<tr bgcolor="#FFFFFF">'.$name.'</td></tr>
	 <tr bgcolor="#EAF2FA"><td><strong>Phone Number:</strong></td></tr>
	<tr bgcolor="#FFFFFF"><td>'.$phone.'</td></tr>
	<tr bgcolor="#EAF2FA"><td><strong>Email :</strong></td></tr>
	<tr bgcolor="#FFFFFF">'.$email.'</td></tr>
	<tr bgcolor="#EAF2FA"><td><strong>Source Lead:</strong></td></tr> 
	<tr bgcolor="#FFFFFF"><td>'.$source_url.'</td></tr> 
	</table>
	</body>
	</html>';

	// To send HTML mail, the Content-type header must be set
	$headers .= "From: Adsquotient <from@adsquotient.com>\r\n"; 
	$headers .= "Return-Path: Adsquotient <noreply@adsquotient.com>\r\n";
	$headers .= "Reply-To: Adsquotient <replyto@adsquotient.com>\r\n";
	$headers .= "MIME-Version: 1.0 \r\n";
	$headers .= "Organization: Advertising \r\n"; 
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion()."\n";


	// Mail it
	wp_mail($to, $subject, $message, $headers);

	//End of gravity mail notifications




	//print_r($body); exit;
	if( $body['status'] > 202 ){
	$error = $body['response'];

		//entry update failed, get error information, error could just be a string
	if ( is_array( $error )){
		$error_code     = $error['code'];
		$error_message  = $error['message'];
		$error_data     = isset( $error['data'] ) ? $error['data'] : '';
		$status     = "Code: {$error_code}. Message: {$error_message}. Data: {$error_data}.";
	}
	else{
		$status = $error;
	}
	die( "Could not post entries. {$status}" );
	}
	else
	{
	header('Location:https://www.adsquotient.com/thankyou/');
	}
}