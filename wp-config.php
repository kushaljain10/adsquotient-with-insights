<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'production_quotient' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', "$7&#!ial@326Quoti" );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define( 'FS_METHOD', 'direct' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'jkoxyc7centdp8zylpdnqdpcyt6pal5g339ufbrbt0gjanftfaiyqcslh92hbwiv' );
define( 'SECURE_AUTH_KEY',  'mmpuhxjt6qeq47kbycacveqftpijphomitwkmmxw7k1jw3ujucpokrhfuptwnkkc' );
define( 'LOGGED_IN_KEY',    'vguz9jdtcaal529aslxmjxhxvu9iavpio9doma4roqkjthrslbrnr6nmxmrmgaj1' );
define( 'NONCE_KEY',        'pbslfgpwdpghwfgspu4fspmsplsk3epzmptxtgydtscgaoxvoezlwcclhkbvqoup' );
define( 'AUTH_SALT',        'joyve82pyyovurzeeavmgexnqes9wthdojkjwqh0yx56pnuqbujcopvvx5ofcoeh' );
define( 'SECURE_AUTH_SALT', 'g2xipnuaci8ul1sjzc8wteitqyulwauqqj6vbx5ezgmbc274fzlo1ufi9xcv1vj0' );
define( 'LOGGED_IN_SALT',   'etflxxbxoy8j0wazskctkhn7h0bu4zoaldxyonxbcghvz1twxncl74radye3xmqp' );
define( 'NONCE_SALT',       '9hhsvl5nw58xasxnxunx1oefql1rgoexawqzaymtkn3fbwlbjujvbtojxlzecngq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpqd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
// define('FS_METHOD' 'direct');
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
