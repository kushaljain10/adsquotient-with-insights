<?php
require_once "config.php";
require_once 'header.php';
require_once 'functions.php';
// if (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
//   header('Location: fbindex.php');
//   exit();
// }
// Set client id from Session

$clientId = isset($_SESSION["clientid"]) ? $_SESSION["clientid"] : '';

// Prepare a select statement
    $sql = "SELECT * FROM campaigns WHERE id = ?";

    if($stmt = mysqli_prepare($mysqlLink, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);

        // Set parameters
        $param_id = trim($_GET['id']);

        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);

            if(mysqli_num_rows($result) == 1){
                /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

                // Retrieve individual field value
                $dbAqID         = !empty($row["id"]) ? $row["id"] : '';
                $dbAqCampName   = !empty($row["aqcampname"]) ? $row["aqcampname"] : '';
                $dbAqSector     = !empty($row["aqsector"]) ? $row["aqsector"] : '';
                $dbAqCity       = !empty($row["aqcity"]) ? $row["aqcity"] : '';
                $dbCampIds   = !empty($row["campaign_id"]) ? $row["campaign_id"] : '';

            } else{
                // URL doesn't contain valid id parameter. Redirect to error page
                header("location: error.php");
                exit();
            }

        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }

    // Close statement
    mysqli_stmt_close($stmt);

    // Close connection
    // mysqli_close($mysqlLink);

$aqCampName     = $dbAqCampName;
$aqSector = $dbAqSector;
$aqcampCity = $dbAqCity;
// $aqInsights = $dbAqInsights;
// Update AQ id in session, it helps to update facebook data in database
$_SESSION['aqid'] = $dbAqID;

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate name
    $aqCampName = trim($_POST["aqCampName"]);
    if(empty($aqCampName)){
        $name_err = "Please enter AQ Dataset name.";
    } elseif(!filter_var($aqCampName, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $name_err = "Please enter a valid Dataset name.";
    } else{
        $name = $aqCampName;
    }

    $aqcampid = trim($_GET["id"]);
    $aqcampsector = trim($_POST["sector"]);
    $aqcampCity = trim($_POST["aqcampCity"]);
    // $aqInsights = htmlspecialchars(trim($_POST["aqInsights"]));

    // Check input errors before inserting in database
    // if(!empty($aqCampName) && !empty($aqcampsector) && !empty($aqcampCity) && !empty($aqInsights)){
    if(!empty($aqCampName) && !empty($aqcampsector) && !empty($aqcampCity)){
        // Prepare an insert statement
        // $sql = "UPDATE campaigns SET `aqcampname` = ?, `aqsector` = ?, `aqcity` = ?, `aqinsights` = ? WHERE `id`= ?; ";
        $sql = "UPDATE campaigns SET `aqcampname` = ?, `aqsector` = ?, `aqcity` = ? WHERE `id`= ?; ";
        if($stmt = mysqli_prepare($mysqlLink, $sql)){
            // Bind variables to the prepared statement as parameters
            // mysqli_stmt_bind_param($stmt, "ssssi", $param_campname, $param_sector, $param_city, $param_insights, $param_id);
            mysqli_stmt_bind_param($stmt, "ssssi", $param_campname, $param_sector, $param_city, $param_id);

            // Set parameters
            $param_campname = $aqCampName;
            $param_sector = $aqcampsector;
            $param_city = $aqcampCity;
            // $param_insights = $aqInsights;
            $param_id = $aqcampid;
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                echo '<div class="alert alert-success">AQ dataset updated</div>';
                // Records created successfully. Redirect to landing page
                // header("location: index.php");
                // exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
            // printf("Error: %s.\n", $stmt->error);
        }
        // Close statement
        mysqli_stmt_close($stmt);
    }
    // Close connection
    mysqli_close($mysqlLink);
}
?>
<div class="row">
  <div class="col-md-3">
    <a href="dashboard.php">Home</a> >> <?php echo $aqCampName; ?>
  </div>
</div>
<div class="wrapper">
  <?php if(!empty($dbAqCampName)){ ?>

            <div class="row">
                <div class="col-md-8">
                    <div class="page-header">
                        <h2>Edit AQ Dataset</h2>
                    </div>
                    <!-- <p>Connect with social media channel and choose a campaigns.</p> -->
                    <form action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($aqCampName_err)) ? 'has-error' : ''; ?>">
                            <label>AQ name<span>*</span></label>
                            <input required placeholder="Enter a name(It is for reference)" type="text" name="aqCampName" class="form-control" value="<?php echo $aqCampName; ?>">
                            <span class="help-block"><?php echo $aqCampName_err; ?></span>
                        </div>
                        <div class="form-group">
                          <label for="sector">Sector:<span>*</span></label>
                          <select class="form-control" name="sector" id="sector">
                            <optgroup label="Real Estate">
                              <option <?php if($aqSector=='real_estate_lessthan_50l'){ echo 'selected';} ?> value="real_estate_lessthan_50l">< 50 Lakhs</option>
                              <option <?php if($aqSector=='real_estate_50l_1c'){ echo 'selected';} ?> value="real_estate_50l_1c">50L - 1Crore</option>
                              <option <?php if($aqSector=='real_estate_1c_2c'){ echo 'selected';} ?> value="real_estate_1c_2c">1Crore - 2 Crore</option>
                              <option <?php if($aqSector=='real_estate_greaterthan_2c'){ echo 'selected';} ?> value="real_estate_greaterthan_2c">2 Crore+</option>
                            </optgroup>
                            <optgroup label="BFSI">
                              <option <?php if($aqSector=='bfsi_mutual_funds'){ echo 'selected';} ?> value="bfsi_mutual_funds">Mutual Funds</option>
                              <option <?php if($aqSector=='bfsi_insuarance'){ echo 'selected';} ?> value="bfsi_insuarance">Insuarance</option>
                            </optgroup>
                            <optgroup label="Retail">
                              <option <?php if($aqSector=='retail_jewellery'){ echo 'selected';} ?> value="retail_jewellery">Jewellery</option>
                              <option <?php if($aqSector=='retail_fashion'){ echo 'selected';} ?> value="retail_fashion">Fashion</option>
                            </optgroup>
                            <optgroup label="Ecommerce">
                              <option <?php if($aqSector=='ecommerce_fashion'){ echo 'selected';} ?> value="ecommerce_fashion">Fashion</option>
                            </optgroup>
                            <optgroup label="FMCG">
                              <option <?php if($aqSector=='fmcg_food'){ echo 'selected';} ?> value="fmcg_food">Food</option>
                            </optgroup>
                            <optgroup label="Healthcare">
                              <option <?php if($aqSector=='multi_speciality'){ echo 'selected';} ?> value="multi_speciality">Multi Speciality</option>
                              <option <?php if($aqSector=='single_speciality'){ echo 'selected';} ?> value="single_speciality">Single Speciality</option>
                            </optgroup>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="aqcampCity">City:<span>*</span></label>
                          <select required class="form-control" name="aqcampCity" id="aqcampCity">
                            <option>Select city</option>
                            <option <?php if($aqcampCity == 'Chennai') echo 'selected'; ?> value="Chennai">Chennai</option>
                            <option <?php if($aqcampCity == 'Bangalore') echo 'selected'; ?> value="Bangalore">Bangalore</option>
                            <option <?php if($aqcampCity == 'Mumbai') echo 'selected'; ?> value="Mumbai">Mumbai</option>
                          </select>
                        </div>
                        <?php /* ?><div class="form-group">
                          <label for="aqInsights">Insights:<span>*</span></label>
                          <textarea required class="form-control" name="aqInsights" id="aqInsights"><?php echo $aqInsights; ?></textarea>
                        </div><?php */ ?>
                        <div class="row">
                          <a href="dashboard.php" class="btn btn-primary">Cancel</a>
                          <button type="submit" name="update" class="btn btn-primary">Save</button>
                          <a href="campaign_detail.php?aqid=<?php echo $_GET['id']; ?>" class="btn btn-primary">FB campaign list</a>
                          <a href="#" class="btn btn-primary" disabled title="In v2.0" data-toggle="tooltip">Google campaign list</a>
                          <?php if($dbCampIds==''){ ?>
                          <a href="compare.php?aqid=<?php echo $_GET['id']; ?>" class="btn btn-primary" disabled title="No campaigns selected to compare" data-toggle="tooltip">Compare</a>
                          <?php } else { ?>
                          <a href="compare.php?aqid=<?php echo $_GET['id']; ?>" class="btn btn-primary">Compare</a>
                          <?php } ?>
                        </div>
                    </form>
                </div>
            </div>
          <?php }else{ ?>
            <div class="row">
              <div class="col-md-12">
                  <div class="page-header">
                      <h1>AQ Data Table</h1>
                  </div>
                  <div class="form-group">
                      <label>AQ Data Table name</label>
                      <p class="form-control-static"><?php echo $dbAqCampName; ?></p>
                  </div>
                  <div class="form-group">
                      <label>Sector</label>
                      <p class="form-control-static"><?php echo $dbAqSector; ?></p>
                  </div>
                  <div class="form-group">
                      <label>City</label>
                      <p class="form-control-static"><?php echo $dbAqCity; ?></p>
                  </div>
                  <?php /* ?><div class="form-group">
                      <label>Insights</label>
                      <p class="form-control-static"><?php echo $dbAqInsights; ?></p>
                  </div> <?php */ ?>
                  <p><a href="dashboard.php" class="btn btn-primary">Back</a></p>
              </div>
          </div>

         <?php  } ?>
            <!-- <div class="row">
              <a href="<?php echo $fbloginURL; ?>" class="btn btn-primary ">Get Facebook details</a>
              <a href="<?php echo "#";//$googleloginurl; ?>" class="btn btn-primary disabled">Connect with Google</a>
            </div> -->
        </div>
    </div>
</body>
</html>
