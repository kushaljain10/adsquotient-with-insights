<?php
require_once 'header.php';
require_once 'functions.php';

$dbfb_token = !empty($_SESSION['access_token']) ? $_SESSION['access_token'] : '';

if(empty($dbfb_token)){
  header("location: dashboard.php?");
  exit;
}

if(isset($_GET['aqid']) && !is_nan($_GET['aqid'])){
	$aqID = $_GET['aqid'];
}else{
	header('Location: dashboard.php?errormsg=No valid AQ campaign'); exit;
}
// Get values from db
$aqCampResultAry = doGetCampIdFromAQId($mysqlLink, $aqID, $_SESSION["clientid"]);
if($aqCampResultAry['success']){
  $inputCity = !empty($aqCampResultAry['data']['city']) ? $aqCampResultAry['data']['city'] : 'City not available';
  if(!empty($aqCampResultAry['data']['campaign_id'])){
    $dbCampIdAry = explode(',',$aqCampResultAry['data']['campaign_id']);
    $inputSector = !empty($aqCampResultAry['data']['aqsector']) ? $aqCampResultAry['data']['aqsector'] : 'Sector not available';
    $inputCity = !empty($_GET['city']) ? trim($_GET['city']) : "chennai";
  }
}
$debugVal = isset($_GET['debug']) ? $_GET['debug'] : false;


if(!empty($inputSector) && !empty($inputCity)){
  /*switch ($inputSubSector){
    case "under_50L":
      $inputSubSector = "<50L";
      break;

    case "50L-1C":
      $inputSubSector = "50L-1C";
      break;

    case "1C-2C":
      $inputSubSector = "1C-2C";
      break;
  }*/
  // echo $inputSector."<br>";
  // echo $inputSubSector."<br>";
  // Prepare a select statement
  $sql = "SELECT * FROM benchmark_details WHERE `aqsector` LIKE ? AND `city` like ? ";
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
      // Bind variables to the prepared statement as parameters
      mysqli_stmt_bind_param($stmt, "ss", $inputSector, $inputCity);

      // Attempt to execute the prepared statement
      if(mysqli_stmt_execute($stmt)){
          $result = mysqli_stmt_get_result($stmt);
          // echo __LINE__.mysqli_num_rows($result); exit;
          if(mysqli_num_rows($result) == 1){
              /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
              $benchMarkDetailsAry = mysqli_fetch_array($result, MYSQLI_ASSOC);
          } else{
              // URL doesn't contain valid id parameter. Redirect to error page
              // header("location: error.php");
              // exit();
          }
      } else{
          echo "Oops! Something went wrong. Please try again later.";
      }
  }
}
// $dbfb_token = "EAAFLpgouUzYBANiTVbbXZAvZCfb5ZA4GZA1omMdavabFylkCg6IW7bZALPmqT0iMxVNbKVBadNT88NEOqZBwNB1jDl5eurE59PAMIzpqK8CCCEI53oPXKHpJsXvzeFylCbdrEIxQPsxyzrM6vNG0E7QWQxccsdbe7GiZBHoTNn8UAZDZD";


// printArray($benchMarkDetailsAry);
$averageAry = array();
// Total number of campaigns selected to compare
$totalCampCnt = count($dbCampIdAry);

if(!empty($dbCampIdAry)){
  foreach($dbCampIdAry as $dbCampId){
    $fbParameters = $dbCampId."/insights?fields=campaign_name,campaign_id,objective,spend,reach,impressions,cpc,ctr,cpp,cpm,cost_per_action_type,cost_per_conversion,cost_per_10_sec_video_view,cost_per_thruplay&date_preset=lifetime";
    try {
      $response = $FB->get($fbParameters, $dbfb_token);
      try{
        $accDataAry[] = $response->getGraphEdge()->asArray();
      } catch (\Exception $e) {
        echo "Error occur in response:".$e->getMessage();
      }
    } catch (\Exception $e) {
        echo "Error occur:".$e->getMessage();
    }
  }
}
if(!empty($accDataAry)){
  foreach ($accDataAry as $accData) {
    /* To debug the data */
    if($debugVal==='true'){
      $result = getObjectiveMetrics($accData,true);
      $resultDebugAry[] = $result;
    }else{
      // $result = getObjectiveMetrics($accData);
      $result = getObjectiveMetrics($accData,true);
      $resultDebugAry[] = $result;
    }
    // Push all cost_per_result in an array with objective as key
    $resultAry[$result['objective']]['cost_per_result'][] = $result['cost_per_result'];
  }
  if($debugVal==='true'){
  /* uncomment following line to debug the data */
  }
  printArray($resultDebugAry);
  if(!empty($resultAry)){
    foreach ($resultAry as $key => $resultVal) {
      $average = array_sum($resultVal['cost_per_result'])/count($resultVal['cost_per_result']);
      $averageAry[$key] = number_format($average,2,'.','');
    }
    $fbAverageAry= array("facebook"=>$averageAry);
  }else{
    echo "<br>Internal error occured. Please contact admin";
  }
}else{
  echo "<br>Unable to get data from Facebook!. Please try again after sometimes.";
}

// Merge facebook result array and benchmark array
if(!empty($fbAverageAry)){
  $fbAverageData = array();
  foreach($fbAverageAry['facebook'] as $fbKey=> $fbAverageData){
    $fbAverageData = floatval($fbAverageData);
    $percentChangeVal = '';
    switch ($fbKey){
      case "LINK_CLICKS":
        $benchmarkData = !empty($benchMarkDetailsAry['fb_CPL']) ? floatval($benchMarkDetailsAry['fb_CPL']) : 'NA';
        break;

      case "VIDEO_VIEWS":
        $benchmarkData = !empty($benchMarkDetailsAry['fb_CPV']) ? floatval($benchMarkDetailsAry['fb_CPV']) : 'NA';
        break;

      case "REACH":
        $benchmarkData = !empty($benchMarkDetailsAry['fb_CPC']) ? floatval($benchMarkDetailsAry['fb_CPC']) : 'NA';
        break;

      case "LEAD_GENERATION":
        $benchmarkData = !empty($benchMarkDetailsAry['fb_CPL']) ? floatval($benchMarkDetailsAry['fb_CPL']) : 'NA';
        break;

      case "POST_ENGAGEMENT":
        $benchmarkData = !empty($benchMarkDetailsAry['fb_CTR']) ? floatval($benchMarkDetailsAry['fb_CTR']) : 'NA';
        break;
    }
    // var_dump($benchmarkData);
    if(is_finite($benchmarkData)){
      // $percentChangeVal = number_format(((1 - $benchmarkData / $fbAverageData) * 100), 2, '.','');
      $percentChangeVal = number_format(((1 - $fbAverageData/ $benchmarkData) * 100), 2, '.','');
    }
    $percentChange = !empty($percentChangeVal) ? $percentChangeVal." %" : 'NA';
    $benchmarkFieldData[] = array('Channel'=>'Facebook','fbAdType'=>$fbKey,'fbAdValue'=>$fbAverageData ,'dbData'=>$benchmarkData,'difference'=>$percentChange);
  }
}else{
  echo "Unable to fetch Facebook data!";
}
// printArray($benchmarkFieldData);
?>

  <div class="container-fluid">
    <div class="row">
  		<div class="col-md-3">
  			<a href="dashboard.php">Home</a> >> <a href="fbindex.php">Ad account</a> >> Detail
  		</div>
  	</div>
      <div class="row">
          <div class="col-md-12">
              <div class="page-header clearfix">
                  <h2 class="pull-left"><?php echo "Ads Quotient comparison data"; ?></h2>
              </div>
          </div>
        </div>
        <div class="panel panel-primary">
          <div class="panel-heading">Benchmark comparison</div>
          <div class="panel-body">
            <div class="col-md-4">
              <p><b>Sector: </b><?php echo $inputSector; ?></p>
            </div>
            <!-- <div class="col-md-4">
              <p><b>Sub Sector: </b><?php echo $benchmarkSubSector; ?></p>
            </div> -->
            <div class="col-md-4">
              <p><b>City: </b><?php echo ucfirst(strtolower($inputCity)); ?></p>
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Channel</th>
                    <th>Ad Type</th>
                    <th>Client Cost per result</th>
                    <th>Benchmark Cost per result</th>
                    <th>% Difference</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($benchmarkFieldData)){
                        foreach ($benchmarkFieldData as $benchmarkField) {
                            echo "<tr>";
                            echo "<td>".$benchmarkField['Channel']."</td>";
                            echo "<td>".$benchmarkField['fbAdType']."</td>";
                            echo "<td>".$benchmarkField['fbAdValue']."</td>";
                            echo "<td>".$benchmarkField['dbData']."</td>";
                            echo "<td class='".$comparisonClass."'>".$benchmarkField['difference']."</td>";
                            echo "</tr>";
                        }
                    }
                   ?>
                </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
<?php if($debugVal){ ?>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Raw data</h4>
        <p>Copy the content and share with us to debug</p>
        <!-- <button onclick="copyText()">Copy text</button> -->
      </div>
      <div class="modal-body">
        <p><?php if(!empty($resultDebugAry)){ var_dump($resultDebugAry); }else{echo 'No data';} ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Raw data</button>
<?php } ?>
</body>
</html>
<style>
.aqcls-up,.aqcls-down{
  text-align: center;
  background-size: 14% !important;
  background-position: 81% 35%;
  background-repeat: no-repeat;
}
.aqcls-up{
  background-image: url('images/Green_Arrow_Top-512.png');
}
.aqcls-down{
  background-image: url('images/Red_Arrow_Down-512.png');
}
</style>
<script>
function copyText() {
  /* Get the text field */
  var copyText = document.getElementById("copyText");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  // alert("Copied the text: " + copyText.value);
  alert("Copied the text: ");
}
</script>
