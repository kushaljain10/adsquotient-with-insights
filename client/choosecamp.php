<?php
// Include config file
// require_once "config.php";
require_once "header.php";

function printArray($str){
  echo "<pre>";
  print_r($str);
  echo "</pre>";
}
printArray($_GET);
// Define variables and initialize with empty values
$name = $platform = $campid = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "GET"){
    // Get campaign name
    $name = isset($_GET["name"]) ? trim($_GET["name"]) : "";
    $campName = !empty($name) ? urldecode($name) : "";

    // Get camp_id
    $camp_id = isset($_GET["camp_id"]) ? trim($_GET["camp_id"]) : "";
    $campID = !empty($camp_id) ? $camp_id : "";

    // Get campaign objective
    $obj = isset($_GET["obj"]) ? trim($_GET["obj"]) : "";
    $campObj = !empty($obj) ? urldecode($obj) : "";

    // Get campaign objective
    $platform = isset($_GET["platform"]) ? trim($_GET["platform"]) : "";
    $campPlatform = !empty($platform) ? urldecode($platform) : "";

    // Logged in user
    $clientID = ($_SESSION["loggedin"]) ? $_SESSION["clientid"] : "";

    // AQ id
    $aqID = isset($_SESSION["aqid"]) ? $_SESSION["aqid"] : "";

    $campCPC = isset($_GET["cpc"]) ? trim($_GET["cpc"]) : "";
    $campCTR = isset($_GET["ctr"]) ? trim($_GET["ctr"]) : "";

    // Check input errors before inserting in database
    if(!empty($clientID)){
      // mysqli_report(MYSQLI_REPORT_ALL);
        // check Camp id is exists or not in database
        // Prepare a select statement
        $sql = "SELECT id FROM campaigns WHERE campaign_id = ?";

         if($stmt = mysqli_prepare($mysqlLink, $sql)){
             // Bind variables to the prepared statement as parameters
             mysqli_stmt_bind_param($stmt, "i", $camp_id);

             // Attempt to execute the prepared statement
             if(mysqli_stmt_execute($stmt)){
                 // Store result
                 mysqli_stmt_store_result($stmt);
                 // Check if username exists, if yes then verify password
                 if(mysqli_stmt_num_rows($stmt) == 1){
                     // Bind result variables
                     mysqli_stmt_bind_result($stmt, $id);
                     if(mysqli_stmt_fetch($stmt)){
                       $updateSql = "UPDATE campaigns SET campaign_name = ?, campaign_id = ?, platform =? , objective = ?, fb_CPC = ?, fb_CTR = ? WHERE id = ?";
                       if($stmt = mysqli_prepare($mysqlLink, $updateSql)){
                           // Bind variables to the prepared statement as parameters
                           mysqli_stmt_bind_param($stmt, "sissddi", $param_campName,$param_campID ,$param_platform ,$param_objective, $param_cpc, $param_ctr, $id );
                           // Set parameters
                           $param_campName = $campName;
                           $param_campID = $campID;
                           $param_platform = $campPlatform;
                           $param_objective = $campObj;
                           $param_AQId = $aqID;
                           $param_cpc = $campCPC;
                           $param_ctr = $campCTR;
                             // Redirect user to welcome page
                          if(mysqli_stmt_execute($stmt)){
                             header("location: dashboard.php");
                          }else{
                            echo "Something went wrong while execution. Please try again later.";
                           }
                        }
                     }
                 } else{
                     // Prepare an insert statement
                     $sql = "INSERT INTO campaigns (client_id, campaign_name, campaign_id, objective) VALUES (?, ?, ?, ?)";
                     if($stmt = mysqli_prepare($mysqlLink, $sql)){
                         // Bind variables to the prepared statement as parameters
                         mysqli_stmt_bind_param($stmt, "isis",$param_clientID, $param_campname, $param_campID, $param_objective);

                         // Set parameters
                         $param_campname = $aqCampName;
                         $param_campID = $campID;
                         $param_objective = $campObj;
                         $param_clientID = $clientId;
                         // Attempt to execute the prepared statement
                         if(mysqli_stmt_execute($stmt)){
                             // Records created successfully. Redirect to landing page
                             $insertedId = $mysqlLink->insert_id;
                         }
                       }
                     }
             } else{
                 echo "Oops! Something went wrong. Please try again later.";
             }
         }
         // Close statement
         mysqli_stmt_close($stmt);
        }else{
            echo("Statement failed: ". $stmt->error . "<br>");
        }
    }
    // Close connection
    mysqli_close($mysqlLink);
}
?>
