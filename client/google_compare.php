<?php
require_once 'header.php';
require_once 'functions.php';

$tableHeader = '<th>My CPC</th><th>Benchmark CPC</th><th>% Difference</th>
                                <th>My CPM</th><th>Benchmark CPM</th><th>% Difference</th>
                                <th>My CPL</th><th>Benchmark CPL</th><th>% Difference</th>
                                <th>My CTR</th><th>Benchmark CTR</th><th>% Difference</th>
                                <th>My Cost per ThruPlay</th><th>Benchmark Cost per ThruPlay</th><th>% Difference</th>
                                <th>My CPE</th><th>Benchmark CPE</th><th>% Difference</th>';

?>
<div class="row">
	<div class="col-md-5">
		<a href="dashboard.php">Home</a> >>
		<a href="update.php?id=<?php echo $aqID; ?>"><?php echo $dbAqCampName;  ?></a> >> Benchmark Comparison
	</div>
</div>
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
              <div class="clearfix">
                  <h3 class="pull-left"><?php echo "Benchmark Comparison"; ?></h3>
              </div>
          </div>
        </div>
          <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="margin-bottom: 0;">
                  <div class="panel-body">
                    <b>Connected Ad account:</b> <?php echo $dbAqFbAdAccountName; ?><a style="color:#ffa500;" class="btn btn-link" href="fbaccounts.php?aqid=<?php echo $aqID; ?>" >Change</a>
                      <br >
                        <a data-toggle="collapse" id="fbCampName" href="#collapse1">View Google campaign list</a>
                      <a style="color:#ffa500;" class="btn btn-link" href="campaign_detail.php?aqid=<?php echo $aqID; ?>">Change</a>
                      <div id="collapse1" class="panel-collapse collapse">
                        <ul class="list-group">
                            <li class="list-group-item"><?php echo $fbCampName['campaign_name']; ?></li>
                        </ul>
						<!-- <div class="panel-footer"></div> -->
                      </div>
                  </div>
                </div>
		  </div>
          </div>
        <br>
        <div class="row">
          <form action="" method="GET">
          <div class="col-md-12" style="padding-bottom: 30px;">
            <div class="col-md-2">
            <select class="form-control" required name="aqsector">
              <option value="">Choose sector</option>
              <optgroup label="Real Estate">
                <option <?php if($aqsector=='real_estate_lessthan_50l'){ echo 'selected';} ?> value="real_estate_lessthan_50l">< 50 Lakhs</option>
                <option <?php if($aqsector=='real_estate_50l_1c'){ echo 'selected';} ?> value="real_estate_50l_1c">50L - 1Crore</option>
                <option <?php if($aqsector=='real_estate_1c_2c'){ echo 'selected';} ?> value="real_estate_1c_2c">1Crore - 2 Crore</option>
                <option <?php if($aqsector=='real_estate_greaterthan_2c'){ echo 'selected';} ?> value="real_estate_greaterthan_2c">2 Crore+</option>
              </optgroup>
              <optgroup label="BFSI">
                <option <?php if($aqsector=='bfsi_mutual_funds'){ echo 'selected';} ?> value="bfsi_mutual_funds">Mutual Funds</option>
                <option <?php if($aqsector=='bfsi_insuarance'){ echo 'selected';} ?> value="bfsi_insuarance">Insuarance</option>
              </optgroup>
              <optgroup label="Retail">
                <option <?php if($aqsector=='retail_jewellery'){ echo 'selected';} ?> value="retail_jewellery">Jewellery</option>
                <option <?php if($aqsector=='retail_fashion'){ echo 'selected';} ?> value="retail_fashion">Fashion</option>
              </optgroup>
              <optgroup label="Ecommerce">
                <option <?php if($aqsector=='ecommerce_fashion'){ echo 'selected';} ?> value="ecommerce_fashion">Fashion</option>
              </optgroup>
              <optgroup label="FMCG">
                <option <?php if($aqsector=='fmcg_food'){ echo 'selected';} ?> value="fmcg_food">Food</option>
              </optgroup>
              <optgroup label="Healthcare">
                <option <?php if($aqsector=='multi_speciality'){ echo 'selected';} ?> value="multi_speciality">Multi Speciality</option>
                <option <?php if($aqsector=='single_speciality'){ echo 'selected';} ?> value="single_speciality">Single Speciality</option>
              </optgroup>
            </select>
          </div>
          <div class="col-md-2">
            <select class="form-control" required name="aqMetric">
              <option <?php if($aqMetric=='all'){ echo 'selected'; } ?> value="all">ALL</option>
              <option <?php if($aqMetric=='cpl'){ echo 'selected'; } ?> value="cpl">CPL</option>
              <option <?php if($aqMetric=='cpc'){ echo 'selected'; } ?> value="cpc">CPC</option>
              <option <?php if($aqMetric=='ctr'){ echo 'selected'; } ?> value="ctr">CTR</option>
              <option <?php if($aqMetric=='cptp'){ echo 'selected'; } ?> value="cptp">CPTP</option>
              <option <?php if($aqMetric=='cpm'){ echo 'selected'; } ?> value="cpm">CPM</option>
              <option <?php if($aqMetric=='cpe'){ echo 'selected'; } ?> value="cpe">CPE</option>
              <option <?php if($aqMetric=='cpv'){ echo 'selected'; } ?> value="cpv">CPV</option>
            </select>
          </div>
          <div class="col-md-2">
            <select class="form-control" required name="aqDuration">
              <option value="">Duration</option>
              <option <?php if($aqDuration =='last_30d') {echo 'selected';} ?> value="last_30d">Last 30 days</option>
              <option <?php if($aqDuration =='last_90d') {echo 'selected';} ?> value="last_90d">Last 3 months </option>
              <option <?php if($aqDuration =='this_year') {echo 'selected';} ?> value="this_year">This year</option>
              <option <?php if($aqDuration =='lifetime') {echo 'selected';} ?> value="lifetime">Lifetime</option>
            </select>
          </div>
          <div class="col-md-2">
            <select class="form-control" required name="aqCity">
              <option value="">City</option>
              <option <?php if(strtolower($aqCity) =='all') {echo 'selected';} ?> value="all">ALL</option>
              <option <?php if(strtolower($aqCity) =='chennai') {echo 'selected';} ?> value="chennai">Chennai</option>
              <option <?php if(strtolower($aqCity) =='bangalore') {echo 'selected';} ?> value="bangalore">Bangalore </option>
              <option <?php if(strtolower($aqCity) =='mumbai') {echo 'selected';} ?> value="mumbai">Mumbai</option>
            </select>
          </div>
          <div class="col-md-2">
            <button class="btn btn-primary" type="submit">Refresh</button>
          </div>
          <input type="hidden" name="aqid" value="<?php echo $aqID; ?>">
          </div>
        </form>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Channel</th>
                    <th>Ad Type</th>
                    <?php echo $tableHeader; ?>
                  </tr>
                </thead>
                <tbody style="text-align: center;">
                  
                  </tbody>
                </table>
            </div>
        </div>

        <!-- <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label>Insights</label>
                  <p class="form-control-static"><?php echo $dbInsights; ?></p>
              </div>
            </div>
        </div> -->
  </div>
</div>
</body>
</html>
<style>
th{text-align: center;}
.aqcls-up,.aqcls-down{
  text-align: center;
  background-size: 14% !important;
  background-position: 81% 35%;
  background-repeat: no-repeat;
}
.aqcls-up{
  background-image: url('images/Green_Arrow_Top-512.png');
}
.aqcls-down{
  background-image: url('images/Red_Arrow_Down-512.png');
}
.wrapper{
  width: 100%;
}
table tr th{
  font-size: 13px;
}
</style>
<script>
function copyText() {
  /* Get the text field */
  var copyText = document.getElementById("copyText");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  // alert("Copied the text: " + copyText.value);
  alert("Copied the text: ");
}
</script>
<?php include 'footer.php'; ?>
