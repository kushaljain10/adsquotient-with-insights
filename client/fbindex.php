<?php
require_once 'config.php';
require_once 'header.php';
require_once 'functions.php';

$param_clientID = isset($_SESSION["clientid"]) ? $_SESSION["clientid"] : '';
// printArray($_SESSION);
// Get facebook token from database
$sql = "SELECT fb_access_token FROM fb_details WHERE id_client = ?";

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* create a prepared statement */
if($result = mysqli_prepare($mysqlLink, $sql)){
	/* bind parameters for markers */
	mysqli_stmt_bind_param($result, "i", $param_clientID);
	/* execute query */
	 mysqli_stmt_execute($result);
	 /* bind result variables */
	 mysqli_stmt_bind_result($result, $fbTokenResult);
	 /* fetch value */
	 mysqli_stmt_fetch($result);
   if(!empty($fbTokenResult))
	  $_SESSION['access_token'] = $fbTokenResult;
}
if (!isset($_SESSION['access_token'])) {
	// header('Location: create.php');
	// exit();
	// $acces_to = !empty($_SESSION['access_token']) ? $_SESSION['access_token'] : '';
 //    if(!empty($acces_to)){
 //      $dataBtn = '<a href="fbindex.php" class="align-middle btn btn-info" role="button">Facebook page list</a>';
 //    }else{
      $dataBtn = '<a href="fbconnect.php" class="align-middle btn btn-info" role="button">Connect with Facebook</a>';
    // }
    echo $dataBtn;
}else{
	$sql = "SELECT id FROM fb_details WHERE id_client = ?";
	if($stmt = mysqli_prepare($mysqlLink, $sql)){
			// Bind variables to the prepared statement as parameters
			mysqli_stmt_bind_param($stmt, "i", $param_clientID);

			// Attempt to execute the prepared statement
			if(mysqli_stmt_execute($stmt)){
					// Store result
					mysqli_stmt_store_result($stmt);

					// Check if client id exists, if yes then update the access token
					if(mysqli_stmt_num_rows($stmt) == 1){
							// Bind result variables
							mysqli_stmt_bind_result($stmt, $Id);
							if(mysqli_stmt_fetch($stmt)){
									$sql = "UPDATE fb_details SET fb_access_token = ? WHERE id_client = ? AND id = ?";
									if($stmt = mysqli_prepare($mysqlLink, $sql)){
										mysqli_stmt_bind_param($stmt, "sii", $param_fbtoken,$param_clientID,$param_id );
										// Set parameters
										$param_fbtoken 	= $_SESSION['access_token'];
										$param_clientID = $_SESSION["clientid"];
										$param_id 			= $Id;
										if(!mysqli_stmt_execute($stmt)){
												echo "Something went wrong while updating results. Please try again later.";
										}
										else
										{
											header("Location: dashboard.php");
											exit();
										}
									}
							}
						}else{
							// Make a new entry with client id and token
							$sql = "INSERT INTO fb_details (id_client, fb_access_token) VALUES (?, ?)";
							if($stmt = mysqli_prepare($mysqlLink, $sql)){
									// Bind variables to the prepared statement as parameters
									mysqli_stmt_bind_param($stmt, "is", $param_clientID,$param_fbtoken);
									// Set parameters
									$param_clientID = $_SESSION["clientid"];
									$param_fbtoken = $_SESSION['access_token'];
									// Attempt to execute the prepared statement
									if(!mysqli_stmt_execute($stmt)){
											echo $stmt->error." Something went wrong. Please try again later.";
									}
									else
									{
										header("Location: dashboard.php");
										exit();
									}
									// Close statement
									mysqli_stmt_close($stmt);
							}
						}
					}
				}
}
?>
<div class="row">&nbsp;</div>
<div class="row">
		<?php if(!empty($_SESSION['userData']['adaccounts'])){ ?>
 			<table class="table table-hover table-bordered">
				<tbody>
					<tr>
					    <th style="text-align: center;">Name</th>
					    <th style="text-align: center;">Currency</th>
					    <th style="text-align: center;">Total amount spent</th>
					    <th style="text-align: center;">Account status</th>
					    <th style="text-align: center;">Action</th>
					</tr>
			  		<?php $i = 0;
	        	if(!empty($_SESSION['userData']['adaccounts'])){
							foreach($_SESSION['userData']['adaccounts'] as $acc){
								$acc_num  = $acc["id"];
								$acc_name  = $acc["name"];
								$acc_status  = $acc["account_status"];
								switch ($acc_status) {
									case "1":
										$acc_status_term = "ACTIVE";
										$acc_status_msg = "Select";
										$btn = "btn btn-primary";
										$btn_status = "";
										break;

									case "2":
										$acc_status_term = "DISABLED";
										$acc_status_msg = "DISABLED";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "3":
										$acc_status_term = "UNSETTLED";
										$acc_status_msg = "UNSETTLED";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "7":
										$acc_status_term = "PENDING_RISK_REVIEW";
										$acc_status_msg = "PENDING RISK REVIEW";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "8":
										$acc_status_term = "PENDING_SETTLEMENT";
										$acc_status_msg = "PENDING SETTLEMENT";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "9":
										$acc_status_term = "IN_GRACE_PERIOD";
										$acc_status_msg = "IN GRACE PERIOD";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "100":
										$acc_status_term = "PENDING_CLOSURE";
										$acc_status_msg = "PENDING CLOSURE";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "101":
										$acc_status_term = "CLOSED";
										$acc_status_msg = "CLOSED";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "201":
										$acc_status_term = "ANY_ACTIVE";
										$acc_status_msg = "ANY ACTIVE";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									case "202":
										$acc_status_term = "ANY_CLOSED";
										$acc_status_msg = "ANY CLOSED";
										$btn = "btn btn-danger";
										$btn_status = "disabled";
										break;

									default:
										$acc_status_term = "UNKNOW ERROR";
										$acc_status_msg = "UNKNOW ERROR";
										$btn = "btn btn-primary";
										$btn_status = "disabled";
								}

								 ?>
									<tr>
                    <td><?php echo $acc_name; ?></td>
                    <td><?php if($acc["currency"]){echo $acc["currency"]; } ?></td>
                    <td><?php if($acc["amount_spent"]){echo currentFormat($acc["amount_spent"]); } ?></td>
                    <td><?php echo $acc_status_msg; ?></td>
                    <td><a class="<?php echo $btn; ?>" <?php echo $btn_status; ?> title='<?php echo $acc_status_msg; ?>' data-toggle='tooltip' href="acc_detail.php?acc_id=<?php echo $acc_num; ?>"><?php echo $acc_status_msg; ?></a></td>
									</tr>
							<?php
							$i++;
							}
				 		}  ?>
				</tbody>
			</table>
		<?php } ?>
	</div>
</body>
<?php include 'footer.php'; ?>
