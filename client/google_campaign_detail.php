<?php
require_once "config.php";
include_once 'header.php';
require_once 'functions.php';

?>
<form method="POST" action="" class="filter_form">
<div class="row">
	<div class="col-md-5">
		<a href="dashboard.php">Home</a> >>
		<a href="update.php?id=<?php echo $aqID; ?>"><?php echo $dbAqCampName;  ?></a> >> Google Ads Campaign list
	</div>
</div>
	<div class="panel panel-default" style="margin-bottom: 0;">
		<div class="panel-body"><b>Connected Ad account:</b> <?php echo 'Ad Account Name'; ?><a style="color:#ffa500;" class="btn btn-link" href="fbaccounts.php?aqid=<?php echo $aqID; ?>">Change Google Ads Account</a></div>
	</div>
<div class="form-inline">
	<div class="row">
		<h3>Google campaign list</h3>
	</div>
	<div class="row">
			<div class="col-md-3 col-xs-8 col-lg-3">
				<input class="btn btn-primary" value="Save Google Ads campaign list" name="frmCampSubmit" id="frmCampSubmit" type="submit"/>
			</div>
			<!-- <div class="col-md-3 col-xs-6 col-lg-2">
				<a href="fbaccounts.php?aqid=<?php echo $aqID; ?>" class="btn btn-warning"><?php echo !empty($fbAccountID) ? 'Change' : 'Add'; ?> FB ad account</a>
			</div> -->
				<div class="col-md-2 col-xs-4 col-lg-2">
					<a href="compare.php?aqid=<?php echo $aqID; ?>" class="btn btn-success">Compare</a>
				</div>
		</div>
		<div class="row">
			<p><?php echo $msgPanel; ?></p>
		</div>
	</div>
<?php
	$headingArray = array('Select', 'Campaign Name', 'Objective', 'Spend', 'Status');
	$dataArray = array(
		array('<input type="checkbox" value="1">', 'Campaign 1', 'Objective 1', 'Spend 1', 'Status 1'), 
		array('<input type="checkbox" value="2">', 'Campaign 2', 'Objective 2', 'Spend 2', 'Status 2'),
		array('<input type="checkbox" value="3">', 'Campaign 3', 'Objective 3', 'Spend 3', 'Status 3')
	);
?>

<table class="table table-hover">
<?php
	if(count($headingArray)>0){
		echo '<tr>';
		foreach($headingArray as $heading){
			echo '<th>'."$heading".'</th>';
		}
		echo '</tr>';
	}
	if(count($dataArray)>0){
		foreach($dataArray as $dataRow){
			echo '<tr>';
			foreach($dataRow as $datas){
				echo '<td>'."$datas".'</td>';
			}
			echo '</tr>';
		}
	}	
?> 
		  <!-- <tr>
				<td><input data-toggle="tooltip" type="checkbox" value="1"></td>
				<td>name</td>
				<td>objective</td>
				<td>spend</td>
				<td>status</td>
		  </tr> -->
</table>
</form>
</div>
</body>
</html>
<script>
// function valthis() {
// 	if($("input[type='checkbox']:checked").length > 0){
// 		if($("input[type='checkbox']:checked").length > 50){
// 			alert("Maximum campaign should be less than 50");
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	}else{
// 		alert("Please, choose a campaign to save!");
// 		return false;
// 	}
// }
$('#selectAll').click(function(e){
    var table= $(e.target).closest('table');
		$('td input:checkbox',table).prop('checked',this.checked);
});
</script>
<?php include 'footer.php'; ?>
