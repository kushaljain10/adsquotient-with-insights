<?php
require_once "config.php";
require_once 'header.php';
require_once 'functions.php';
// printArray(BASE_URL);
// echo dirname(__FILE__);
// CHeck $fbToken
$clientID = $_SESSION["clientid"];
$fbdata = doGetFBAccIDFromClient($mysqlLink, $clientID);
if($fbdata['success']){
	$_SESSION['client']['access_token'] = $_SESSION['access_token'] = $fbdata['data']['fbtoken'];
	$_SESSION['client']['account_id'] = $fbdata['data']['fbAccountID'];
	// printArray($fbdata);
}
?>
<div class="container">
<?php
if($_SESSION["loggedin"]){
if(empty($_SESSION['access_token']) )
{
?>
	<div class="row">
		<div class="alert alert-danger">Connect with Facebook by clicking <a href="fbconnect.php">here!</a></div>
	</div>
<?php
}
}
?>
<style type="text/css">
    .wrapper{
        width: 100%;
    }
    .btn{
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>
	<div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">AQ Datasets</h2>
                        <?php if(empty($_SESSION['access_token']) ){ ?>
                            <a href="create.php" class="btn btn-info pull-right" data-toggle="tooltip" title="Connect with FB to Add New Dataset" disabled>Add AQ Dataset</a>
                        <?php }else{ ?>
                            <a href="create.php" class="btn btn-info pull-right">Add AQ Dataset</a>
                        <?php } ?>
                    </div>
                    <?php

                    // Attempt select query execution
                    $sql = "SELECT * FROM campaigns WHERE client_id = '".$clientID."'";
                    if($result = mysqli_query($mysqlLink, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            echo "<table class='table table-bordered table-striped'>";
                                echo "<thead>";
                                    echo "<tr>";
                                        echo "<th>#</th>";
                                        echo "<th>AQ Dataset Name</th>";
                                        echo "<th>Sector</th>";
                                        echo "<th>City</th>";
                                        // echo "<th>Platform</th>";
                                        echo "<th>Action</th>";
                                    echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                                $k=1;
                                while($row = mysqli_fetch_array($result)){
                                    echo "<tr>";
                                        echo "<td>" . $k . "</td>";
                                        echo "<td>" . $row['aqcampname'] . "</td>";
                                        echo "<td>" . $row['aqsector'] . "</td>";
                                        echo "<td>" . $row['aqcity'] . "</td>";
                                        // echo "<td>" . $row['platform'] . "</td>";
                                        // echo "<td style='text-align:center;'>";
                                        echo "<td>";
                                            echo "<a href='update.php?id=". $row['id'] ."' class='btn btn-info' role='button' style='float:none;'>Edit</a>";
                                            echo "<a href='delete.php?aqid=". $row['id'] ."' class='btn btn-danger' role='button' style='float:none;'>Delete</a>";
                                        if($row['fb_ad_account']!='')
                                        {
                                            	// echo "<a href='campaign_detail.php?aqid=".$row['id']."' class='btn btn-info' role='button'  style='float:none;'>Edit FB Campaigns</a>";
                                            	echo "<a href='compare.php?aqid=".$row['id']."' class='btn btn-success' role='button'  style='float:none;'>Compare</a>";
                                                // echo "<a href='update.php?id=". $row['id'] ."' title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil' style='float:none;'></span></a>";
                                                // echo "<a href='delete.php?aqid=". $row['id'] ."' title='Delete Record' data-toggle='tooltip'  style='float:none;'><span class='glyphicon glyphicon-trash'></span></a>";
                                                /*echo "<a href='delete.php?campid=". $row['id'] ."' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                                                echo "<a href='compare.php?id=". $row['id'] ."' title='Compare' data-toggle='tooltip'><span class='glyphicon glyphicon-eye-open'></span></a>";*/
                                        }
                                        else
                                        {
                                                echo "<a href='fbaccounts.php?aqid=".$row['id']."' class='btn btn-info' role='button' style='float:none;'>Add FB Ad Account</a>";
                                        }
                                        echo "</td>";
                                    echo "</tr>";
                                    $k++;
                                }
                                echo "</tbody>";
                            echo "</table>";
                            // Free result set
                            mysqli_free_result($result);
                        } else{
                            echo "<p class='lead'><em>No records were found.</em></p>";
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqlLink);
                    }

                    // Close connection
                    mysqli_close($mysqlLink);
                    ?>
                </div>
            </div>

  <?php
    $acces_to = !empty($_SESSION['access_token']) ? $_SESSION['access_token'] : '';
    if(!empty($acces_to)){
      $dataBtn = '<a href="fbindex.php" class="align-middle btn btn-info" role="button">Facebook page list</a>';
    }else{
      $dataBtn = '<a href="fbconnect.php" class="align-middle btn btn-info" role="button">Connect with Facebook</a>';
    }
    // echo $dataBtn;
   ?>
</div>
</body>
</html>
<?php include 'footer.php'; ?>
