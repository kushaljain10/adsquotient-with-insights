<?php
require_once "config.php";
require_once 'header.php';
require_once 'functions.php';
// if (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
//   header('Location: fbindex.php');
//   exit();
// }
// Set client id from Session


// Define variables and initialize with empty values
$clientId = isset($_SESSION["clientid"]) ? $_SESSION["clientid"] : '';
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate name
    $aqCampName = trim($_POST["aqCampName"]);
    if(empty($aqCampName)){
        $name_err = "Please enter AQ Dataset name.";
    } elseif(!filter_var($aqCampName, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $name_err = "Please enter a valid campaign name.";
    } else{
        $name = $aqCampName;
    }
    $aqcampsector = trim($_POST["aqsector"]);
    $aqcampCity = trim($_POST["aqcampCity"]);

    // Check input errors before inserting in database
    if(!empty($aqCampName) && !empty($aqcampsector) && !empty($aqcampCity)){
      // printArray($_POST);
        // Prepare an insert statement
        $sql = "INSERT INTO campaigns (client_id,aqcampname,aqsector,aqcity,added_time) VALUES (?, ?, ?,?,?)";
        if($stmt = mysqli_prepare($mysqlLink, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "issss",$clientId, $aqCampName, $aqcampsector,$aqcampCity,date("Y-m-d H:i:s"));

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                // echo $insertedId = $mysqlLink->insert_id;
                // echo '<div class="alert alert-success">AQ campaign created, go back to add Facebook campaigns by click <a href="dashboard.php">Here!</a></div>';
                $lastInsertId = mysqli_insert_id($mysqlLink);
                header("location: update.php?id=".$lastInsertId);
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
            // printf("Error: %s.\n", $stmt->error);
        }else{
          echo "Error occured";
        }
        // Close statement
        mysqli_stmt_close($stmt);
    }
    // Close connection
    mysqli_close($mysqlLink);
}
?>
<div class="row">
  <div class="col-md-3">
    <a href="dashboard.php">Home</a> >> Add Dataset
  </div>
</div>
<div class="wrapper">
  <?php if(empty($dbAqCampName)){ ?>
            <div class="row">
                <div class="col-md-8">
                    <div class="page-header">
                        <h2>AQ Dataset</h2>
                    </div>
                    <!-- <p>Connect with social media channel and choose a campaigns.</p> -->
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($aqCampName_err)) ? 'has-error' : ''; ?>">
                            <label>AQ Dataset name<span>*</span></label>
                            <input required placeholder="Enter a name(It is for reference)" type="text" name="aqCampName" class="form-control" value="<?php echo $aqCampName; ?>">
                            <span class="help-block"><?php echo $aqCampName_err; ?></span>
                        </div>
                        <select class="form-control" required name="aqsector">
            							<option value="">Choose sector</option>
            							<optgroup label="Real Estate">
            						    <option value="real_estate_lessthan_50l">< 50 Lakhs</option>
            						    <option value="real_estate_50l_1c">50L - 1Crore</option>
            						    <option value="real_estate_1c_2c">1Crore - 2 Crore</option>
            						    <option value="real_estate_greaterthan_2c">2 Crore+</option>
            						  </optgroup>
            						  <optgroup label="BFSI">
            						    <option value="bfsi_mutual_funds">Mutual Funds</option>
            						    <option value="bfsi_insuarance">Insuarance</option>
            						  </optgroup>
            						  <optgroup label="Retail">
            						    <option value="retail_jewellery">Jewellery</option>
            						    <option value="retail_fashion">Fashion</option>
            						  </optgroup>
            						  <optgroup label="Ecommerce">
            						    <option value="ecommerce_fashion">Fashion</option>
            						  </optgroup>
            						  <optgroup label="FMCG">
            						    <option value="fmcg_food">Food</option>
            						  </optgroup>
            						  <optgroup label="Healthcare">
            						    <option value="multi_speciality">Multi Speciality</option>
            						    <option value="single_speciality">Single Speciality</option>
            						  </optgroup>
            						</select>
                        <!-- <div class="form-group">
                          <label for="sector">Sector:<span>*</span></label>
                          <select class="form-control" name="sector" id="sector">
                            <option>Real estate</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="sub-sector">Sub Sector:<span>*</span></label>
                          <select required class="form-control" name="subsector" id="sub-sector">
                            <option value="<50L">Less than 50L</option>
                            <option value="50L-1C">50L - 1 crore</option>
                            <option value="1C-2C">1 Crore - 2 Crore</option>
                            <option value=">2C">2 Crore ++</option>
                          </select>
                        </div> -->
                        <div class="form-group">
                          <label for="aqcampCity">City:<span>*</span></label>
                          <select required class="form-control" name="aqcampCity" id="aqcampCity">
                            <option>Select city</option>
                            <option>Chennai</option>
                            <option>Bangalore</option>
                            <option>Mumbai</option>
                          </select>
                        </div>
                        <div class="row">
                          <a href="dashboard.php" class="btn btn-primary">Cancel</a>
                          <button type="submit" name="save" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
          <?php }else{ ?>
            <div class="row">
              <div class="col-md-12">
                  <div class="page-header">
                      <h1>AQ campaign</h1>
                  </div>
                  <div class="form-group">
                      <label>AQ campaign name</label>
                      <p class="form-control-static"><?php echo $dbAqCampName; ?></p>
                  </div>
                  <div class="form-group">
                      <label>Sector</label>
                      <p class="form-control-static"><?php echo $dbAqSector; ?></p>
                  </div>
                  <div class="form-group">
                      <label>Sub sector</label>
                      <p class="form-control-static"><?php echo $dbAqSubsector; ?></p>
                  </div>
                  <div class="form-group">
                      <label>City</label>
                      <p class="form-control-static"><?php echo $dbAqCity; ?></p>
                  </div>
                  <p><a href="dashboard.php" class="btn btn-primary">Back</a></p>
              </div>
          </div>

        <?php  } /* ?>
            <div class="row">
              <a href="<?php echo $fbloginURL; ?>" class="btn btn-primary ">Connect with Facebook</a>
              <a href="<?php echo "#";//$googleloginurl; ?>" class="btn btn-primary disabled">Connect with Google</a>
            </div>
        <?php */ ?>
        </div>
    </div>
</body>
</html>
