<?php
require_once "config.php";
ini_set("error_reporting", 0);
if(isset($_GET['er']) && ($_GET['er']==1)){
   // Report all errors
  error_reporting(E_ALL);
  // Same as error_reporting(E_ALL);
  // Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);
}
// mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
// mysqli_report(MYSQLI_REPORT_ALL);
$parts = explode('/', $_SERVER["SCRIPT_NAME"]);
$file = $parts[count($parts) - 1];
$needle = "login";

// Check if the user is logged in, if not then redirect him to login page
if((!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == false) && ($file!=="login.php")){
  header("location: login.php");
  exit;
}

?>
<style type="text/css">
		.btn-info {
    float: right;
}
ul.add_acc {
    text-decoration: none;
    list-style: none;
    color: #000;
}
ul.add_acc li a{
    color: #000;
}
.btn_show_hide{display: none;}
	</style>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ads Quotient</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<?php /* ?><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script><?php */ ?>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <script type="text/javascript">
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });
$(document).ready(function(){
  $(".show_arrow").click(function(){
    $(".btn_show_hide").fadeToggle("slow");
  });
});
</script>
<style type="text/css">
.wrapper{
    width: 75%;
    margin: 0 auto;
}
.page-header h2{
    margin-top: 0;
}
table tr td:last-child a{
    margin-right: 15px;
}
  a.btn.btn-info.btn_show_hide {
    display: none;
}
i.fas.fa-angle-down {
    font-size: 25px;
    padding-left: 10px;
}
select.form-control:not([size]):not([multiple]) {
  height: auto!important;
}
.form-group span{
  color: red;
}
</style>
</head>
<body>

	<div class="container-fluid" style="margin-top: 20px">
		<div class="row">
      <div class="col-md-3 col-lg-3" >
          <a href="dashboard.php"><img src="mobile-logo.png" width="200"></a>
          <br/><br/>
      </div>
      <div class="col-md-9 show_arrow" style="text-align: right;">
        <?php if(isset($_SESSION['userData']['picture']['url'])){ ?>
    			<img style="border-radius: 50%;" src="<?php echo $_SESSION['userData']['picture']['url']; ?>"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['userData']['name'] ?><br>
    			<!-- <a href="logout.php" class="btn btn-info btn_show_hide" role="button">Logout</a> -->
        <?php } ?>
        <?php if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] == true){ ?>
        <a href="reset-password.php" class="btn btn-warning small" style="font-size:85%; background-color:#ccc; border-color:#ccc;">Reset Password</a>
        <a href="logout.php" class="btn btn-danger small" style="font-size:85%; background-color:#aaa; border-color:#aaa;">Sign out</a>
        <?php } ?>
      </div>
  </div>

  <div class="wrapper">
