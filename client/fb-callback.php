<?php
	// require_once "config.php";
	if(!session_id()) {
	    session_start();
	}
	require_once "header.php";
	try {
		$accessToken = $helper->getAccessToken();
	} catch (\Facebook\Exceptions\FacebookResponseException $e) {
		echo "Response Exception: " . $e->getMessage();
		exit();
	} catch (\Facebook\Exceptions\FacebookSDKException $e) {
		echo "SDK Exception: " . $e->getMessage();
		exit();
	} catch (Exception $e){
      echo "Ads Quotient exception: ". $e->getMessage();
      exit;
    }

		if (isset($accessToken)) {
		  // Logged in!
			$oAuth2Client = $FB->getOAuth2Client();
			if (!$accessToken->isLongLived()){
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			}
		  $_SESSION['access_token'] = (string) $accessToken;

		  // Now you can redirect to another page and use the
		  // access token from $_SESSION['facebook_access_token']
		} elseif ($helper->getError()) {
		  // The user denied the request
		  exit;
		}
	// printArray($accessToken);
	/*$response = $FB->get("/me?fields=adaccounts{business_name,balance,campaigns{ads{insights{relevance_score}},insights{campaign_name,cpc,ctr,clicks,frequency,impressions}}}", $accessToken);*/
	$response = $FB->get("/me?fields=name,email,picture{url},adaccounts.limit(100){id,account_id,name,amount_spent,currency,created_time,account_status}", $accessToken);
	/*$response = $FB->get("/me?fields=adaccounts{business_name,balance,campaigns{ads{adset,campaign,adlabels,name,adset_id,campaign_id,bid_amount,ad_review_feedback,recommendations,insights{campaign_name,cpc,ctr,clicks,frequency,impressions}}}}", $accessToken);*/
	// $response = $FB->get("/me?fields=id, name, email, picture.type(large),accounts{name,id,fan_count,access_token,picture.width(400).height(400)}", $accessToken);
	$userData = $response->getGraphNode()->asArray();
	$_SESSION['userData'] = $userData;
	// $_SESSION['access_token'] = $accessToken;
	// $_SESSION['access_token'] = (string) $accessToken;

	header('Location: fbindex.php');
	exit();
?>
