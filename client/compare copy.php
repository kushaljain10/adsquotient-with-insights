<?php
require_once 'header.php';
require_once 'functions.php';

$FBOriginalObjective = array("POST_ENGAGEMENT","LEAD_GENERATION","REACH","VIDEO_VIEWS","LINK_CLICKS");
$FBFormattedObjective = array("Post Engagement","Lead Generation","Reach","Video Views","Link Clicks");

$allData = array();

$dbfb_token = !empty($_SESSION['access_token']) ? $_SESSION['access_token'] : '';
$errMsg = '';
if(empty($dbfb_token)){
  header("location: dashboard.php");
  exit;
}

if(isset($_GET['aqid']) && !is_nan($_GET['aqid'])){
  $aqID = $_GET['aqid'];
}else{
  header('Location: dashboard.php?errormsg=No valid AQ campaign'); exit;
}
// Get values from db
$aqCampResultAry = doGetCampIdFromAQId($mysqlLink, $aqID, $_SESSION["clientid"]);
// printArray($aqCampResultAry);
if($aqCampResultAry['success']){
  $dbaqCity = !empty($aqCampResultAry['data']['aqcity']) ? $aqCampResultAry['data']['aqcity'] : 'City not available';
  $dbInsights = !empty($aqCampResultAry['data']['aqinsights']) ? $aqCampResultAry['data']['aqinsights'] : 'Insights not available';
  $dbAqCampName = !empty($aqCampResultAry['data']['aqcampname']) ? $aqCampResultAry['data']['aqcampname'] : '';
  $dbAqFbAdAccountName  = !empty($aqCampResultAry['data']["aqAdAccount"]) ? $aqCampResultAry['data']["aqAdAccount"] : '';
  $dbaqSector = !empty($aqCampResultAry['data']['aqsector']) ? $aqCampResultAry['data']['aqsector'] : 'Sector not available';
  if(!empty($aqCampResultAry['data']['campaign_id'])){
    $dbCampIdAry = explode(',',$aqCampResultAry['data']['campaign_id']);
  }
}

$debugVal = isset($_GET['debug']) ? $_GET['debug'] : false;
if(isset($_GET['aqMetric']) && isset($_GET['aqDuration']) && isset($_GET['aqCity'])){
  $aqMetric = isset($_GET['aqMetric']) ? $_GET['aqMetric'] : 'all';
  $aqDuration = isset($_GET['aqDuration']) ? $_GET['aqDuration'] : 'lifetime';
  $aqsector = isset($_GET['aqsector']) ? $_GET['aqsector'] : '';
  $aqCity = isset($_GET['aqCity']) ? $_GET['aqCity'] : 'chennai';
}
if(empty($_GET['aqMetric']))
{
  $aqMetric = 'all';
}
if(empty($_GET['aqsector']))
{
  $aqsector = $dbaqSector;
}
if(empty($_GET['aqDuration']))
{
  $aqDuration = 'lifetime';
}
if(empty($_GET['aqCity']))
{
  $aqCity = $dbaqCity;
}

// printArray($aqCampResultAry);
if(!empty($aqsector) && !empty($aqCity)){
  // Prepare a select statement
  $sql = "SELECT * FROM benchmark_details WHERE `aqsector` LIKE ? AND `city` like ? ";
  // echo "aqsector: ".$aqsector.'<br>aqCity: '. $aqCity;
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
      // Bind variables to the prepared statement as parameters
      mysqli_stmt_bind_param($stmt, "ss", $aqsector, $aqCity);

      // Attempt to execute the prepared statement
      if(mysqli_stmt_execute($stmt)){
          $result = mysqli_stmt_get_result($stmt);
          // echo __LINE__.mysqli_num_rows($result); exit;
          // if(mysqli_num_rows($result) == 1){
          if(mysqli_num_rows($result) > 1){
              /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
              while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                $benchMarkDetailsData[] = $row;
              }

          } else{
              // URL doesn't contain valid id parameter. Redirect to error page
              // header("location: error.php");
              // exit();
          }
      } else{
          echo "Oops! Something went wrong. Please try again later.";
      }
  }
}
// printArray($benchMarkDetailsData);
// $dbfb_token = "EAAFLpgouUzYBANiTVbbXZAvZCfb5ZA4GZA1omMdavabFylkCg6IW7bZALPmqT0iMxVNbKVBadNT88NEOqZBwNB1jDl5eurE59PAMIzpqK8CCCEI53oPXKHpJsXvzeFylCbdrEIxQPsxyzrM6vNG0E7QWQxccsdbe7GiZBHoTNn8UAZDZD";
if(!empty($benchMarkDetailsData)){
  foreach ($benchMarkDetailsData as $benchMarkData) {
    $benchMarkDetailsAry[$benchMarkData['objective']] = array();
    foreach ($benchMarkData as $key => $value) {
      $benchMarkDetailsAry[$benchMarkData['objective']][$key] = $value;
    }
  }
}else{
  $errMsg =  '<div class="alert alert-danger">Unable to get data!</div>';
}
// printArray($benchMarkDetailsAry);
$averageAry = array();
// Total number of campaigns selected to compare
$totalCampCnt = count($dbCampIdAry);

if(!empty($dbCampIdAry)){
  foreach($dbCampIdAry as $dbCampId){
    $fbParameters = $dbCampId."/insights?fields=campaign_name,campaign_id,objective,spend,cpc,ctr,cpp,cpm,cost_per_action_type,cost_per_conversion,cost_per_10_sec_video_view,cost_per_thruplay&date_preset=lifetime";
    try {
      $response = $FB->get($fbParameters, $dbfb_token);
      try{
        $accDataAry[] = $response->getGraphEdge()->asArray();
      } catch (\Exception $e) {
        $errMsg =  '<div class="alert alert-danger">Error occur in response:'.$e->getMessage().'</div>';
      }
    } catch (\Exception $e) {
      $errMsg =  '<div class="alert alert-danger">Error occur:'.$e->getMessage().'</div>';
    }
  }
  if(!empty($accDataAry)){
    $fbDataResultAry = doGetCampDetails1($accDataAry);
    $fbCampNameAry  = doGetFBCampName($accDataAry);
  }
  // printArray($fbDataResultAry);
}else{
  $errMsg =  '<div class="alert alert-danger">No campaigns found for this AQ dataset, please choose campaign list by clicking <a class="btn btn-danger" href="campaign_detail.php?aqid='.$aqID.'"> here.</a></div>';
}
$benchmarkFieldData = array();
?>
<div class="row">
	<div class="col-md-5">
		<a href="dashboard.php">Home</a> >>
		<a href="update.php?id=<?php echo $aqID; ?>"><?php echo $dbAqCampName;  ?></a> >> Benchmark Comparison
	</div>
</div>
<?php //printArray($fbCampNameAry); ?>
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
              <div class="clearfix">
                  <h3 class="pull-left"><?php echo "Benchmark Comparison"; ?></h3>
              </div>
          </div>
        </div>
        <?php if(!empty($dbAqFbAdAccountName)){ ?>
          <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="margin-bottom: 0;">
                  <div class="panel-body">
                    <b>Connected Ad account:</b> <?php echo $dbAqFbAdAccountName; ?><a style="color:#ffa500;" class="btn btn-link" href="fbaccounts.php?aqid=<?php echo $aqID; ?>" >Change</a>
                    <?php if(!empty($fbCampNameAry)){ ?>
                      <br >
                        <a data-toggle="collapse" id="fbCampName" href="#collapse1">View Fb campaign list</a>
                      <a style="color:#ffa500;" class="btn btn-link" href="campaign_detail.php?aqid=<?php echo $aqID; ?>">Change</a>
                      <div id="collapse1" class="panel-collapse collapse">
                        <ul class="list-group">
                          <?php foreach ($fbCampNameAry as $fbCampName) { ?>
                            <li class="list-group-item"><?php echo $fbCampName['campaign_name']; ?></li>
                          <?php }  ?>
                        </ul>
                        <!-- <div class="panel-footer"></div> -->
                      </div>
                    <?php } ?>
                  </div>
                </div>
          </div>
          </div>
        <?php }
        // echo $aqsector.__LINE__.$dbaqSector;
         ?>
        <br>
        <div class="row">
          <form action="" method="GET">
          <div class="col-md-12" style="padding-bottom: 30px;">
            <div class="col-md-2">
            <select class="form-control" required name="aqsector">
              <option value="">Choose sector</option>
              <optgroup label="Real Estate">
                <option <?php if($aqsector=='real_estate_lessthan_50l'){ echo 'selected';} ?> value="real_estate_lessthan_50l">< 50 Lakhs</option>
                <option <?php if($aqsector=='real_estate_50l_1c'){ echo 'selected';} ?> value="real_estate_50l_1c">50L - 1Crore</option>
                <option <?php if($aqsector=='real_estate_1c_2c'){ echo 'selected';} ?> value="real_estate_1c_2c">1Crore - 2 Crore</option>
                <option <?php if($aqsector=='real_estate_greaterthan_2c'){ echo 'selected';} ?> value="real_estate_greaterthan_2c">2 Crore+</option>
              </optgroup>
              <optgroup label="BFSI">
                <option <?php if($aqsector=='bfsi_mutual_funds'){ echo 'selected';} ?> value="bfsi_mutual_funds">Mutual Funds</option>
                <option <?php if($aqsector=='bfsi_insuarance'){ echo 'selected';} ?> value="bfsi_insuarance">Insuarance</option>
              </optgroup>
              <optgroup label="Retail">
                <option <?php if($aqsector=='retail_jewellery'){ echo 'selected';} ?> value="retail_jewellery">Jewellery</option>
                <option <?php if($aqsector=='retail_fashion'){ echo 'selected';} ?> value="retail_fashion">Fashion</option>
              </optgroup>
              <optgroup label="Ecommerce">
                <option <?php if($aqsector=='ecommerce_fashion'){ echo 'selected';} ?> value="ecommerce_fashion">Fashion</option>
              </optgroup>
              <optgroup label="FMCG">
                <option <?php if($aqsector=='fmcg_food'){ echo 'selected';} ?> value="fmcg_food">Food</option>
              </optgroup>
              <optgroup label="Healthcare">
                <option <?php if($aqsector=='multi_speciality'){ echo 'selected';} ?> value="multi_speciality">Multi Speciality</option>
                <option <?php if($aqsector=='single_speciality'){ echo 'selected';} ?> value="single_speciality">Single Speciality</option>
              </optgroup>
            </select>
          </div>
          <div class="col-md-2">
            <select class="form-control" required name="aqMetric">
              <option <?php if($aqMetric=='all'){ echo 'selected'; } ?> value="all">ALL</option>
              <option <?php if($aqMetric=='cpl'){ echo 'selected'; } ?> value="cpl">CPL</option>
              <option <?php if($aqMetric=='cpc'){ echo 'selected'; } ?> value="cpc">CPC</option>
              <option <?php if($aqMetric=='ctr'){ echo 'selected'; } ?> value="ctr">CTR</option>
              <option <?php if($aqMetric=='cptp'){ echo 'selected'; } ?> value="cptp">CPTP</option>
              <option <?php if($aqMetric=='cpm'){ echo 'selected'; } ?> value="cpm">CPM</option>
              <option <?php if($aqMetric=='cpe'){ echo 'selected'; } ?> value="cpe">CPE</option>
              <!-- <option <?php //if($aqMetric=='cpv'){ echo 'selected'; } ?> value="cpv">CPV</option> -->
            </select>
          </div>
          <div class="col-md-2">
            <select class="form-control" required name="aqDuration">
              <option value="">Duration</option>
              <option <?php if($aqDuration =='last_30d') {echo 'selected';} ?> value="last_30d">Last 30 days</option>
              <option <?php if($aqDuration =='last_90d') {echo 'selected';} ?> value="last_90d">Last 3 months </option>
              <option <?php if($aqDuration =='this_year') {echo 'selected';} ?> value="this_year">This year</option>
              <option <?php if($aqDuration =='lifetime') {echo 'selected';} ?> value="lifetime">Lifetime</option>
            </select>
          </div>
          <div class="col-md-2">
            <select class="form-control" required name="aqCity">
              <option value="">City</option>
              <option <?php if(strtolower($aqCity) =='all') {echo 'selected';} ?> value="all">ALL</option>
              <option <?php if(strtolower($aqCity) =='chennai') {echo 'selected';} ?> value="chennai">Chennai</option>
              <option <?php if(strtolower($aqCity) =='bangalore') {echo 'selected';} ?> value="bangalore">Bangalore </option>
              <option <?php if(strtolower($aqCity) =='mumbai') {echo 'selected';} ?> value="mumbai">Mumbai</option>
            </select>
          </div>
          <div class="col-md-2">
            <button class="btn btn-primary" type="submit">Refresh</button>
          </div>
          <input type="hidden" name="aqid" value="<?php echo $aqID; ?>">
          </div>
        </form>
        </div>
        <br/>
        <?php
        if(!empty($errMsg)){
          echo $errMsg;
        }else{
         ?>
        <?php
          // Check submitted data
          // $strGetMetric = (!empty($_GET['']))
          // printArray($_GET);
          if(isset($aqMetric) && !empty($aqMetric)){ $aqmetric_check = 1;
            switch ($aqMetric){
              case 'all':
                $tableHeader = '<th>My CPC</th><th>Benchmark CPC</th><th>% Difference</th>
                                <th>My CPM</th><th>Benchmark CPM</th><th>% Difference</th>
                                <th>My CPL</th><th>Benchmark CPL</th><th>% Difference</th>
                                <th>My CTR</th><th>Benchmark CTR</th><th>% Difference</th>
                                <th>My Cost per ThruPlay</th><th>Benchmark Cost per ThruPlay</th><th>% Difference</th>
                                <th>My CPE</th><th>Benchmark CPE</th><th>% Difference</th>';
                break;
              case 'cpc':
                $tableHeader = '<th>My CPC</th><th>Benchmark CPC</th><th>% Difference</th>';
                break;
              case 'cpm':
                $tableHeader = '<th>My CPM</th><th>Benchmark CPM</th><th>% Difference</th>';
                break;
              case 'cpl':
                $tableHeader = '<th>My CPL</th><th>Benchmark CPL</th><th>% Difference</th>';
                break;
              case 'ctr':
                $tableHeader = '<th>My CTR</th><th>Benchmark CTR</th><th>% Difference</th>';
                break;
              case 'cptp':
                $tableHeader = '<th>My Cost per ThruPlay</th><th>Benchmark Cost per ThruPlay</th><th>% Difference</th>';
                break;
              case 'cpe':
                $tableHeader = '<th>My CPE</th><th>Benchmark CPE</th><th>% Difference</th>';
                break;
            }
          }
         ?>
        <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Channel</th>
                    <th>Ad Type</th>
                    <?php echo $tableHeader; ?>
                  </tr>
                </thead>
                <tbody style="text-align: center;">
                  <?php
                    if(!empty($fbDataResultAry)){
                        foreach ($fbDataResultAry as $fbDataResult) {
                          // printArray($fbDataResult);
                          $clientCPC = !is_numeric($fbDataResult['cpc']) ? 'NA' : $fbDataResult['cpc'];
                          $cpcDiff = getDifference($clientCPC,($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPC']));

                          $clientCPM = !is_numeric($fbDataResult['cpm']) ? 'NA' : $fbDataResult['cpm'];
                          $cpmDiff =  getDifference($clientCPM,($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPM']));

                          $clientCPL = !is_numeric($fbDataResult['cpl']) ? 'NA' : $fbDataResult['cpl'];
                          $cplDiff = getDifference($clientCPL,($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPL']));

                          $clientCTR = !is_numeric($fbDataResult['ctr']) ? 'NA' : $fbDataResult['ctr'];
                          $ctrDiff =  getDifference($clientCTR,($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CTR']),'CTR');

                          $clientCPTP = !is_numeric($fbDataResult['cptp']) ? 'NA' : $fbDataResult['cptp'];
                          $cptpDiff =  getDifference($clientCPTP,($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPTP']));

                          // $clientCPV = !is_numeric($fbDataResult['cpv']) ? 'NA' : $fbDataResult['cpv'];
                          // $cpvDiff = getDifference($clientCPV,($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPV']));

                          $clientCPE = !is_numeric($fbDataResult['cpe']) ? 'NA' : $fbDataResult['cpe'];
                          $cpeDiff = getDifference($clientCPE,($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPE']));

                          $benchMarkCPC = $benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPC'] == 0 ? 'NA' : number_format($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPC'],2);
                          $benchMarkCPM = $benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPM'] == 0 ? 'NA' : number_format($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPM'],2);
                          $benchMarkCPL = $benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPL'] == 0 ? 'NA' : number_format($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPL'],2);
                          $benchMarkCTR = $benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CTR'] == 0 ? 'NA' : number_format($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CTR'],2);
                          $benchMarkCPTP = $benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPTP'] == 0 ? 'NA' : number_format($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPTP'],2);
                          $benchMarkCPV = $benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPV'] == 0 ? 'NA' : number_format($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPV'],2);
                          $benchMarkCPE = $benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPE'] == 0 ? 'NA' : number_format($benchMarkDetailsAry[strtolower($fbDataResult['obj'])]['fb_CPE'],2);

                          $finalcpcDiff =  is_numeric($cpcDiff) ? number_format($cpcDiff).'%' : 'NA';
                          $finalcpmDiff =  is_numeric($cpmDiff) ? number_format($cpmDiff).'%' : 'NA';
                          $finalcplDiff =  is_numeric($cplDiff) ? number_format($cplDiff).'%' : 'NA';
                          $finalctrDiff =  is_numeric($ctrDiff) ? number_format($ctrDiff).'%' : 'NA';
                          $finalcptpDiff =  is_numeric($cptpDiff) ? number_format($cptpDiff).'%' : 'NA';
                          $finalcpvDiff =  is_numeric($cpvDiff) ? number_format($cpvDiff).'%' : 'NA';
                          $finalcpeDiff =  is_numeric($cpeDiff) ? number_format($cpeDiff).'%' : 'NA';
                          switch ($aqMetric){
                            case 'all':
                              $tableVal = "<td>".number_format($clientCPC,2)."</td><td>".$benchMarkCPC."</td><td>".$finalcpcDiff."</td>
                                          <td>".number_format($clientCPM,2)."</td><td>".$benchMarkCPM."</td><td>".$finalcpmDiff."</td>
                                          <td>".number_format($clientCPL,2)."</td><td>".$benchMarkCPL."</td><td>".$finalcplDiff."</td>
                                          <td>".number_format($clientCTR,2)."</td><td>".$benchMarkCTR."</td><td>".$finalctrDiff."</td>
                                          <td>".number_format($clientCPTP,2)."</td><td>".$benchMarkCPTP."</td><td>".$finalcptpDiff."</td>
                                          <td>".number_format($clientCPE,2)."</td><td>".$benchMarkCPE."</td><td>".$finalcpeDiff."</td>";
                              break;
                            case 'cpc':
                              $tableVal = "<td>".number_format($clientCPC,2)."</td><td>".$benchMarkCPC."</td><td>".$finalcpcDiff."</td>";
                              break;
                            case 'cpm':
                              $tableVal = "<td>".number_format($clientCPM,2)."</td><td>".$benchMarkCPM."</td><td>".$finalcpmDiff."</td>";
                              break;
                            case 'cpl':
                              $tableVal = "<td>".number_format($clientCPL,2)."</td><td>".$benchMarkCPL."</td><td>".$finalcplDiff."</td>";
                              break;
                            case 'ctr':
                              $tableVal = "<td>".number_format($clientCTR,2)."</td><td>".$benchMarkCTR."</td><td>".$finalcplDiff."</td>";
                              break;
                            case 'cptp':
                              $tableVal = "<td>".number_format($clientCPTP,2)."</td><td>".$benchMarkCPTP."</td><td>".$finalcplDiff."</td>";
                              break;
                            // case 'cpv':
                            //   $tableVal = "<td>".number_format($clientCPV,2)."</td><td>".$benchMarkCPV."</td><td>".$finalcpvDiff."</td>";
                            //   break;
                            case 'cpe':
                              $tableVal = "<td>".number_format($clientCPE,2)."</td><td>".$benchMarkCPE."</td><td>".$finalcpvDiff."</td>";
                              break;
                          }
                            // printArray($fbDataResult);
                            $FBObjective = str_replace($FBOriginalObjective, $FBFormattedObjective, $fbDataResult['obj']);
                            echo "<tr>";
                            echo "<td>Facebook</td>";
                            echo "<td>".$FBObjective."</td>";
                            echo $tableVal;
                            echo "</tr>";

                            $allData["$FBObjective"] = array('objective' => "$FBObjective", 'cpc' => number_format($clientCPC,2), 'b_cpc' => $benchMarkCPC, 'cpm' => number_format($clientCPM,2), 'b_cpm' => $benchMarkCPM, 'cpl' => number_format($clientCPL,2), 'b_cpl' => $benchMarkCPL, 'ctr' => number_format($clientCTR,2), 'b_ctr' => $benchMarkCTR, 'cptp' => number_format($clientCPTP,2), 'b_cptp' => $benchMarkCPTP, 'cpe' => number_format($clientCPE,2), 'b_cpe' => $benchMarkCPE);
                        }
                    }
                   ?>
                  </tbody>
                </table>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-info">
              <div class="panel-heading"><center>INSIGHTS</center></div>
                <?php
                  if(!empty($allData)){
                    foreach ($allData as $campaignData){
                      echo '<ul class="list-group">';
                      echo '<li class="list-group-item list-group-item-info"><b>' . $campaignData['objective'] . '</b></li>';
                      echo insights($mysqlLink, $aqMetric, $campaignData);
                      echo '</ul>';
                      }
                  }
                  else{
                    echo 'No insights found.';
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
</div>
<?php if($debugVal){ ?>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Raw data</h4>
        <p>Copy the content and share with us to debug</p>
        <!-- <button onclick="copyText()">Copy text</button> -->
      </div>
      <div class="modal-body">
        <p><?php if(!empty($accDataAry)){ printArray($accDataAry); }else{echo 'No data';} ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Raw data</button>
<?php } ?>
</body>
</html>
<style>
th{text-align: center;}
.aqcls-up,.aqcls-down{
  text-align: center;
  background-size: 14% !important;
  background-position: 81% 35%;
  background-repeat: no-repeat;
}
.aqcls-up{
  background-image: url('images/Green_Arrow_Top-512.png');
}
.aqcls-down{
  background-image: url('images/Red_Arrow_Down-512.png');
}
.wrapper{
  width: 100%;
}
table tr th{
  font-size: 13px;
}
</style>
<script>
function copyText() {
  /* Get the text field */
  var copyText = document.getElementById("copyText");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  // alert("Copied the text: " + copyText.value);
  alert("Copied the text: ");
}
</script>
<?php include 'footer.php'; ?>
