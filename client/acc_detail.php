<?php
require_once "config.php";
include_once 'header.php';
require_once 'functions.php';
// $dateRange = !empty($_GET["date_filter"]) ? $_GET["date_filter"] : "last_90d";
$dateRange = !empty($_GET["date_filter"]) ? $_GET["date_filter"] : "lifetime";
$fbAccountID = '';
$sql = "SELECT fb_account_id,id FROM fb_details where id_client = ?";
if($stmt = mysqli_prepare($mysqlLink, $sql)){
	/* bind variables to the prepared statement as parameters */
	mysqli_stmt_bind_param($stmt, "i", $_SESSION["clientid"]);
	/* execute query */
	mysqli_stmt_execute($stmt);
	// echo mysqli_stmt_error($stmt);
	mysqli_stmt_store_result($stmt);
	/* bind result variables */
	mysqli_stmt_bind_result($stmt,$fbAccountID,$id);
	/* fetch value */
	mysqli_stmt_fetch($stmt);
	// Update Account id if FB account id is not updated
	if(!empty($id) ){
		$updateSql = "UPDATE fb_details SET fb_account_id = ? WHERE id = ?";
		if($stmt = mysqli_prepare($mysqlLink, $updateSql)){
			mysqli_stmt_bind_param($stmt, "si", $_GET['acc_id'],$id );
			if(!mysqli_stmt_execute($stmt)){
					echo $stmt->error." Something went wrong while updating results. Please try again later.";
			}else {
				header('Location: dashboard.php?msg=FB account selected'); exit;
			}
		}else{
			echo $stmt->error;
		}
	}
}else{
	print "Failed to prepare statement\n";
}
// $fbAccountID = doGetFBAccIDFromClient($mysqlLink, $_SESSION["clientid"]);
// if(empty($fbAccountID)){
	// header('Location: index.php'); exit;
// }
if (!isset($_SESSION['access_token'])) {
	 header('Location: login.php');
	exit();
}
$city = !empty($_REQUEST['city']) ? strtolower($_REQUEST['city']) : 'chennai';
$sector = !empty($_REQUEST['sector']) ? strtolower($_REQUEST['sector']) : 're';

$acces_to = $_SESSION['access_token'];
$fbParameters = $fbAccountID."/campaigns?fields=name,objective,id,status,insights.date_preset(".$dateRange."){ad_name,campaign_name,cpc,ctr,cpm,clicks, frequency,impressions,objective,reach,spend}&limit=250";
$response = $FB->get($fbParameters, $acces_to);
// $response = $FB->get($fbAccountID."/campaigns?fields=insights.date_preset(".$dateRange."){ad_name, campaign_name,cpc,ctr,cpm,clicks, frequency,impressions,objective,reach,spend}", $acces_to);
if(!empty($response)){
	$accFirstPageData = $response->getGraphEdge();
	$accData = $accFirstPageData->asArray();
	$metaData = $accFirstPageData->getMetaData();
}
?>
<form method="POST" action="" class="filter_form">
	<div class="row">
		<div class="col-md-3">
			<a href="dashboard.php">Home</a> >> <a href="fbindex.php">Ad account</a> >> Detail
		</div>
	</div>
<div class="form-inline">
	<div class="row">
		<div class="form-group col-md-6 col-xs-6 col-lg-6">
		 	<div class="form-group">
						<select name="date_filter" class="form-control select" style="width: auto">
							<option value="">Select Date Range</option>
							<option <?php if($dateRange == "last_3d") echo "selected"; ?> value="last_3d">Last 3 days</option>
							<option <?php if($dateRange == "last_7d") echo "selected"; ?> value="last_7d">Last 7 days</option>
							<option <?php if($dateRange == "last_30d") echo "selected"; ?> value="last_30d">Last 30 days</option>
							<option <?php if($dateRange == "last_90d") echo "selected"; ?> value="last_90d">Last 90 days</option>
							<option <?php if($dateRange == "last_quarter") echo "selected"; ?> value="last_quarter">Last quarter</option>
							<option <?php if($dateRange == "lifetime") echo "selected"; ?> value="lifetime">Lifetime</option>
						</select>
						<select class="form-control" required name="sector">
							<option value="">Choose sector</option>
							<optgroup label="Real Estate">
						    <option value="lessthan_50l">< 50 Lakhs</option>
						    <option value="50l_1c">50L - 1Crore</option>
						    <option value="1c_2c">1Crore - 2 Crore</option>
						    <option value="greaterthan_2c">2 Crore+</option>
						  </optgroup>
						  <optgroup label="BFSI">
						    <option value="bfsi_mutual_funds">Mutual Funds</option>
						    <option value="bfsi_insuarance">Insuarance</option>
						  </optgroup>
						  <optgroup label="Retail">
						    <option value="retail_jewellery">Jewellery</option>
						    <option value="retail_fashion">Fashion</option>
						  </optgroup>
						  <optgroup label="Ecommerce">
						    <option value="ecommerce_fashion">Fashion</option>
						  </optgroup>
						  <optgroup label="FMCG">
						    <option value="fmcg_food">Food</option>
						  </optgroup>
						  <optgroup label="Healthcare">
						    <option value="multi_speciality">Multi Speciality</option>
						    <option value="single_speciality">Single Speciality</option>
						  </optgroup>
						</select>

						<select class="form-control" id="city" name="city" required>
							<option value="">Select City</option>
							<option value="chennai">Chennai</option>
							<option value="bangalore">Bangalore</option>
							<option value="mumbai">Mumbai</option>
						</select>
						<input type="hidden" name="acc_id" value="<?php echo $fbAccountID; ?>">
			</div>
		</div>
			<div class="col-md-2 col-xs-6 col-lg-2">
				<input class="btn btn-primary" value="Compare selected" name="frmCampSubmit" id="frmCampSubmit" type="submit"/>
			</div>
		</div>
	</div>
<table class="table table-hover">
  <tr>
		<th>Select</th>
    <th>Campaign Name</th>
		<th>Objective</th>
    <th>Spend</th>
		<th>Status</th>
 </tr>
<?php
if(!empty($accData)){
	foreach ($accData as $campdata) {
		if(!empty($campdata)){  ?>
		  <tr>
				<?php $data_link = "name=".urlencode($campdata['insights'][0]['campaign_name'])."&camp_id=".$campdata["id"]."&platform=Facebook&obj=".urlencode($campdata['insights'][0]['objective'])."&cpc=".urlencode($campdata['insights'][0]['cpc'])."&ctr=".urlencode($campdata['insights'][0]['ctr']);
				?>
				<td><input data-toggle="tooltip" type="checkbox" <?php if(!in_array($campdata['objective'], $CAMP_OBJECTIVE_ARRAY)){?> disabled  title="This objective will available in next Phase of AdsQuotient." <?php } ?> name="campaignsid[]" value="<?php echo $campdata['id']; ?>"></td>
				<td><?php echo $campdata['name']; ?></td>
				<td><?php echo $campdata['objective']; ?></td>
				<td><?php echo $campdata['insights'][0]['spend']; ?></td>
				<td><?php if(in_array($campdata['objective'], $CAMP_OBJECTIVE_ARRAY)){ echo $campdata['status']; }else{ echo "Objective unavailable";}?></td>
		  </tr>
	<?php }
	}
}else{ ?>
<tr>
    <td colspan="10">No data found</td>
</tr>
<?php }
// echo $nextPageResponse = $FB->next($accFirstPageData);
// echo $prevPageResponse = $FB->previous($accFirstPageData);
 ?>
</table>
</form>
</div>
</body>
</html>
<!-- <script>
$(function() {
  $('input[name=campIdAry]').on('change', function() {
    $('#campaignsid').val($('input[name=campIdAry]:checked').map(function() {
      return this.value;
    }).get());
  });
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
});
</script> -->

<?php include 'footer.php'; ?>
