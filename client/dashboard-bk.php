<?php
require_once "header.php";
?>
<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style type="text/css">
        .wrapper{
            width: 75%;
            margin: 0 auto;
        }
        .page-header h2{
            margin-top: 0;
        }
        table tr td:last-child a{
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>
<body>
  <div class="page-header">
        <h1>Ads quotient</h1>
    </div>-->
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Campaigns Details</h2>
                        <a href="create.php" class="btn btn-success pull-right">Add New Campaigns</a>
                    </div>
                    <?php

                    // Attempt select query execution
                    $sql = "SELECT * FROM campaigns";
                    if($result = mysqli_query($mysqlLink, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            echo "<table class='table table-bordered table-striped'>";
                                echo "<thead>";
                                    echo "<tr>";
                                        echo "<th>#</th>";
                                        echo "<th>AQ Campign Name</th>";
                                        echo "<th>Sector</th>";
                                        echo "<th>Sub sector</th>";
                                        echo "<th>City</th>";
                                        echo "<th>Campign Name</th>";
                                        echo "<th>Objective</th>";
                                        echo "<th>Platform</th>";
                                        echo "<th>Action</th>";
                                    echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                                $k=1;
                                while($row = mysqli_fetch_array($result)){
                                    echo "<tr>";
                                        echo "<td>" . $k . "</td>";
                                        echo "<td>" . $row['aqcampname'] . "</td>";
                                        echo "<td>" . $row['aqsector'] . "</td>";
                                        echo "<td>" . $row['aqsubsector'] . "</td>";
                                        echo "<td>" . $row['aqcity'] . "</td>";
                                        echo "<td>" . $row['campaign_name'] . "</td>";
                                        echo "<td>" . $row['objective'] . "</td>";
                                        echo "<td>" . $row['platform'] . "</td>";
                                        echo "<td>";
                                            echo "<a href='update.php?id=". $row['id'] ."' title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                                            echo "<a href='delete.php?campid=". $row['id'] ."' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                                            echo "<a href='compare.php?id=". $row['id'] ."' title='Compare' data-toggle='tooltip'><span class='glyphicon glyphicon-eye-open'></span></a>";
                                        echo "</td>";
                                    echo "</tr>";
                                    $k++;
                                }
                                echo "</tbody>";
                            echo "</table>";
                            // Free result set
                            mysqli_free_result($result);
                        } else{
                            echo "<p class='lead'><em>No records were found.</em></p>";
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqlLink);
                    }

                    // Close connection
                    mysqli_close($mysqlLink);
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
