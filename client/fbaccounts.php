<?php
require_once 'config.php';
require_once 'header.php';
require_once 'functions.php';

// Processing form data when form is submitted
if(isset($_GET["acc_id"])&&!empty($_GET["acc_id"])){
	$sql = "UPDATE campaigns SET `fb_ad_account` = ?, `fb_ad_account_name` = ?, `campaign_id` = '' WHERE `id`= ?; ";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "ssi", $param_ad_account, $param_ad_account_name, $param_id);

        // Set parameters
        $param_ad_account = $_GET['acc_id'];
        $param_ad_account_name = $_GET['adname'];
        $param_id = $_GET['aqid'];
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            echo '<div class="alert alert-success">AQ campaign created, go back to add Facebook campaigns by click <a href="dashboard.php">Here!</a></div>';
            // Records created successfully. Redirect to landing page
            // header("location: index.php");
            header("location: campaign_detail.php?aqid=".$_GET['aqid']);
            exit();
        } else{
            echo "Something went wrong. Please try again later.";
        }
        // printf("Error: %s.\n", $stmt->error);
    }
}

try {
		$accessToken = $helper->getAccessToken();
	} catch (\Facebook\Exceptions\FacebookResponseException $e) {
		echo "Response Exception: " . $e->getMessage();
		exit();
	} catch (\Facebook\Exceptions\FacebookSDKException $e) {
		echo "SDK Exception: " . $e->getMessage();
		exit();
	} catch (Exception $e){
      echo "Ads Quotient exception: ". $e->getMessage();
      exit;
    }


$accessToken = $_SESSION['access_token'];
$aqid = $_GET['aqid'];

if($aqid=='')
{
	header("location: error.php");
	exit();
}
// Prepare a select statement
$sql = "SELECT * FROM campaigns WHERE id = ?";

if($stmt = mysqli_prepare($mysqlLink, $sql)){
    // Bind variables to the prepared statement as parameters
    mysqli_stmt_bind_param($stmt, "i", $param_id);

    // Set parameters
    $param_id = $aqid;

    // Attempt to execute the prepared statement
    if(mysqli_stmt_execute($stmt)){
        $result = mysqli_stmt_get_result($stmt);

        if(mysqli_num_rows($result) == 1){
            /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

            // Retrieve individual field value
            $dbAqCampName   = !empty($row["aqcampname"]) ? $row["aqcampname"] : '';
            $dbAqSector     = !empty($row["aqsector"]) ? $row["aqsector"] : '';
            $dbAqCity       = !empty($row["aqcity"]) ? $row["aqcity"] : '';
            $dbAqInsights   = !empty($row["aqinsights"]) ? $row["aqinsights"] : '';
            $dbFbAdAccount   = !empty($row["fb_ad_account"]) ? $row["fb_ad_account"] : '';

        } else{
        	// URL doesn't contain valid id parameter. Redirect to error page
            header("location: error.php");
            exit();
        }

    } else{
        echo "Oops! Something went wrong. Please try again later.";
    }
}

$response = $FB->get("/me?fields=name,email,picture{url},adaccounts.limit(300){id,account_id,name,amount_spent,currency,created_time,account_status}", $accessToken);
$userData = $response->getGraphNode()->asArray();
// printArray($userData);
?>
<div class="row">
	<div class="col-md-5">
		<a href="dashboard.php">Home</a> >>
		<a href="update.php?id=<?php echo $aqid; ?>"><?php echo $dbAqCampName;  ?></a> >> FB Ad account list
	</div>
</div>
<div class="row">
	<h2>Facebook Ad account list</h2>
</div>
<!-- <div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>AQ campaign name</label>
					<p class="form-control-static"><?php echo $dbAqCampName; ?></p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Sector</label>
					<p class="form-control-static"><?php echo $dbAqSector; ?></p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>City</label>
					<p class="form-control-static"><?php echo $dbAqCity; ?></p>
				</div>
			</div>
		</div>
	</div>
</div> -->
<div class="row">
<?php if(!empty($userData['adaccounts'])){ ?>
		<table class="table table-hover table-bordered">
		<tbody>
			<tr>
			    <th style="text-align: center;">Name</th>
			    <th style="text-align: center;">Currency</th>
			    <th style="text-align: center;">Total amount spent</th>
			    <th style="text-align: center;">Account status</th>
			    <th style="text-align: center;">Action</th>
			</tr>
	  		<?php $i = 0;
    	if(!empty($userData['adaccounts'])){
					foreach($userData['adaccounts'] as $acc){
						$acc_num  = $acc["id"];
						$acc_name  = $acc["name"];
						$acc_status  = $acc["account_status"];
						switch ($acc_status) {
							case "1":
								$acc_status_term = "ACTIVE";
								$acc_status_msg = "Select";
								$btn = "btn btn-primary";
								$btn_status = "";
								break;

							case "2":
								$acc_status_term = "DISABLED";
								$acc_status_msg = "DISABLED";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "3":
								$acc_status_term = "UNSETTLED";
								$acc_status_msg = "UNSETTLED";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "7":
								$acc_status_term = "PENDING_RISK_REVIEW";
								$acc_status_msg = "PENDING RISK REVIEW";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "8":
								$acc_status_term = "PENDING_SETTLEMENT";
								$acc_status_msg = "PENDING SETTLEMENT";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "9":
								$acc_status_term = "IN_GRACE_PERIOD";
								$acc_status_msg = "IN GRACE PERIOD";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "100":
								$acc_status_term = "PENDING_CLOSURE";
								$acc_status_msg = "PENDING CLOSURE";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "101":
								$acc_status_term = "CLOSED";
								$acc_status_msg = "CLOSED";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "201":
								$acc_status_term = "ANY_ACTIVE";
								$acc_status_msg = "ANY ACTIVE";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							case "202":
								$acc_status_term = "ANY_CLOSED";
								$acc_status_msg = "ANY CLOSED";
								$btn = "btn btn-danger";
								$btn_status = "disabled";
								break;

							default:
								$acc_status_term = "UNKNOW ERROR";
								$acc_status_msg = "UNKNOW ERROR";
								$btn = "btn btn-primary";
								$btn_status = "disabled";
						}

						 ?>
							<tr>
            <td><?php echo $acc_name; ?></td>
            <td><?php if($acc["currency"]){echo $acc["currency"]; } ?></td>
            <td><?php if($acc["amount_spent"]){echo currentFormat($acc["amount_spent"]); }else{ $acc_status_msg = "No Spends";$btn = "btn btn-danger";$btn_status = "disabled";} ?></td>
            <td><?php echo $acc_status_msg; ?></td>
						<?php
							$returnurl = $_SERVER['REQUEST_URI'].'&acc_id='.$acc_num.'&adname='.urlencode($acc_name);
						 ?>
						<?php /* ?><td><a class="<?php echo $btn; ?>" <?php echo $btn_status; ?> <?php if($dbFbAdAccount!='' && $dbFbAdAccount!=$acc_num){ ?> onclick="return confirm('Changing ad account will remove all selected campaigns. Do you want to continue?');" <?php } ?> title='<?php echo $acc_status_msg; ?>' data-toggle='tooltip' href="<?php echo $returnurl; ?>"><?php echo $acc_status_msg; ?></a></td> <?php */ ?>
						<?php if($dbFbAdAccount==$acc_num){ ?>
							<td><a class="btn btn-success" title='Selected ad account' disabled data-toggle='tooltip' href="#">Already selected</a></td>
						<?php }else{ ?>
							<td><a class="<?php echo $btn; ?>" <?php echo $btn_status; ?>  onclick="return confirm('Changing ad account will remove all selected campaigns. Do you want to continue?');" title='<?php echo $acc_status_msg; ?>' data-toggle='tooltip' href="<?php echo $returnurl; ?>"><?php echo $acc_status_msg; ?></a></td>
						<?php	} ?>
							</tr>
					<?php
					$i++;
					}
		 		}  ?>
		</tbody>
	</table>
<?php } ?>
</div>
