<?php
if(empty($_GET["camp_id"])){
	header('Location: index.php'); exit;
}
require_once "config.php";
require_once 'header.php';


if(!session_id()) {
    session_start();
}
if (!isset($_SESSION['access_token'])) {
	 header('Location: login.php');
	exit();
}
$acces_to = $_SESSION['access_token'];
$response = $FB->get($_GET['camp_id']."?fields=insights{ad_name, campaign_name,cpc,ctr,cpm,clicks, frequency,impressions,objective,reach,spend},adsets{insights{adset_name}},ads{insights{relevance_score}}", $acces_to);

$accData = $response->getGraphNode()->asArray();
?>
<table class="camp_detail">
  <tr>
    <th>Campign Name</th>
    <th>CPC</th>
    <th>CTR</th>
    <th>CPM</th>
    <th>Clicks</th>
    <th>Frequency</th>
    <th>Impressions</th>
    <th>Objective</th>
    <th>Reach</th>
    <th>Spend</th>
    <th>Relevance Score</th>
    <th>Adset name</th>
  </tr>
<!-- <input type="text" id="config-demo" class="form-control"> -->

<?php if($accData['insights'][0]['campaign_name']){ ?>
		  <tr>
		    <td><?php echo $accData['insights'][0]['campaign_name']; ?></td>
		    <td><?php echo $accData['insights'][0]['cpc']; ?></td>
		    <td><?php echo $accData['insights'][0]['ctr']; ?></td>
		    <td><?php echo $accData['insights'][0]['cpm']; ?></td>
		    <td><?php echo $accData['insights'][0]['clicks']; ?></td>
		    <td><?php echo $accData['insights'][0]['frequency']; ?></td>
		    <td><?php echo $accData['insights'][0]['impressions']; ?></td>
		    <td><?php echo $accData['insights'][0]['objective']; ?></td>
		    <td><?php echo $accData['insights'][0]['reach']; ?></td>
		    <td><?php echo $accData['insights'][0]['spend']; ?></td>
		    <td><?php echo $accData['ads'][0]['insights'][0]['relevance_score']['score']; ?></td>
		    <td><?php echo $accData['adsets'][0]['insights'][0]['adset_name']; ?></td>
		  </tr>

		<?php }else{ ?>

<tr><td colspan="12">No data Found</td></tr>
		<?php } ?>
</table>
</div>
</body>
</html>
<?php include 'footer.php'; ?>
