<?php
require_once "config.php";
require_once 'header.php';
require_once 'functions.php';

  $acces_to = !empty($_SESSION['access_token']) ? $_SESSION['access_token'] : '';
  if(!empty($acces_to)){
    header('Location: dashboard.php?msg=facebook-connected');
 	  exit();
  }else{
    $dataBtn = '<a href="fbconnect.php" class="align-middle btn btn-info" role="button">Connect with Facebook</a>';
  }
  echo $dataBtn;
 ?>
