<?php
require_once "config.php";
include_once 'header.php';
require_once 'functions.php';
// error_reporting(-1);
// $dateRange = !empty($_GET["date_filter"]) ? $_GET["date_filter"] : "last_90d";
$dateRange = !empty($_GET["date_filter"]) ? $_GET["date_filter"] : "lifetime";
// $resultAry = doGetFBAccIDFromClient($mysqlLink, $_SESSION["clientid"]);
if(isset($_GET['aqid']) && !is_nan($_GET['aqid'])){
	$aqID = $_GET['aqid'];
}else{
	header('Location: dashboard.php?errormsg=No valid AQ campaign'); exit;
}
$errMsg = '';
$resultAry = doGetFBAccIDFromCampaignID($mysqlLink, $aqID);
// printArray($resultAry);
if($resultAry['success']){
	$_SESSION['client']['access_token'] = $acces_to = $resultAry['data']['fbtoken'];
	$_SESSION['client']['account_id'] = $fbAccountID = $resultAry['data']['fbAccountID'];
}else{
	echo $resultAry['msg'];
	// header('Location: index.php'); exit;
}
$msgPanel = '';
if(isset($_POST['frmCampSubmit']) && !empty($_POST['frmCampSubmit'])){
	if(isset($_POST['campaignsid']) && !empty($_POST['campaignsid'])){
		$dbCampId = implode(',',$_POST['campaignsid']);
		// update details to database
		$updateSql = "UPDATE campaigns SET campaign_id = ? WHERE id = ?";
		if($stmt = mysqli_prepare($mysqlLink, $updateSql)){
			mysqli_stmt_bind_param($stmt, "si", $dbCampId, $aqID );
			if(mysqli_stmt_execute($stmt)){
				 $msgPanel =  '<div class="alert alert-success">Fb campaign list updated</div>';
			}else{
				$msgPanel =  '<div class="alert alert-danger"> Something went wrong while execution. Please try again later.</div>';
			 }
		}else{
			$msgPanel =  '<div class="alert alert-danger"> Error while fetching data.</div>';
		}
	}else{
		$msgPanel =  '<div class="alert alert-danger"> Please choose campaign.</div>';
	}
}
// Get values from db
$aqCampResultAry = doGetCampIdFromAQId($mysqlLink, $aqID, $_SESSION["clientid"]);
// printArray($aqCampResultAry);
$dbAqCampName =  !empty($aqCampResultAry['data']['aqcampname']) ? $aqCampResultAry['data']['aqcampname'] : '';
$dbAqFbAdAccountName =  !empty($aqCampResultAry['data']['aqAdAccount']) ? $aqCampResultAry['data']['aqAdAccount'] : '';
if($aqCampResultAry['success'] && !empty($aqCampResultAry['data']['campaign_id'])){
	$dbCampId = explode(',',$aqCampResultAry['data']['campaign_id'] );
}
// $city = !empty($_REQUEST['city']) ? strtolower($_REQUEST['city']) : 'chennai';
// $sector = !empty($_REQUEST['sector']) ? strtolower($_REQUEST['sector']) : 're';
if(!empty($acces_to)){
	if(!empty($acces_to) && !empty($fbAccountID)){
		// $fbParameters = $fbAccountID."/campaigns?fields=name,objective,id,status,insights.date_preset(".$dateRange."){ad_name,campaign_name,cpc,ctr,cpm,clicks, frequency,impressions,objective,reach,spend}&limit=350";
		$fbParameters = $fbAccountID."/campaigns?fields=name,objective,id,status,insights.date_preset(".$dateRange."){ad_name,campaign_name,objective,spend}&limit=350";
		// echo "<br>".$acces_to;
		try {
			$response = $FB->get($fbParameters, $acces_to);
		} catch (\Exception $e) {
			echo "Ads Quotient exception: ". $e->getMessage();
		}
		// $response = $FB->get($fbAccountID."/campaigns?fields=insights.date_preset(".$dateRange."){ad_name, campaign_name,cpc,ctr,cpm,clicks, frequency,impressions,objective,reach,spend}", $acces_to);
		if(!empty($response)){
			$accFirstPageData = $response->getGraphEdge();
			// printArray($accFirstPageData);
			$accData = $accFirstPageData->asArray();
			// $metaData = $accFirstPageData->getMetaData();
		}
	}else{
		$btn = !empty($fbAccountID) ? 'Change ' : 'Add ';
		$errMsg = "Link Fb Ad account <a href='fbaccounts.php?aqid=".$aqID."' class='btn btn-warning'> ".$btn."FB ad account</a>";
	}
}else{
	$errMsg = "Facebook connection error!";
}
?>
<form method="POST" action="" class="filter_form">
<div class="row">
	<div class="col-md-5">
		<a href="dashboard.php">Home</a> >>
		<a href="update.php?id=<?php echo $aqID; ?>"><?php echo $dbAqCampName;  ?></a> >> FB Campaign list
	</div>
</div>
<?php if(!empty($dbAqFbAdAccountName)){ ?>
	<div class="panel panel-default" style="margin-bottom: 0;">
		<div class="panel-body"><b>Connected Ad account:</b> <?php echo $dbAqFbAdAccountName; ?><a style="color:#ffa500;" class="btn btn-link" href="fbaccounts.php?aqid=<?php echo $aqID; ?>">Change FB Ad account</a></div>
	</div>
<?php } ?>
<div class="form-inline">
	<div class="row">
		<h3>Facebook campaign list</h3>
	</div>
	<div class="row">
			<?php if(!empty($fbAccountID)){ ?>
			<div class="col-md-2 col-xs-6 col-lg-2">
				<input class="btn btn-primary" value="Save FB campaign list" name="frmCampSubmit" id="frmCampSubmit" type="submit"/>
			</div>
		<?php } ?>
			<!-- <div class="col-md-3 col-xs-6 col-lg-2">
				<a href="fbaccounts.php?aqid=<?php echo $aqID; ?>" class="btn btn-warning"><?php echo !empty($fbAccountID) ? 'Change' : 'Add'; ?> FB ad account</a>
			</div> -->
			<?php if(!empty($dbCampId)){ ?>
				<div class="col-md-2 col-xs-6 col-lg-2">
					<a href="compare.php?aqid=<?php echo $aqID; ?>" class="btn btn-success">Compare</a>
				</div>
			<?php } ?>
		</div>
		<?php if(!empty($msgPanel)){ ?>
		<div class="row">
			<p><?php echo $msgPanel; ?></p>
		</div>
	<?php } ?>
	</div>
<?php if(empty($errMsg)){ ?>
<table class="table table-hover">
  <tr>
		<!-- <th><input type="checkbox" id="selectAll" />Select All</th> -->
		<th>Select</th>
    <th>Campaign Name</th>
		<th>Objective</th>
    <th>Spend</th>
		<th>Status</th>
 </tr>
<?php
if(!empty($accData)){
	foreach ($accData as $campdata) {
		// printArray($campdata);
		echo "<br>".$campdata['id']."/insights?fields=campaign_name,campaign_id,objective,spend,cpc,ctr,cpp,cpm,cost_per_action_type,cost_per_conversion,cost_per_10_sec_video_view,cost_per_thruplay&date_preset=lifetime";

		if(!empty($campdata)){  ?>
		  <tr>
				<?php $data_link = "name=".urlencode($campdata['insights'][0]['campaign_name'])."&camp_id=".$campdata["id"]."&platform=Facebook&obj=".urlencode($campdata['insights'][0]['objective'])."&cpc=".urlencode($campdata['insights'][0]['cpc'])."&ctr=".urlencode($campdata['insights'][0]['ctr']);
				?>
				<td><input data-toggle="tooltip" type="checkbox" <?php if(!in_array($campdata['objective'], $CAMP_OBJECTIVE_ARRAY)){?>
					 disabled  title="This objective will available in next Phase of AdsQuotient."
				 <?php } ?> <?php if(in_array($campdata["id"], $dbCampId)){ echo "checked='checked'"; } ?> name="campaignsid[]" value="<?php echo $campdata['id']; ?>">
			 </td>
				<td><?php echo $campdata['name']; ?></td>
				<td><?php echo $campdata['objective']; ?></td>
				<td><?php echo $campdata['insights'][0]['spend']; ?></td>
				<td><?php if(in_array($campdata['objective'], $CAMP_OBJECTIVE_ARRAY)){ echo $campdata['status']; }else{ echo "Objective unavailable";}?></td>
		  </tr>
	<?php }
	}
	// printArray($batch);
}else{ ?>
<tr>
    <td colspan="10">No data found</td>
</tr>
<?php }
// echo $nextPageResponse = $FB->next($accFirstPageData);
// echo $prevPageResponse = $FB->previous($accFirstPageData);
 ?>
</table>
<?php }else{ ?>
	<div class="clearfix">
		<br>
		<div class="panel panel-warning">
			<div class="panel-heading">Let's get started</div>
			<div class="panel-body"><?php echo $errMsg; ?></div>
	</div>
</div>
<?php } ?>
</form>
</div>
</body>
</html>
<script>
// function valthis() {
// 	if($("input[type='checkbox']:checked").length > 0){
// 		if($("input[type='checkbox']:checked").length > 50){
// 			alert("Maximum campaign should be less than 50");
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	}else{
// 		alert("Please, choose a campaign to save!");
// 		return false;
// 	}
// }
$('#selectAll').click(function(e){
    var table= $(e.target).closest('table');
		$('td input:checkbox',table).prop('checked',this.checked);
});
</script>
<?php include 'footer.php'; ?>
