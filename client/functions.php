<?PHP

if(!function_exists(printArray)){
  function printArray($str){
    echo "<pre>";
    print_r($str);
    echo "</pre>";
  }
}

function getObjectiveMetrics($campaignDataArr,$debug=NULL){
    $campDetailsAry = array();
    $campaignData = $campaignDataArr[0];
    $objective = $campaignData['objective'];
    if(!empty($objective)){
      // echo 'objective: '.$objective.'<br>';
      // printArray($campaignDataArr);
      $actionType = "";
      switch ($objective){
        case "LINK_CLICKS":
          $result = doGetdetailsFromFBData($campaignData['cost_per_action_type'],"landing_page_view");
          break;

        case "REACH":
          $result = !empty($campaignData['cpp']) ? $campaignData['cpp'] : '';
          // $result = doGetdetailsFromFBData($campaignData['cost_per_action_type'],"reach");
          break;

        case "LEAD_GENERATION":
          $result = doGetdetailsFromFBData($campaignData['cost_per_action_type'],"leadgen.other");
          break;

        case "VIDEO_VIEWS":
          $result = doGetdetailsFromFBData($campaignData['cost_per_thruplay'],"video_view");
          // printArray($campaignData);
          break;

        case "POST_ENGAGEMENT":
          $result = doGetdetailsFromFBData($campaignData['cost_per_action_type'],"post_engagement");
          break;

        // case "PRODUCT_CATALOG_SALES":
        //   $result = doGetdetailsFromFBData($campaignData['cost_per_action_type'],"post_engagement");
        //   break;
      }
      // To debug the data
      if($debug){
        $campDetailsAry = array("campaign_name"=>$campaignData["campaign_name"],"campaign_id"=>$campaignData["campaign_id"],"objective"=>$campaignData["objective"],"cost_per_result"=>$result,"raw_data"=>$campaignData);
      }else{
        $campDetailsAry = array("objective"=>$campaignData["objective"],"cost_per_result"=>$result);
      }
      return $campDetailsAry;
  }
}
function doGetCPC($dataAry){
  $returnData = '';
  if(!empty($dataAry)){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_action_type'], 'link_click');
  }
  return $returnData;
}
function doGetCPM($dataAry){
  $returnData = '';
  if(!empty($dataAry)){
    $returnData = !empty($dataAry[0]['cpm']) ? $dataAry[0]['cpm'] : '';
  }
  return $returnData;
}
function doGetCPL($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_action_type'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_action_type'], 'leadgen.other');
  }
  return $returnData;
}
function doGetCTR($dataAry){
  $returnData = '';
  if(!empty($dataAry)){
    $returnData = !empty($dataAry[0]['ctr']) ? $dataAry[0]['ctr'] : '';
  }
  return $returnData;
}
function doGetCPV($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_thruplay'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_thruplay'], 'video_view');
  }
  return $returnData;
}
function doGetCPTP($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_thruplay'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_thruplay'], 'video_view');
  }
  return $returnData;
}
function doGetCPE($dataAry){
  $returnData = '';
  if(!empty($dataAry[0]['cost_per_action_type'])){
    $returnData = doGetdetailsFromFBData($dataAry[0]['cost_per_action_type'], 'post_engagement');
  }
  return $returnData;
}
function doGetCampDetails($campaignDataArr){
  $fbResultAry = array();
  if(is_array($campaignDataArr) && !empty($campaignDataArr)){
    foreach ($campaignDataArr as $campaignData) {
      // printArray($campaignData);
      $fbCPC = $fbCPM = $fbCPL = $fbCTR = $fbCPTP = $fbCPV = $fbCPE = '';
      $fbCPC = doGetCPC($campaignData);
      $fbCPM = doGetCPM($campaignData);
      $fbCPL = doGetCPL($campaignData);
      $fbCTR = doGetCTR($campaignData);
      $fbCPTP = doGetCPTP($campaignData);
      $fbCPV = doGetCPV($campaignData);
      $fbCPE = doGetCPE($campaignData);
      // $fbCTR = doGetCTR();
      $fbObjective = !empty($campaignData[0]['objective']) ? $campaignData[0]['objective'] : '';
      $fbCampName = $campaignData[0]['campaign_name'];
      $fbCampId = $campaignData[0]['campaign_id'];
      $fbAry[] = array('campaign_name'=>$fbCampName,'campaign_id'=>$fbCampId,'objective'=>$fbObjective,'cpc'=>$fbCPC,'cpm'=>$fbCPM,'cpl'=>$fbCPL,'ctr'=>$fbCTR,'cptp'=>$fbCPTP,'cpv'=>$fbCPV,'cpe'=>$fbCPE);
    }
    // Get average data based on objective
    $fbResultAry = doGetAverageData($fbAry,'objective');
  }
  return $fbResultAry;
}
function doGetAverageData($fbDataAry,$param){
  // $key = array_search($metricKey, array_column($dataAry, 'action_type'));
  $result = array();
  $resultAry = array();
  if(is_array($fbDataAry) && !empty($fbDataAry)){
    $objAry = array_unique(array_column($fbDataAry,'objective'));
    foreach($objAry as $obj){
      $cpcAry = $cpmAry = $cplAry = $ctrAry = $cptpAry = $cpvAry = $cpeAry = array();
      foreach($fbDataAry as $fbdata){
        if($fbdata['objective'] == $obj ){
          $cpcAry[] = $fbdata['cpc'];
          $cpmAry[] = $fbdata['cpm'];
          $cplAry[] = $fbdata['cpl'];
          $ctrAry[] = $fbdata['ctr'];
          $cptpAry[] = $fbdata['cptp'];
          $cpvAry[] = $fbdata['cpv'];
          $cpeAry[] = $fbdata['cpe'];
        }
      }
      $count = count($cpcAry);
      // $cpc_sum = array_sum($cpcAry);
      $cpc_average = array_sum($cpcAry) / $count;
      $cpm_average = array_sum($cpmAry) / $count;
      $cpl_average = array_sum($cplAry) / $count;
      $ctr_average = array_sum($ctrAry) / $count;
      $cptp_average = array_sum($cptpAry) / $count;
      $cpv_average = array_sum($cpvAry) / $count;
      $cpe_average = array_sum($cpeAry) / $count;

      $resultAry[] = array ('obj'=>$obj, 'cpc'=>$cpc_average, 'cpm'=>$cpm_average, 'cpl'=>$cpl_average, 'ctr'=>$ctr_average, 'cptp'=>$cptp_average, 'cpv'=>$cpv_average, 'cpe'=>$cpe_average);
    }
    // printArray($full_array);
  }
  return $resultAry;
}
function doGetFBCampName($campaignDataArr){
  $fbResultAry = array();
  if(is_array($campaignDataArr) && !empty($campaignDataArr)){
    foreach ($campaignDataArr as $campaignData) {
      // printArray($campaignData);
      $fbCampName = $campaignData[0]['campaign_name'];
      $fbCampId = $campaignData[0]['campaign_id'];
      $fbAry[] = array('campaign_name'=>$fbCampName,'campaign_id'=>$fbCampId);
    }
  }
  $fbResultAry = $fbAry;
  return $fbResultAry;
}
function doGetCampDetails1($campaignDataArr){
  $fbResultAry = array();
  if(is_array($campaignDataArr) && !empty($campaignDataArr)){
    // printArray($campaignDataArr);
    foreach ($campaignDataArr as $campaignData) {
      if(!empty($campaignData)){
        // printArray($campaignData);
        $fbCPC = $fbCPM = $fbCPL = $fbCTR = $fbCPTP = $fbCPV = $fbCPE = '';
        $fbCPC = doGetCPC($campaignData);
        $fbCPM = doGetCPM($campaignData);
        $fbCPL = doGetCPL($campaignData);
        $fbCTR = doGetCTR($campaignData);
        $fbCPTP = doGetCPTP($campaignData);
        $fbCPV = doGetCPV($campaignData);
        $fbCPE = doGetCPE($campaignData);
        // $fbCTR = doGetCTR();
        $fbObjective = !empty($campaignData[0]['objective']) ? $campaignData[0]['objective'] : '';
        $fbCampName = $campaignData[0]['campaign_name'];
        $fbCampId = $campaignData[0]['campaign_id'];
        $fbAry[] = array('campaign_name'=>$fbCampName,'campaign_id'=>$fbCampId,'objective'=>$fbObjective,'cpc'=>$fbCPC,'cpm'=>$fbCPM,'cpl'=>$fbCPL,'ctr'=>$fbCTR,'cptp'=>$fbCPTP,'cpv'=>$fbCPV,'cpe'=>$fbCPE);
      }
    }
    // Get average data based on objective
    $fbResultAry = doGetAverageData1($fbAry,'objective');
  }
  return $fbResultAry;
}
function doGetAverageData1($fbDataAry,$param){
  // $key = array_search($metricKey, array_column($dataAry, 'action_type'));
  $result = array();
  $resultAry = array();
  if(is_array($fbDataAry) && !empty($fbDataAry)){
    $objAry = array_unique(array_column($fbDataAry,'objective'));
    foreach($objAry as $obj){
      $cpcAry = $cpmAry = $cplAry = $ctrAry = $cptpAry = $cpvAry = $cpeAry = array();
      foreach($fbDataAry as $fbdata){
        if($fbdata['objective'] == $obj ){
          $cpcAry[] = $fbdata['cpc'];
          $cpmAry[] = $fbdata['cpm'];
          $cplAry[] = $fbdata['cpl'];
          $ctrAry[] = $fbdata['ctr'];
          $cptpAry[] = $fbdata['cptp'];
          $cpvAry[] = $fbdata['cpv'];
          $cpeAry[] = $fbdata['cpe'];
        }
      }
      $count = count($cpcAry);
      // $cpc_sum = array_sum($cpcAry);
      $cpc_average = array_sum($cpcAry) / $count;
      $cpm_average = array_sum($cpmAry) / $count;
      $cpl_average = array_sum($cplAry) / $count;
      $ctr_average = array_sum($ctrAry) / $count;
      $cptp_average = array_sum($cptpAry) / $count;
      $cpv_average = array_sum($cpvAry) / $count;
      $cpe_average = array_sum($cpeAry) / $count;

      $resultAry[] = array ('obj'=>$obj, 'cpc'=>$cpc_average, 'cpm'=>$cpm_average, 'cpl'=>$cpl_average, 'ctr'=>$ctr_average, 'cptp'=>$cptp_average, 'cpv'=>$cpv_average, 'cpe'=>$cpe_average);
    }
    // printArray($full_array);
  }
  return $resultAry;
}

function myFunction($value, $key, $extraParam){
  echo "The $key $extraParam $value <br>";
}
// getObjectiveMetrics($campaignData);
function doGetdetailsFromFBData($dataAry, $metricKey){
  $result = null;
  if(!empty($dataAry) && !empty($metricKey)){
    $key = array_search($metricKey, array_column($dataAry, 'action_type'));
    $keyValue = $dataAry[$key];
    $result = floatval($keyValue['value']);
  }
  return $result;
}

function doGetFBAccIDFromClient($mysqlLink, $clientID){
  $resultAry = array("success"=>false,"msg"=>'Inital error message',"data"=>array());
  $sql = "SELECT fb_access_token,fb_account_id FROM fb_details where id_client = ?";
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
  	/* bind variables to the prepared statement as parameters */
  	mysqli_stmt_bind_param($stmt, "i", $clientID);
  	/* execute query */
  	mysqli_stmt_execute($stmt);
  	// echo mysqli_stmt_error($stmt);
  	mysqli_stmt_store_result($stmt);

  	if(mysqli_stmt_num_rows($stmt)>0){
  		/* bind result variables */
  		mysqli_stmt_bind_result($stmt, $fbToken, $fbAccountID);
  		/* fetch value */
  		mysqli_stmt_fetch($stmt);
      $resultAry = array("success"=>true,"msg"=>"","data"=>array('fbtoken'=>$fbToken,'fbAccountID'=>$fbAccountID));
  	}else{
      $resultAry = array("success"=>false,"msg"=>"No fb details found","data"=>array());
  	}
  }else{
    $resultAry = array("success"=>false,"msg"=>"Failed to prepare statement","data"=>array());
  }
  return $resultAry;
}

function doGetFBAccIDFromCampaignID($mysqlLink, $aqID){
  $resultAry = array("success"=>false,"msg"=>'Inital error message',"data"=>array());
  $sql = "SELECT fb_details.fb_access_token,campaigns.fb_ad_account FROM fb_details JOIN campaigns ON fb_details.id_client = campaigns.client_id WHERE campaigns.id = ?";
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
    /* bind variables to the prepared statement as parameters */
    mysqli_stmt_bind_param($stmt, "i", $aqID);
    /* execute query */
    mysqli_stmt_execute($stmt);
    // echo mysqli_stmt_error($stmt);
    mysqli_stmt_store_result($stmt);

    if(mysqli_stmt_num_rows($stmt)>0){
      /* bind result variables */
      mysqli_stmt_bind_result($stmt, $fbToken, $fbAccountID);
      /* fetch value */
      mysqli_stmt_fetch($stmt);
      $resultAry = array("success"=>true,"msg"=>"","data"=>array('fbtoken'=>$fbToken,'fbAccountID'=>$fbAccountID));
    }else{
      $resultAry = array("success"=>false,"msg"=>"No fb details found","data"=>array());
    }
  }else{
    $resultAry = array("success"=>false,"msg"=>"Failed to prepare statement","data"=>array());
  }
  return $resultAry;
}

// Get fb campaign id(s) from AQ campaign id
function doGetCampIdFromAQId($mysqlLink, $aqID, $clientID){
  $resultAry = array("success"=>false,"msg"=>'Inital error message',"data"=>array());
  $sql = "SELECT campaign_id,platform,aqcampname,aqsector,aqcity,aqinsights,fb_ad_account_name FROM campaigns where id = ? AND client_id = ?";
  if($stmt = mysqli_prepare($mysqlLink, $sql)){
  	/* bind variables to the prepared statement as parameters */
  	mysqli_stmt_bind_param($stmt, "ii", $aqID, $clientID);
  	/* execute query */
  	mysqli_stmt_execute($stmt);
  	// echo mysqli_stmt_error($stmt);
  	mysqli_stmt_store_result($stmt);
  	if(mysqli_stmt_num_rows($stmt)>0){
  		/* bind result variables */
  		mysqli_stmt_bind_result($stmt, $campaign_id, $platform, $aqcampname, $aqsector, $aqcity ,$aqinsights,$aqAdAccount);
  		/* fetch value */
  		mysqli_stmt_fetch($stmt);
      $resultAry = array("success"=>true,"msg"=>"","data"=>array('clientID'=>$clientID,'campaign_id'=>$campaign_id, 'platform'=>$platform,'aqcampname'=>$aqcampname,'aqsector'=>$aqsector,'aqcity'=>$aqcity,'aqAdAccount'=>$aqAdAccount,'aqinsights'=>$aqinsights));
  	}else{
      $resultAry = array("success"=>false,"msg"=>"No data found","data"=>array());
  	}
  }else{
    $resultAry = array("success"=>false,"msg"=>"Failed to prepare statement","data"=>array());
  }
  return $resultAry;
}
// Add paise to amount spent
function currentFormat($amount){
  $result = null;
  if(is_numeric($amount)){
    $resultVal = substr($amount,0,-2).'.'.substr($amount,-2);
    $result = number_format($resultVal,2);
  }
  return $result;
}

function getDifference($val1, $val2, $val3=NULL){
  if(!is_numeric($val1) || !is_numeric($val2)){
    $diff = 'NA';
  }else{
    $val1 = number_format($val1,2);
    $val2 = number_format($val2,2);
    if(!empty($actual['actual_name']) && $actual['actual_name'] == 'CTR'){
      $diff = number_format(((($val1/$val2) -1) * 100), 2, '.','');
    }else{
      $diff = number_format(((1 - ($val1/$val2)) * 100), 2, '.','');
    }
  }
  return $diff;
}

function insights($mysqlLink, $campaignData){
  $str = '';
  $categories = array();
  foreach($campaignData['metrics'] as $metric){
    if($campaignData[$metric] == 'NA'){
      array_pop($categories);
    } else {
      $sql="SELECT * from insights_codenames where actual_name = '$metric' limit 1";
      $res = mysqli_query($mysqlLink, $sql);
      $row=mysqli_fetch_array($res,MYSQLI_ASSOC);
      $codename = $row['codename'];
      ${$codename} = $campaignData[$metric];
      array_push($categories, $row['category']);
    }
  }
  $categories = array_unique($categories);
  foreach($categories as $category){
    $sql = "SELECT * from insights_formulas where category = '$category'";
    $res = mysqli_query($mysqlLink, $sql);
    $sql_metric = "SELECT * from insights_codenames where category = '$category'";
    $res_metric = mysqli_query($mysqlLink, $sql_metric);
    $actual = mysqli_fetch_array($res_metric,MYSQLI_ASSOC);
    $benchmark = mysqli_fetch_array($res_metric,MYSQLI_ASSOC);

    if($actual['actual_name'] == 'ctr'){
      $diff = number_format((((${$actual['codename']}/${$benchmark['codename']}) -1) * 100), 2, '.','');
    }else{
      $diff = number_format(((1 - (${$actual['codename']}/${$benchmark['codename']})) * 100), 2, '.','');
    }
    if($diff<0){
      $diff = '('.number_format(($diff*-1)).'% decrease compared to benchmark.)';
    }
    else {
      $diff = '('.number_format($diff).'% increase compared to benchmark.)';
    }
    //$diff = number_format($diff).'%';
    //echo $diff;

    if ($res){
      while ($row=mysqli_fetch_array($res,MYSQLI_ASSOC)){
        $formula = !empty($row['formula']) ? $row['formula'] : '';
        $insight = !empty($row['insight']) ? $row['insight'] : 'NA';
        $val = eval("return " . $formula . ";");
        if($val){
          $str = $str . '<li class="list-group-item">'.
          '<div class="row"><div class="col-md-1"><h5><b>'
          .strtoupper($actual['actual_name']).
          '</b></h5></div><div class="col-md-11">'
          . $insight .
          '<br><b>'.$diff.'</b>'.
          '</div></li>';
        }
      }
    }
  }
  if($str == '')
    return '<li class="list-group-item">NA</li>';
  return $str;
}

// function insights($mysqlLink, $aqMetric, $campaignData){
//   $str = '';
//   if($aqMetric == 'all'){
//     $sql="SELECT * from insights";
//   }
//   else{
//     $sql="SELECT * from insights where category = '$aqMetric'";
//   }
//   $res = mysqli_query($mysqlLink, $sql);
//   if ($res)
//   {   
//       while ($row=mysqli_fetch_array($res,MYSQLI_ASSOC)){
//           $formula = !empty($row['formula']) ? $row['formula'] : '';
//           $stringData = !empty($row['string']) ? $row['string'] : 'NA';
//           $assignment = !empty($row['assignments']) ? $row['assignments'] : '';
//           eval($assignment);
//           $val = eval("return " . $formula . ";");
//           if($val){
//               if($formula[0]=='1')
//                 $str = $str . '<li class="list-group-item list-group-item-success">' . $stringData . '</li>';
//               else
//                 $str = $str . '<li class="list-group-item list-group-item-danger">' . $stringData . '</li>';
//           }
//       }
//   }
//   return $str;    
// }
?>
