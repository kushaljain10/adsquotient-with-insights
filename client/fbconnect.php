<?PHP
// Include config file
require_once "header.php";

$redirectURL = "https://localhost/adsquotient/client/fb-callback.php";
//$redirectURL = "https://www.adsquotient.com/client/fb-callback.php";
$permissions = ['ads_management'];
$fbloginURL = $helper->getLoginUrl($redirectURL, $permissions);
header("Location:".$fbloginURL);
?>
