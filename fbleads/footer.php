<section class="footer">
	<div class="col-12 col-md-12 col-sm-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-md-6 col-sm-6">
				<p class="left"> Copyright © 2019 Ads Quotient <span class="sep"> | </span>
				<a href="https://www.adsquotient.com/privacy-policy/" class="footlinks" target="_blank">Privacy Policy</a><span class="sep"> | </span>
			<a href="https://www.adsquotient.com/terms-and-conditions/" class="footlinks" target="_blank">Terms And Conditions</a></p>
				<p></p>
			</div>
			<!-- <div class="col-12 col-md-2 col-sm-2"></div> -->
			<div class="col-12 col-md-3 col-sm-3"></div>
			<div class="col-12 col-md-3 col-sm-3">
				<p class="right"><a href="https://www.socialbeat.in/">PRODUCT OF SOCIAL BEAT</a></p>
			</div>
		</div>
	</div>
		
</section>