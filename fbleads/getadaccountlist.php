<?php 
require_once "config.php";
//require_once "dbConfig.php";
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
//$conn = new mysqli($servername,$username,$password,$dbname);
/*Get project name list from database*/
//$strProjectList = doGetAllProjectNameFromDb($conn);
if(isset($_REQUEST["page_id"]) && !empty($_REQUEST["page_id"])){
	/*Add profile access token to database */
	$strProfileAccessToken = !empty($_SESSION["access_token"]) ? $_SESSION["access_token"] : "";
	/*Add access token to datatbase*/
	// doAddAccessToken($conn,$strProfileAccessToken);

	// printArray($_SESSION);
	$strSelectedPageId = $_REQUEST["page_id"];
	if(!empty($_SESSION["userData"])){
		$strSelectedPageToken = $sess = "";
		if(!empty($_SESSION["userData"]["accounts"])){
			foreach($_SESSION["userData"]["accounts"] as $sess){
				if($sess["id"]==$strSelectedPageId){
					$strSelectedPageToken = $sess["access_token"];
					$updatePageToken = doupdatePageToken($conn, $strSelectedPageId, $strSelectedPageToken,$_SESSION);
				}
			}
		}
	}else{
		$strSelectedPageToken = doGetPageTokenByPageId($conn, $strSelectedPageId);
	}
	$accessToken = $strSelectedPageToken;
	// printArray($accessToken);
	/*Get page Id and get page token from Page id to fetch list of forms*/
	if(!empty($accessToken)){
		/*Get form details from forms using page access token*/
		try {
		  // Returns a `FacebookFacebookResponse` object
		  $pageFormListresponseObj = $FB->get(
		    '/'.$strSelectedPageId.'/?fields=id,name,access_token,address,adaccounts{campaigns{insights{cost_per_action_type,clicks,ctr,frequency,relevance_score,impressions}}}',
	        $accessToken
		  );
		} catch(FacebookExceptionsFacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(FacebookExceptionsFacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		} catch(Exception $e){
		  echo "Assetz page accessToken returned an error: " . $e->getMessage();
		  exit;
		}
		$formdetails = ($pageFormListresponseObj->getDecodedBody());
		//printArray($formdetails);
	}
}else{
	echo "Invalid input";
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form lists</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row justify-content-center">
			<h2 style="padding:20px 0;">List of Forms from <span style="color:rgba(51, 103, 214, 4);"><?php echo $formdetails["name"]; ?></span></h2>
			<div class="col-md-12">
				<table class="table table-hover table-bordered">
					<thead>
						<th>Name</th>
						<th>Status</th>
						<th>Lead count</th>
						<!-- <th>Csv download</th> -->
						<th>Project name</th>
						<th class="text-center">Sync status</th>
						<th class="text-center">Action</th>
					</thead>
					<tbody>
						<?php 
							if(!empty($formdetails)){
								$formId = "";
								$syncStatus = $formSyncStatus = "";
								foreach ($formdetails["leadgen_forms"]["data"] as $forms) {
										$formId = $forms["id"];
										$formSyncStatus = doCheckFormSync($conn, $formId);
										$selectedProjectId = !empty($formSyncStatus) ? $formSyncStatus[0]["project_id"] : "";
										$projSyncStatus = !empty($selectedProjectId) ? "Synced" : "Not sync";
								 ?>
								<tr>
									<td><?php echo $forms["name"]; ?></td>
									<td><?php echo $forms["status"]; ?></td>
									<td><?php echo $forms["leads_count"]; ?></td>
									<!-- <td><a href="<?php //echo $forms["leadgen_export_csv_url"]; ?>" target="_blank">Download</td> -->
									<td>
										<select id="<?php echo $formId; ?>">
											<option value="">Choose projects</option>
											<?php if(!empty($strProjectList)){
												$projList = "";
												foreach ($strProjectList as $projList) { 
												if(!empty($projList["name"])){ ?>
													<option <?php if($selectedProjectId ==$projList["id"]){ ?> selected <?php } ?> value="<?php echo $projList["id"]; ?>"><?php echo $projList["name"]; ?></option>
												<?php } 
												} 
											} ?>
										</select>
										<input type="hidden" name="hd<?php echo $formId; ?>" id="hd<?php echo $formId; ?>" value="<?php echo $selectedProjectId; ?>">
                                        <input type="hidden" name="fb_page_id" id="fb_page_id" value="<?php echo $strSelectedPageId; ?>">
									</td>
									<td class="text-center"><button id="sync<?php echo $formId; ?>" class="btn <?php if(empty($selectedProjectId)){ echo "btn-warning";}else{echo "btn-success";} ?> "><?php echo $projSyncStatus; ?></button></td>
									<!-- <td><a href="getLeadsDetailsList.php?form-id=<?php echo $formId; ?>">Connect</a></td> -->
									<td class="text-center"><button id="sendCRM<?php echo $formId; ?>" onClick="doSendToCrm('<?php echo $formId; ?>');" class="btn btn-info">Send to CRM</button> 
										<?php if(empty($selectedProjectId)){ ?>
											 <button id="authorizeStatus<?php echo $formId; ?>" onClick="doAuthorize('<?php echo $formId; ?>');" class="btn btn-info">Authorize</button> 
										<?php }else{ ?>
											<button id="authorizeStatus<?php echo $formId; ?>" onClick="doDeAuthorize('<?php echo $formId; ?>');" class="btn btn-danger">Deauthorize</button> 
										<?php } ?>
									</td>
								</tr>
								<?php }
							}
						 ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $('select').on('change', function() {
	  var formid = $(this).attr("id");
	   $("#hd"+formid).val(this.value);
	  
	});
});

function doSendToCrm(id){
	var projectId = $("#hd"+id).val();
	if(projectId==''){
		alert('Choose a project to this form belongs!');
		return false;
	}else{
		var confirmation = confirm("Are you sure to push lead details to CRM?");
		var formId = $("#"+id).val();
		var projectId = $("#hd"+id).val();
		var pageId = $("#fb_page_id").val();
		if(confirmation==true){
			$.ajax({
		           url: "doSendLeadsToCRM.php",
		           type: "POST",
		           data: {
		               formId: id,
		               projectId: projectId,
					   pageId:pageId,
		               action: "doPushLeads"
		           },
		           dataType: "JSON",
		           success: function (jsonStr) {
		               //alert(jsonStr["msg"]);
		               if(jsonStr["msg"] != ''){
		               		$("#sendCRM"+id).html("Sent");
			               $("#sendCRM"+id).removeClass("btn-info").addClass("btn-success");
		               }
		               // console.log(JSON.stringify(jsonStr));
		               // $("#result").text(JSON.stringify(jsonStr));
		           }
		       });
		}else{
			return false;
		}
	}
}

function doSendCRM(id){
	/*Check project is selected or not*/
	var updatedName = document.getElementById("frmUpdatedName"+id).value;
	if(updatedName==""){
		alert("Please enter name to send!");
		return false;
	}else{

	}
}
function doAuthorize(id){
	var confirmation = confirm("Are you sure to Authorize for this FB Form and Project?");
	if(confirmation==true){
		var formId = $("#"+id).val();
		var projectId = $("#hd"+id).val();
		var pageId = $("#fb_page_id").val();
		//var pageId = $("#fb_page_id").val();
		if(projectId==''){
			alert('Choose a project to this form belongs!');
			return false;
		}else{
	       $.ajax({
	           url: "doAuthorize.php",
	           type: "POST",
	           data: {
	               formId: id,
	               projectId: $("#hd"+id).val(),
				   pageId:pageId,
	               action: "authorize"
	           },
	           dataType: "JSON",
	           success: function (jsonStr) {
	               //alert(jsonStr["msg"]);
	               if(jsonStr["msg"] != ''){
	               		$("#hd"+id).val();
		               $("#authorizeStatus"+id).removeClass("btn-info").addClass("btn-danger");
		               $("#authorizeStatus"+id).html("Deauthorize");
		               $("#sync"+id).removeClass("btn-warning").addClass("btn-success");
		               $("#sync"+id).html("Synced");
		               $("#authorizeStatus"+id).attr("onclick","doDeAuthorize('"+id+"')");
	               }
	               // console.log(JSON.stringify(jsonStr));
	               // $("#result").text(JSON.stringify(jsonStr));
	           }
	       });
		}
	}
}
function doDeAuthorize(id){
	var confirmation = confirm("Are you sure to Deauthorize for this FB Form and Project and it wont push leads to CRM?");
	if(confirmation==true){
		var formId = $("#"+id).val();
		var projectId = $("#hd"+id).val();
		if(projectId==''){
			alert('Choose a project to this form belongs!');
			return false;
		}else{
	       $.ajax({
	           url: "doDeAuthorize.php",
	           type: "POST",
	           data: {
	               formId: id,
	               projectId: $("#hd"+id).val(),
	               action: "deauthorize"
	           },
	           dataType: "JSON",
	           success: function (jsonStr) {
	               //alert(jsonStr["msg"]);
	               $("#authorizeStatus"+id).removeClass("btn-danger").addClass("btn-info");
	               $("#authorizeStatus"+id).html("Authorize");
	               $("#hd"+id).val("");
	               $("#"+id).val("");
	               $("#sync"+id).addClass("btn-warning");
	               $("#sync"+id).html("Not sync");
	               $("#authorizeStatus"+id).attr("onclick","doAuthorize('"+id+"')");
	               // $("#result").text(JSON.stringify(jsonStr));
	           }
	       });
		}
	}else{
		return false;
	}
}
</script>