<?php
if(empty($_GET["acc_id"])){
	header('Location: index.php'); exit;
}
require_once "config.php";
include_once 'header.php';
$dateRange = !empty($_GET["date_filter"]) ? $_GET["date_filter"] : "last_90d";

if (!isset($_SESSION['access_token'])) {
	 header('Location: login.php');
	exit();
}
$acces_to = $_SESSION['access_token'];
$response = $FB->get($_GET['acc_id']."/campaigns?fields=insights.date_preset(".$dateRange."){ad_name, campaign_name,cpc,ctr,cpm,clicks, frequency,impressions,objective,reach,spend}", $acces_to);
//$response = $FB->get($_GET['acc_id']."/campaigns?fields=insights.date_preset(".$dateRange."){campaign_name,campaign_id}", $acces_to);
$accData = $response->getGraphEdge()->asArray();
?>
<form method="get" action="" class="filter_form">
	<div class="row">
		<div class="col-3">
			<a href="https://www.adsquotient.com/fbleads/">Home</a> >> Detail
		</div>
		<div class="col-8">
		 	<div class="clearfix">
				<div class="filter float-right">
						<select name="date_filter" class="form-control select" style="width: auto">
							<option value="">Select Date Range</option>
							<option <?php if($dateRange == "last_3d") echo "selected"; ?> value="last_3d">Last 3 days</option>
							<option <?php if($dateRange == "last_7d") echo "selected"; ?> value="last_7d">Last 7 days</option>
							<option <?php if($dateRange == "last_30d") echo "selected"; ?> value="last_30d">Last 30 days</option>
							<option <?php if($dateRange == "last_90d") echo "selected"; ?> value="last_90d">Last 90 days</option>
						</select>
						<input type="hidden" name="acc_id" value="<?php echo $_GET['acc_id']; ?>"> 
				</div>
			</div>
		</div>
		<div class="col-1">
			<input type="submit" name="Filter" value="Filter" class="btn btn-info">
		</div>
	</div>
</form>
</div>
	<div class="row"></div>

<table class="table table-hover">
  <tr>
    <th>Campign Name</th>
    <th>CPC</th>
    <th>CTR</th>
    <th>CPM</th>
    <th>Clicks</th>
    <th>Frequency</th>
    <th>Impressions</th>
    <th>Objective</th>
    <th>Reach</th>
    <th>Spend</th>
 </tr>
<?php //print_r($accData);
if(!empty($accData[0]['insights'][0]['campaign_name'])){
	foreach ($accData as $campdata) {
	
		if(!empty($campdata['insights'][0]['campaign_name'])){  ?>
		  <tr>
		    <?php /* ?><td><a href="campaign_detail.php?camp_id=<?php echo $campdata['insights'][0]['campaign_id']; ?>"><?php echo $campdata['insights'][0]['campaign_name']; ?></a></td> <?php */ ?>
		    <td><?php echo $campdata['insights'][0]['campaign_name']; ?></td>
		    <td><?php echo $campdata['insights'][0]['cpc']; ?></td>
		    <td><?php echo $campdata['insights'][0]['ctr']; ?></td>
		    <td><?php echo $campdata['insights'][0]['cpm']; ?></td>
		    <td><?php echo $campdata['insights'][0]['clicks']; ?></td>
		    <td><?php echo $campdata['insights'][0]['frequency']; ?></td>
		    <td><?php echo $campdata['insights'][0]['impressions']; ?></td>
		    <td><?php echo $campdata['insights'][0]['objective']; ?></td>
		    <td><?php echo $campdata['insights'][0]['reach']; ?></td>
		    <td><?php echo $campdata['insights'][0]['spend']; ?></td>
		  </tr>
	<?php } 
	}
}else{ ?>
<tr>
    <td colspan="10">No data found</td>
</tr>
<?php } ?>
</table>
</div>
</body>
</html>
<?php include 'footer.php'; ?>