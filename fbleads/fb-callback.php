<?php
	require_once "config.php";
	try {
		$accessToken = $helper->getAccessToken();		
	} catch (\Facebook\Exceptions\FacebookResponseException $e) {
		echo "Response Exception: " . $e->getMessage();
		exit();
	} catch (\Facebook\Exceptions\FacebookSDKException $e) {
		echo "SDK Exception: " . $e->getMessage();
		exit();
	} catch (Exception $e){
        echo "Assetz exception: ". $e->getMessage();
        exit;
      }

	if (!$accessToken) {
		//header('Location: login.php');
		echo "Access token missing";
		header('Location:fbleads/login.php');
		exit();
	}

	$oAuth2Client = $FB->getOAuth2Client();
	if (!$accessToken->isLongLived())
		$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		/*$response = $FB->get("/me?fields=adaccounts{business_name,balance,campaigns{ads{insights{relevance_score}},insights{campaign_name,cpc,ctr,clicks,frequency,impressions}}}", $accessToken);*/
		$response = $FB->get("/me?fields=name,email,picture{url},adaccounts{business_name,account_id,name}", $accessToken);
		/*$response = $FB->get("/me?fields=adaccounts{business_name,balance,campaigns{ads{adset,campaign,adlabels,name,adset_id,campaign_id,bid_amount,ad_review_feedback,recommendations,insights{campaign_name,cpc,ctr,clicks,frequency,impressions}}}}", $accessToken);*/
		// $response = $FB->get("/me?fields=id, name, email, picture.type(large),accounts{name,id,fan_count,access_token,picture.width(400).height(400)}", $accessToken);
		$userData = $response->getGraphNode()->asArray();
	$_SESSION['userData'] = $userData;
	$_SESSION['access_to'] = $accessToken;
	$_SESSION['access_token'] = (string) $accessToken;
	
	header('Location: index.php');
	//header('Location:https://assetzpropertybangalore.in/fbleads/fb-callback.php');
	exit();
?>