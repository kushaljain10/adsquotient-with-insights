<?php
if(!session_id()) {
    session_start();
}
if (!isset($_SESSION['access_token'])) {
	 header('Location: login.php');
	exit();
}
include 'header.php'; ?>
<div class="row">
	
		<?php if(!empty($_SESSION['userData']['adaccounts'])){ ?>
			
 			<table class="table table-hover table-bordered">
				<tbody>
					
					<tr>
					    <th style="text-align: center;">Business Name</th>
					    <th style="text-align: center;">Account Name</th>
					</tr>
					
			  		<?php $i = 0;
			  	
			        	if(!empty($_SESSION['userData']['adaccounts'])){
							foreach($_SESSION['userData']['adaccounts'] as $acc){
								$acc_num= $acc["id"]; ?>
									<tr>
										<td><a href="acc_detail.php?acc_id=<?php echo $acc_num; ?>"><?php if($acc["business_name"]){echo $acc["business_name"]; } ?></a></td>
										<td><a href="acc_detail.php?acc_id=<?php echo $acc_num; ?>"><?php if($acc["name"]){ echo $acc["name"];
										} ?></a></td>
									</tr>
							
							<?php 
							$i++;
							}
				 		}  ?>
				</tbody>
			</table>
		<?php } ?>
	</div>
</body>
</html>

<?php include 'footer.php'; ?>
