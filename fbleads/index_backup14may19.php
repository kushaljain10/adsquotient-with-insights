<?php
if(!session_id()) {
    session_start();
}
if (!isset($_SESSION['access_token'])) {
	 header('Location: login.php');
	exit();
}
/*echo "<pre>";
print_r($_SESSION);
echo "</pre>";*/
//require_once "dbConfig.php";
/*$conn = new mysqli($servername,$username,$password,$dbname);
	if (!isset($_SESSION['access_token'])) {
		header('Location: login.php');
		exit();
	}else{ 
		echo "<pre>";
		print_r($_SESSION);
		echo "</pre>";
		$fbUserId = isset($_SESSION["userData"]) ? $_SESSION["userData"]["id"] : "";
		if(!empty($fbUserId)){
			// Add or update fb profile details
			$updateUserInfo = doUpdateUserInfo($conn,$_SESSION);
		}*/
?>
	<style type="text/css">
		.btn-info {
    margin-right: 50px;
    float: right;
}
	</style>
	<a href="logout.php" class="btn btn-info" role="button">Logout</a>
	
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FB Ads details</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
	<div class="container" style="margin-top: 20px">
		<div class="row">
    
    <div class="col-md-3 col-lg-3" >
            <img src="mobile-logo.png" width="200">
            <br/><br/>
            </div>
            <div class="col-md-9">
				<table class="table table-hover table-bordered">
					<tbody>
						<tr>
							<td>ID</td>
							<td><?php echo $_SESSION['userData']['id'] ?></td>
						</tr>
						<tr>
							<td>Name</td>
							<td><?php echo $_SESSION['userData']['name'] ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?php echo $_SESSION['userData']['email'] ?></td>
						</tr>
                        </tbody>
						</table>
			</div>
     
     </div>
		<div class="row justify-content-center">
			<div class="col-md-3">
				<img style="border-radius: 50%;" src="<?php echo $_SESSION['userData']['picture']['url'] ?>">
			</div>            
		</div>
        <div>
        <?php /*?><h2>List of your Facebook Pages        </h2><?php */?>
        <h4>Facebook Ads Account Details</h4>
        </div>
        <div class="row">
        <table class="table table-hover table-bordered">
					<tbody>
      
            	<?php if(!empty($_SESSION['userData']['adaccounts'])){
						foreach($_SESSION['userData']['adaccounts'] as $acc){	
							if(!empty($acc["campaigns"])){
						?>
                             
                               <td colspan="5"><b>Account name: </b><?php echo $acc["business_name"]; ?></td>
                                <td colspan="2" style="text-align: center;"><b>Budget</b> :<?php echo "Rs ".number_format($acc["balance"]); ?></td>

<tr>
    <th>Campign Name</th>
    <th>CPC</th>
    <th>CTR</th>
    <th>Clicks</th>
    <th>Frequency</th>
    <th>Impressions</th>
    <th>Relevance Score</th>
</tr>


<?php

	foreach ($acc["campaigns"] as $acc1) {
		foreach ($acc1["ads"] as $acc2) { ?>	
			<tr>
			<?php foreach ($acc2["insights"] as $acc3) { ?>
			<?php
				 foreach ($acc1["insights"] as $acc4) { ?>	
					<td><?php echo $acc4["campaign_name"]; ?> </td>
					<td><?php echo $acc4["cpc"]; ?> </td>
					<td><?php echo $acc4["ctr"]; ?> </td>
					<td><?php echo $acc4["clicks"]; ?> </td>
					<td><?php echo $acc4["frequency"]; ?> </td>
					<td><?php echo $acc4["impressions"]; ?> </td>
			  	<?php 
			  	} ?>
			<td style="text-align: center;"><?php echo $acc3["relevance_score"]["score"]; ?> </td>
		<?php	}
			echo "</tr>";
		}
	}
}            
echo "</tr>";
 } 
} ?>
            </tbody>
			</table>
        </div>
	</div>
</body>
</html>
<script type="text/javascript">
function doUpdateAccessToken(accessToken){
	alert(accessToken);
}
</script>
