<?php
/**
 * Template Name:  AMP-Page
 * @package  SocialBeat Landiing Page Template
 */
?>
<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<!doctype html>
<html amp lang="en">
	<head>
	<meta charset="utf-8">
	<title>Ads-quotient</title>
	<link rel="canonical" href="http://13.234.228.21/demo/">
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/images/Favicon.png">
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<style amp-boilerplate>
	body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}
	@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
*,::before,::after{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:rgba(0,0,0,0)}article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}
body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;
background-color:#fff}[tabindex="-1"]:focus{outline:0}hr{box-sizing:content-box;height:0;overflow:visible}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}abbr[title],abbr[data-original-title]{text-decoration:underline;-webkit-text-decoration:underline dotted;
text-decoration:underline dotted;cursor:help;border-bottom:0;-webkit-text-decoration-skip-ink:none;text-decoration-skip-ink:none}address{margin-bottom:1rem;font-style:normal;line-height:inherit}ol,ul,dl{margin-top:0;margin-bottom:1rem}ol ol,ul ul,ol ul,ul ol{margin-bottom:0}dt{font-weight:700}
dd{margin-bottom:.5rem;margin-left:0}blockquote{margin:0 0 1rem}b,strong{font-weight:bolder}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}a{color:#007bff;text-decoration:none;background-color:transparent}
a:hover{color:#0056b3;text-decoration:underline}a:not([href]):not([tabindex]){color:inherit;text-decoration:none}a:not([href]):not([tabindex]):hover,a:not([href]):not([tabindex]):focus{color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus{outline:0}pre,code,kbd,
samp{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;font-size:1em}pre{margin-top:0;margin-bottom:1rem;overflow:auto}figure{margin:0 0 1rem}img{vertical-align:middle;border-style:none}svg{overflow:hidden;vertical-align:middle}table{border-collapse:collapse}
caption{padding-top:.75rem;padding-bottom:.75rem;color:#6c757d;text-align:left;caption-side:bottom}th{text-align:inherit}label{display:inline-block;margin-bottom:.5rem}button{border-radius:0}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}input,button,select,optgroup,
textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}select{word-wrap:normal}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button:not(:disabled),[type="button"]:not(:disabled),
[type="reset"]:not(:disabled),[type="submit"]:not(:disabled){cursor:pointer}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{padding:0;border-style:none}input[type="radio"],input[type="checkbox"]{box-sizing:border-box;padding:0}input[type="date"],
input[type="time"],input[type="datetime-local"],input[type="month"]{-webkit-appearance:listbox}textarea{overflow:auto;resize:vertical}fieldset{min-width:0;padding:0;margin:0;border:0}legend{display:block;width:100%;max-width:100%;padding:0;margin-bottom:.5rem;font-size:1.5rem;
line-height:inherit;color:inherit;white-space:normal}progress{vertical-align:baseline}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{outline-offset:-2px;
-webkit-appearance:none}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}output{display:inline-block}summary{display:list-item;cursor:pointer}template{display:none}[hidden]{display:none}
h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1,.h1{font-size:2.5rem}h2,.h2{font-size:2rem}h3,.h3{font-size:1.75rem}h4,.h4{font-size:1.5rem}h5,.h5{font-size:1.25rem}h6,.h6{font-size:1rem}hr{margin-top:1rem;margin-bottom:1rem;
border:0;border-top:1px solid rgba(0,0,0,0.1)}.list-unstyled{padding-left:0;list-style:none}.list-inline{padding-left:0;list-style:none}.list-inline-item{display:inline-block}.list-inline-item:not(:last-child){margin-right:.5rem}.img-fluid{max-width:100%;height:auto}
.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}

@media (min-width: 576px){.container{max-width:540px}}
@media (min-width: 768px){.container{max-width:720px}}
@media (min-width: 992px){.container{max-width:960px}}
@media (min-width: 1200px){.container{max-width:1215px}}
.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}
.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}
.no-gutters{margin-right:0;margin-left:0}
.no-gutters > .col,.no-gutters > [class*="col-"]{padding-right:0;padding-left:0}
.col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11,.col-12,.col,.col-auto,.col-sm-1,.col-sm-2,
.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm,.col-sm-auto,
.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,
.col-md,.col-md-auto,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,
.col-lg-11,.col-lg-12,.col-lg,.col-lg-auto,.col-xl-1,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,
.col-xl-9,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}
.col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}
.order-first{-ms-flex-order:-1;order:-1}.order-last{-ms-flex-order:13;order:13}
.order-0{-ms-flex-order:0;order:0}.order-1{-ms-flex-order:1;order:1}
.order-2{-ms-flex-order:2;order:2}.order-3{-ms-flex-order:3;order:3}
.order-4{-ms-flex-order:4;order:4}.order-5{-ms-flex-order:5;order:5}
.order-6{-ms-flex-order:6;order:6}.order-7{-ms-flex-order:7;order:7}
.order-8{-ms-flex-order:8;order:8}.order-9{-ms-flex-order:9;order:9}
.order-10{-ms-flex-order:10;order:10}.order-11{-ms-flex-order:11;order:11}
.order-12{-ms-flex-order:12;order:12}.offset-1{margin-left:8.333333%}
.offset-2{margin-left:16.666667%}.offset-3{margin-left:25%}.offset-4{margin-left:33.333333%}
.offset-5{margin-left:41.666667%}.offset-6{margin-left:50%}.offset-7{margin-left:58.333333%}
.offset-8{margin-left:66.666667%}.offset-9{margin-left:75%}.offset-10{margin-left:83.333333%}
.offset-11{margin-left:91.666667%}

@media (min-width: 576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;
flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;
flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.
col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-sm-first{-ms-flex-order:-1;order:-1}
.order-sm-last{-ms-flex-order:13;order:13}.order-sm-0{-ms-flex-order:0;order:0}
.order-sm-1{-ms-flex-order:1;order:1}.order-sm-2{-ms-flex-order:2;order:2}
.order-sm-3{-ms-flex-order:3;order:3}.order-sm-4{-ms-flex-order:4;order:4}
.order-sm-5{-ms-flex-order:5;order:5}.order-sm-6{-ms-flex-order:6;order:6}
.order-sm-7{-ms-flex-order:7;order:7}.order-sm-8{-ms-flex-order:8;order:8}
.order-sm-9{-ms-flex-order:9;order:9}.order-sm-10{-ms-flex-order:10;order:10}
.order-sm-11{-ms-flex-order:11;order:11}.order-sm-12{-ms-flex-order:12;order:12}
.offset-sm-0{margin-left:0}.offset-sm-1{margin-left:8.333333%}.offset-sm-2{margin-left:16.666667%}
.offset-sm-3{margin-left:25%}.offset-sm-4{margin-left:33.333333%}.offset-sm-5{margin-left:41.666667%}
.offset-sm-6{margin-left:50%}.offset-sm-7{margin-left:58.333333%}.offset-sm-8{margin-left:66.666667%}
.offset-sm-9{margin-left:75%}.offset-sm-10{margin-left:83.333333%}.offset-sm-11{margin-left:91.666667%}}

@media (min-width: 768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;
max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-md-first{-ms-flex-order:-1;order:-1}
.order-md-last{-ms-flex-order:13;order:13}.order-md-0{-ms-flex-order:0;order:0}
.order-md-1{-ms-flex-order:1;order:1}.order-md-2{-ms-flex-order:2;order:2}
.order-md-3{-ms-flex-order:3;order:3}.order-md-4{-ms-flex-order:4;order:4}
.order-md-5{-ms-flex-order:5;order:5}.order-md-6{-ms-flex-order:6;order:6}
.order-md-7{-ms-flex-order:7;order:7}.order-md-8{-ms-flex-order:8;order:8}
.order-md-9{-ms-flex-order:9;order:9}.order-md-10{-ms-flex-order:10;order:10}
.order-md-11{-ms-flex-order:11;order:11}.order-md-12{-ms-flex-order:12;order:12}
.offset-md-0{margin-left:0}.offset-md-1{margin-left:8.333333%}.offset-md-2{margin-left:16.666667%}
.offset-md-3{margin-left:25%}.offset-md-4{margin-left:33.333333%}.offset-md-5{margin-left:41.666667%}
.offset-md-6{margin-left:50%}.offset-md-7{margin-left:58.333333%}.offset-md-8{margin-left:66.666667%}
.offset-md-9{margin-left:75%}.offset-md-10{margin-left:83.333333%}.offset-md-11{margin-left:91.666667%}}

@media (min-width: 992px){.col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-lg-first{-ms-flex-order:-1;order:-1}
.order-lg-last{-ms-flex-order:13;order:13}.order-lg-0{-ms-flex-order:0;order:0}
.order-lg-1{-ms-flex-order:1;order:1}.order-lg-2{-ms-flex-order:2;order:2}
.order-lg-3{-ms-flex-order:3;order:3}.order-lg-4{-ms-flex-order:4;order:4}
.order-lg-5{-ms-flex-order:5;order:5}.order-lg-6{-ms-flex-order:6;order:6}
.order-lg-7{-ms-flex-order:7;order:7}.order-lg-8{-ms-flex-order:8;order:8}
.order-lg-9{-ms-flex-order:9;order:9}.order-lg-10{-ms-flex-order:10;order:10}
.order-lg-11{-ms-flex-order:11;order:11}.order-lg-12{-ms-flex-order:12;order:12}
.offset-lg-0{margin-left:0}.offset-lg-1{margin-left:8.333333%}.offset-lg-2{margin-left:16.666667%}
.offset-lg-3{margin-left:25%}.offset-lg-4{margin-left:33.333333%}.offset-lg-5{margin-left:41.666667%}
.offset-lg-6{margin-left:50%}.offset-lg-7{margin-left:58.333333%}.offset-lg-8{margin-left:66.666667%}
.offset-lg-9{margin-left:75%}.offset-lg-10{margin-left:83.333333%}.offset-lg-11{margin-left:91.666667%}}

@media (min-width: 1200px){.col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-xl-first{-ms-flex-order:-1;order:-1}
.order-xl-last{-ms-flex-order:13;order:13}.order-xl-0{-ms-flex-order:0;order:0}
.order-xl-1{-ms-flex-order:1;order:1}.order-xl-2{-ms-flex-order:2;order:2}
.order-xl-3{-ms-flex-order:3;order:3}.order-xl-4{-ms-flex-order:4;order:4}
.order-xl-5{-ms-flex-order:5;order:5}.order-xl-6{-ms-flex-order:6;order:6}
.order-xl-7{-ms-flex-order:7;order:7}.order-xl-8{-ms-flex-order:8;order:8}
.order-xl-9{-ms-flex-order:9;order:9}.order-xl-10{-ms-flex-order:10;order:10}
.order-xl-11{-ms-flex-order:11;order:11}.order-xl-12{-ms-flex-order:12;order:12}
.offset-xl-0{margin-left:0}.offset-xl-1{margin-left:8.333333%}.offset-xl-2{margin-left:16.666667%}
.offset-xl-3{margin-left:25%}.offset-xl-4{margin-left:33.333333%}.offset-xl-5{margin-left:41.666667%}
.offset-xl-6{margin-left:50%}.offset-xl-7{margin-left:58.333333%}.offset-xl-8{margin-left:66.666667%}
.offset-xl-9{margin-left:75%}.offset-xl-10{margin-left:83.333333%}.offset-xl-11{margin-left:91.666667%}}

.clearfix::after{display:block;clear:both;content:""}.d-none{display:none}.d-inline{display:inline}
.d-inline-block{display:inline-block}.d-block{display:block}.d-table{display:table}.d-table-row{display:table-row}
.d-table-cell{display:table-cell}.d-flex{display:-ms-flexbox;display:flex}
.d-inline-flex{display:-ms-inline-flexbox;display:inline-flex}

@media (min-width: 576px){.d-sm-none{display:none}.d-sm-inline{display:inline}.d-sm-inline-block{display:inline-block}
.d-sm-block{display:block}.d-sm-table{display:table}.d-sm-table-row{display:table-row}.d-sm-table-cell{display:table-cell}
.d-sm-flex{display:-ms-flexbox;display:flex}.d-sm-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 768px){.d-md-none{display:none}.d-md-inline{display:inline}.d-md-inline-block{display:inline-block}
.d-md-block{display:block}.d-md-table{display:table}.d-md-table-row{display:table-row}.d-md-table-cell{display:table-cell}
.d-md-flex{display:-ms-flexbox;display:flex}.d-md-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 992px){.d-lg-none{display:none}.d-lg-inline{display:inline}.d-lg-inline-block{display:inline-block}
.d-lg-block{display:block}.d-lg-table{display:table}.d-lg-table-row{display:table-row}.d-lg-table-cell{display:table-cell}
.d-lg-flex{display:-ms-flexbox;display:flex}.d-lg-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 1200px){.d-xl-none{display:none}.d-xl-inline{display:inline}.d-xl-inline-block{display:inline-block}
.d-xl-block{display:block}.d-xl-table{display:table}.d-xl-table-row{display:table-row}.d-xl-table-cell{display:table-cell}
.d-xl-flex{display:-ms-flexbox;display:flex}.d-xl-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

.justify-content-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-end{-ms-flex-pack:end;
justify-content:flex-end}.justify-content-center{-ms-flex-pack:center;justify-content:center}
.justify-content-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-start{-ms-flex-align:start;align-items:flex-start}.align-items-end{-ms-flex-align:end;align-items:flex-end}
.align-items-center{-ms-flex-align:center;align-items:center}
.align-items-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-center{-ms-flex-line-pack:center;align-content:center}
.align-content-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-center{-ms-flex-item-align:center;align-self:center}
.align-self-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-stretch{-ms-flex-item-align:stretch;align-self:stretch}

@media (min-width: 576px){.justify-content-sm-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-sm-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-sm-center{-ms-flex-pack:center;justify-content:center}
.justify-content-sm-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-sm-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-sm-start{-ms-flex-align:start;align-items:flex-start}.align-items-sm-end{-ms-flex-align:end;align-items:flex-end}
.align-items-sm-center{-ms-flex-align:center;align-items:center}
.align-items-sm-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-sm-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-sm-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-sm-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-sm-center{-ms-flex-line-pack:center;align-content:center}
.align-content-sm-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-sm-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-sm-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-sm-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-sm-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-sm-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-sm-center{-ms-flex-item-align:center;align-self:center}
.align-self-sm-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-sm-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 768px){.justify-content-md-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-md-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-md-center{-ms-flex-pack:center;justify-content:center}
.justify-content-md-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-md-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-md-start{-ms-flex-align:start;align-items:flex-start}
.align-items-md-end{-ms-flex-align:end;align-items:flex-end}
.align-items-md-center{-ms-flex-align:center;align-items:center}
.align-items-md-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-md-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-md-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-md-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-md-center{-ms-flex-line-pack:center;align-content:center}
.align-content-md-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-md-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-md-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-md-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-md-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-md-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-md-center{-ms-flex-item-align:center;align-self:center}
.align-self-md-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-md-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 992px){.justify-content-lg-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-lg-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-lg-center{-ms-flex-pack:center;justify-content:center}
.justify-content-lg-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-lg-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-lg-start{-ms-flex-align:start;align-items:flex-start}.align-items-lg-end{-ms-flex-align:end;align-items:flex-end}
.align-items-lg-center{-ms-flex-align:center;align-items:center}
.align-items-lg-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-lg-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-lg-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-lg-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-lg-center{-ms-flex-line-pack:center;align-content:center}
.align-content-lg-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-lg-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-lg-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-lg-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-lg-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-lg-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-lg-center{-ms-flex-item-align:center;align-self:center}
.align-self-lg-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-lg-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 1200px){.justify-content-xl-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-xl-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-xl-center{-ms-flex-pack:center;justify-content:center}
.justify-content-xl-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-xl-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-xl-start{-ms-flex-align:start;align-items:flex-start}
.align-items-xl-end{-ms-flex-align:end;align-items:flex-end}
.align-items-xl-center{-ms-flex-align:center;align-items:center}
.align-items-xl-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-xl-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-xl-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-xl-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-xl-center{-ms-flex-line-pack:center;align-content:center}
.align-content-xl-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-xl-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-xl-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-xl-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-xl-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-xl-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-xl-center{-ms-flex-item-align:center;align-self:center}
.align-self-xl-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-xl-stretch{-ms-flex-item-align:stretch;align-self:stretch}}
.float-left{float:left}.float-right{float:right}.float-none{float:none}

@media (min-width: 576px){.float-sm-left{float:left}.float-sm-right{float:right}.float-sm-none{float:none}}

@media (min-width: 768px){.float-md-left{float:left}.float-md-right{float:right}.float-md-none{float:none}}

@media (min-width: 992px){.float-lg-left{float:left}.float-lg-right{float:right}.float-lg-none{float:none}}

@media (min-width: 1200px){.float-xl-left{float:left}.float-xl-right{float:right}.float-xl-none{float:none}}

.overflow-auto{overflow:auto}.overflow-hidden{overflow:hidden}.position-static{position:static}
.position-relative{position:relative}.position-absolute{position:absolute}.position-fixed{position:fixed}
.position-sticky{position:-webkit-sticky;position:sticky}.fixed-top{position:fixed;top:0;right:0;left:0;z-index:1030}
.fixed-bottom{position:fixed;right:0;bottom:0;left:0;z-index:1030}.w-25{width:25%}.w-50{width:50%}.w-75{width:75%}
.w-100{width:100%}.w-auto{width:auto}.h-25{height:25%}.h-50{height:50%}.h-75{height:75%}.h-100{height:100%}
.h-auto{height:auto}.mw-100{max-width:100%}.mh-100{max-height:100%}.min-vw-100{min-width:100vw}.min-vh-100{min-height:100vh}
.vw-100{width:100vw}.vh-100{height:100vh}.text-monospace{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}
.text-justify{text-align:justify}.text-wrap{white-space:normal}
.text-nowrap{white-space:nowrap}.text-truncate{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}
.text-left{text-align:left}.text-right{text-align:right}
.text-center{text-align:center}

@media (min-width: 576px){.text-sm-left{text-align:left}.text-sm-right{text-align:right}.text-sm-center{text-align:center}}

@media (min-width: 768px){.text-md-left{text-align:left}.text-md-right{text-align:right}.text-md-center{text-align:center}}

@media (min-width: 992px){.text-lg-left{text-align:left}.text-lg-right{text-align:right}.text-lg-center{text-align:center}}

@media (min-width: 1200px){.text-xl-left{text-align:left}.text-xl-right{text-align:right}.text-xl-center{text-align:center}}

.text-lowercase{text-transform:lowercase}.text-uppercase{text-transform:uppercase}.text-capitalize{text-transform:capitalize}
.font-weight-light{font-weight:300}.font-weight-lighter{font-weight:lighter}.font-weight-normal{font-weight:400}
.font-weight-bold{font-weight:700}.font-weight-bolder{font-weight:bolder}.font-italic{font-style:italic}
.text-white{color:#fff}




@font-face {
    font-family: 'OpenSansRegular';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.svg#OpenSansRegular') format('svg');
}
.OpenSansRegular{ font-family : 'OpenSansRegular'}
   .fs-sm-42px { font-size:42px;}
   

@font-face {
    font-family: 'OpenSansLight';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.svg#OpenSansLight') format('svg');
}
.OpenSansLight{ font-family : 'OpenSansLight'}
   .fs-sm-20px { font-size:20px;}

@font-face {
    font-family: 'OpenSansExtraBold';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.svg#OpenSansExtraBold') format('svg');
}
.OpenSansExtraBold{ font-family : 'OpenSansExtraBold'}
   .fs-sm-62px { font-size:62px;}
   
 @font-face {
    font-family: 'Arialregular';
    src: url('fonts/Arialregular.eot');
    src: url('fonts/Arialregular.eot') format('embedded-opentype'),
         url('fonts/Arialregular.woff2') format('woff2'),
         url('fonts/Arialregular.woff') format('woff'),
         url('fonts/Arialregular.ttf') format('truetype'),
         url('fonts/Arialregular.svg#Arialregular') format('svg');
}
  .Arialregular{ font-family : 'Arialregular'}
   .fs-sm-24px { font-size:24px;}
   
.header{background-repeat: no-repeat;background-size: cover;position: relative;background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/adsbanner.png');height:768px;}
.logo {padding: 4% 0 0 8%;}
section.second_position{padding: 1% 0 2% 0;}
section.second_position h1 {text-align:center;padding: 3% 0;font-size:42px;font-family : 'OpenSansRegular';text-transform: uppercase;color:#0e1f51;}
.spent {padding: 4% 3% 3% 0%;text-align: center;}
.spentd {padding:1% 4% 3% 7%;text-align: center;display: inline-block;margin: 0;width: auto;clear: none;}
.spent p{padding: 4% 0% 0 1%; font-size:17px;font-family : 'OpenSansLight';color:#0e1f51;}
section.last-part {background: #e6e8ed;}
input#idName,input#idEmail,input#idPhone {padding: 1% 1%;margin: 3% 0;width: 75%;border-radius: 13px;border: 2px solid #a3acae;background: #e6e8ed;}
input#idSubmit {width: 75%;padding: 1% 1%;margin: 1% 0;border-radius: 13px;border: 2px solid #48d299;background: #48d299;color: white;background: #48d299;text-transform: uppercase;}
.calculate {padding: 5% 0 0 0 ;}
.what_do {padding: 7% 0 3% 0;}
.calculate h1{padding: 3% 0 0 0;font-size:50px;font-family : 'OpenSansLight';color:#0e1f51}
.calculate h2{font-size:50px;color:#0e1f51}
.calculate p{text-align:left;padding:0%;font-size:24px;color:#99a3a5;}
.footer p {color: #0e1f51;text-align: center;padding: 1% 0 0 0;font-size: 18px;font-family : 'OpenSansExtraBold';}
.click p a{color: white;}
.hand {padding-left: 0;}
.spents{padding: 1% 3% 3% 8%;text-align: center;display: inline-block;margin: 0;width: auto;clear: none;}
.performanced {text-align: center;}
.spentd p{padding: 4% 0% 0 1%;font-size: 17px;font-family: 'OpenSansLight';color: #0e1f51;}
.spents p{padding: 4% 0% 0 1%;font-size: 17px;font-family: 'OpenSansLight';color: #0e1f51;}
.desktop-form {padding-left: 0;}


@media screen and (max-width: 767px) {
.header{background-repeat: no-repeat;background-size: cover;position: relative;background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/mob-banner.png');height:600px;}
.logo{display:none;}
.mobile-logo {padding: 6% 0;text-align: center;}
section.second_position h1{font-size: 33px;}
.spent{text-align:center;}
.side-img{display:none;}
section.second_position {padding: 6% 0 6% 0;}
.calculate h1{font-size: 33px;text-align: center;}
.calculate h2{font-size: 33px;text-align:center;}
.calculate p{font-size: 19px;text-align: center;}
.form{text-align: center;}
.footer p{padding: 1% 0 8% 0;}
.click p {position: absolute;bottom: 10%;left: 15%;background: #48d299;display: inline-block;border: 2px solid #48d299;border-radius: 13px;width: 67%;font-size: 18px;padding: 0;margin: 6px 0 0 0;margin-top: 6px;margin-right: 0px;margin-bottom: 0px;margin-left: 0px;text-align: center;}
.spentd {padding: 1% 9% 3% 9%;}
.calculate{padding: 3% 0;}
.click h1{position: absolute;top: 39%;left: 15%;color: white;font-weight: 800;}
.click h3{position: absolute;top: 71%;left: 15%;color: white;font-size: 17px;padding: 1% 0 0 0;font-weight: 400;}
.click h2{position: absolute;top: 54%;left: 15%;color: white;font-size: 43px;font-weight: 200;}
.performance {text-align: center;}
.spents{padding: 1% 5% 1% 8%;}
section.last-part {display: none;}
section.mobile-part {display: block;background:#e6e8ed;}
.sticky_button {
    position: fixed;
    bottom: 0px;
    margin-right: auto;
    margin-left: auto;
    width: 100%;
}
a.enqire {
    width: 100%;
    float: left;
    text-align: center;
    background: #48d299;
    padding: 10px 0px;
    color: #fff;
    border-right: 1px solid #48d299;
    text-transform: uppercase;
    text-decoration: none;
    font-family: RalewayRegular;
}
.click p{display:none;}
}

@media screen and (min-width: 768px) {
.mobile-logo{display:none;}
.click p {position: absolute;bottom: 23%;left: 19%;background: #48d299;display: inline-block;border: 2px solid #48d299;border-radius: 13px;width: 20%;font-size: 21px;padding: 5px 0 4px 0;margin: 6px 0 0 0;text-align: center;}
.click h1{position: absolute;top: 39%;left: 19%;color: white;font-weight: 800;font-size: 47px;}
.click h3{position: absolute;top: 58%;left: 19%;color: white;font-size: 17px;padding: 1% 0 0 0;font-weight: 400;}
.click h2{position: absolute;top: 45%;left: 19%;color: white;font-size: 43px;font-weight: 200;}
.side-img {padding: 0 0 0 3%;}
section.mobile-part {display: none;}
.sticky_button {display: none;}
}
</style>
</head>
<body>
<!-- Google Tag Manager -->
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-MKS8BJP&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>
<section class="header">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 logo">
			<div class="row">
				<div class="col-12 col-md-2 col-sm-2 desktop-logo">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/mobile-logo.png" alt="mobile-logo.png" width="200" height="56" >
					</amp-img>
				</div>
				
			</div>
		</div>
		<div class="mobile-logo">
			<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/mobile-logo.png" alt="mobile-logo.png" width="200" height="56">
			</amp-img>
		</div>
		<div class="click">
		    <h1><b>DATA-DRIVEN</b></h1><br><h2>DIGITAL<br>MARKETING</h2>
			<h3>Use Competitor Benchmarks,<br>Audience Insights and AI to<br>improve your ROI</h3>
			<p><a href="#ads">GET YOUR AD SCORE NOW</a></p>
		</div>
	</div>
</section>

<section class="second_position">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			<h1>Performance Marketing at Scale</h1>
		</div>
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 performance">
			<div class="row">
				<div class="col-12 col-md-2 col-sm-2"></div>
				<div class="col-12 col-md-2 col-sm-2 spent">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/180-crores.png" alt="180-crores.png" width="141" height="99">
					</amp-img>
					<p>Rs. 200 CRORES SPENT ACROSS 25 PLATFORMS IN 2018</p>
				</div>
				<div class="col-12 col-md-1 col-sm-1 side-img">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/side-img.png" alt="side-img.png" width="19" height="271">
					</amp-img>
				</div>
				<div class="col-12 col-md-2 col-sm-2 spent">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/5-million.png" alt="5-million.png" width="144" height="105">
					</amp-img>
					<p>5 BILLION IMPRESSIONS</p>
				</div>
				<div class="col-12 col-md-1 col-sm-1  side-img">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/side-img.png" alt="side-img.png" width="19" height="271">
					</amp-img>
				</div>
				<div class="col-12 col-md-2 col-sm-2 spent">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/INR.png" alt="INR.png" width="104" height="105">
					</amp-img>
					<p>7000 CRORE RETURNS ON AD SPENDS</p>
				</div>
				
			</div>
		</div>
		
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 what_do">
			<h1>WHAT DO WE DO?</h1>
		</div >
		<div class="performance">
				<div class="spentd">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/data-visualized.png" alt="data-visualized.png" width="145" height="140">
					</amp-img>
					<p>DATA VISUALIZED TO GATHER INSIGHTS</p>
				</div>
				
				<div class="spentd">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/competitor.png" alt="competitor.png" width="215" height="149">
					</amp-img>
					<p>COMPETITOR INSIGHTS</p>
				</div>
				
				<div class="spentd">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/programatic-solution.png" alt="programatic-solution.png" width="145" height="150">
					</amp-img>
					<p>PROGRAMMATIC SOLUTIONS</p>
				</div>
		</div>		
		
		<div class="performanced">
				<div class="spents">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/audience.png" alt="audience.png" width="170" height="162">
					</amp-img>
					<p>AUDIENCE SEGMENTATION</p>
				</div>
				
				<div class="spents">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/location.png" alt="location.png" width="172" height="178">
					</amp-img>
					<p>GEO-LOCATION INSIGHTS</p>
				</div>
				
		
			
		</div>	
	</div>
</section>

<section class="last-part">
	
		<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-md-6 col-sm-6 hand">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/last-part.png" alt="last-part.png" width="500" height="600" layout="responsive">
					</amp-img>
				</div>
				<div class="col-12 col-md-5 col-sm-5 calculate"  id="ads">
					<h1 class="calc">CALCULATE  YOUR</h1>
					<h2><b>ADS SCORE FOR FREE</b></h2>
					<p>A Proprietary methodology to calculate your campaigns effectiveness </p>
					<div class="form-section">
				<div class="col-12 col-md-12 col-sm-12 col-lg-12 desktop-form">
					<form action="http://13.234.228.21/demo/sendtogravity.php" method="GET"  target="_top" class="form">
						<input type="text" name="Name" id="idName" class="namefield" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="Name">
						<span visible-when-invalid="valueMissing" validation-for="idName">Your Name please!</span>
						<span visible-when-invalid="typeMismatch" validation-for="idName">Invalid Name!</span>
						<span visible-when-invalid="typeMismatch" validation-for="idPhonel">Invalid Number!</span>
						<input type="email" name="Email" id="idEmail" class="emailfield" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email">
						<span visible-when-invalid="valueMissing" validation-for="idEmail">Your email please!</span>
						<input type="text" name="Phone" id="idPhone" class="phonefield" required pattern="\d{10}" placeholder="Phone number">
						<span visible-when-invalid="valueMissing" validation-for="idPhone">Your Number please!</span>
						<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Email!</span>
						<input type="hidden" name="URL" value="<?php echo $actual_link; ?>" />
						<input type="submit" value="Submit" id="idSubmit" class="submit">
					</form>
				</div>
			</div>
				</div>
			</div>
		</div>
	
</section>

<section class="mobile-part">
	
		<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			<div class="row">
				
				<div class="col-12 col-md-5 col-sm-5 calculate"  id="ads-1">
					<h1 class="calc">CALCULATE  YOUR</h1>
					<h2><b>ADS SCORE FOR FREE</b></h2>
					<p>A Proprietary methodology to calculate your campaigns effectiveness </p>
					<div class="form-section">
						<div class="col-12 col-md-12 col-sm-12 col-lg-12 desktop-form">
							<form action="https://beat.social/ads-quotient/sendtogravity.php" method="GET"  target="_top" class="form">
								<input type="text" name="Name" id="idName" class="namefield" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="Name">
								<span visible-when-invalid="valueMissing" validation-for="idName">Your Name please!</span>
								<span visible-when-invalid="typeMismatch" validation-for="idName">Invalid Name!</span>
								<span visible-when-invalid="typeMismatch" validation-for="idPhonel">Invalid Number!</span>
								<input type="email" name="Email" id="idEmail" class="emailfield" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email">
								<span visible-when-invalid="valueMissing" validation-for="idEmail">Your email please!</span>
								<input type="text" name="Phone" id="idPhone" class="phonefield" required pattern="\d{10}" placeholder="Phone number">
								<span visible-when-invalid="valueMissing" validation-for="idPhone">Your Number please!</span>
								<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Email!</span>
								<input type="hidden" name="URL" value="<?php echo $actual_link; ?>" />
								<input type="submit" value="Submit" id="idSubmit" class="submit">
							</form>
						</div>
					</div>
				</div>
				
				<div class="col-12 col-md-6 col-sm-6 hand">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/last-part.png" alt="last-part.png" width="500" height="600" layout="responsive">
					</amp-img>
				</div>
			</div>
		</div>
	
</section>
<section class="footer">
	<p> Copyright © 2019 Ads-quotient  </p>
</section>

<div class="sticky_button">
	<a class="enqire" id="enquire-now" href="#ads-1">GET YOUR AD SCORE NOW</a>
  </div>
</body>
</html>