<?php
/**
 * Template Name:  privacy
 * @package  SocialBeat Landiing Page Template
 */
?>
<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<!doctype html>
<html amp lang="en">
	<head>
	<meta charset="utf-8">
	<title>Ads-quotient</title>
	<link rel="canonical" href="https://lifeonline.co/apps-lp/">
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/images/Favicon.png">
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<style amp-boilerplate>
	body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}
	@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
*,::before,::after{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:rgba(0,0,0,0)}article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}
body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;
background-color:#fff}[tabindex="-1"]:focus{outline:0}hr{box-sizing:content-box;height:0;overflow:visible}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}abbr[title],abbr[data-original-title]{text-decoration:underline;-webkit-text-decoration:underline dotted;
text-decoration:underline dotted;cursor:help;border-bottom:0;-webkit-text-decoration-skip-ink:none;text-decoration-skip-ink:none}address{margin-bottom:1rem;font-style:normal;line-height:inherit}ol,ul,dl{margin-top:0;margin-bottom:1rem}ol ol,ul ul,ol ul,ul ol{margin-bottom:0}dt{font-weight:700}
dd{margin-bottom:.5rem;margin-left:0}blockquote{margin:0 0 1rem}b,strong{font-weight:bolder}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}a{color:#007bff;text-decoration:none;background-color:transparent}
a:hover{color:#0056b3;text-decoration:underline}a:not([href]):not([tabindex]){color:inherit;text-decoration:none}a:not([href]):not([tabindex]):hover,a:not([href]):not([tabindex]):focus{color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus{outline:0}pre,code,kbd,
samp{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;font-size:1em}pre{margin-top:0;margin-bottom:1rem;overflow:auto}figure{margin:0 0 1rem}img{vertical-align:middle;border-style:none}svg{overflow:hidden;vertical-align:middle}table{border-collapse:collapse}
caption{padding-top:.75rem;padding-bottom:.75rem;color:#6c757d;text-align:left;caption-side:bottom}th{text-align:inherit}label{display:inline-block;margin-bottom:.5rem}button{border-radius:0}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}input,button,select,optgroup,
textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}select{word-wrap:normal}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button:not(:disabled),[type="button"]:not(:disabled),
[type="reset"]:not(:disabled),[type="submit"]:not(:disabled){cursor:pointer}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{padding:0;border-style:none}input[type="radio"],input[type="checkbox"]{box-sizing:border-box;padding:0}input[type="date"],
input[type="time"],input[type="datetime-local"],input[type="month"]{-webkit-appearance:listbox}textarea{overflow:auto;resize:vertical}fieldset{min-width:0;padding:0;margin:0;border:0}legend{display:block;width:100%;max-width:100%;padding:0;margin-bottom:.5rem;font-size:1.5rem;
line-height:inherit;color:inherit;white-space:normal}progress{vertical-align:baseline}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{outline-offset:-2px;
-webkit-appearance:none}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}output{display:inline-block}summary{display:list-item;cursor:pointer}template{display:none}[hidden]{display:none}
h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1,.h1{font-size:2.5rem}h2,.h2{font-size:2rem}h3,.h3{font-size:1.75rem}h4,.h4{font-size:1.5rem}h5,.h5{font-size:1.25rem}h6,.h6{font-size:1rem}hr{margin-top:1rem;margin-bottom:1rem;
border:0;border-top:1px solid rgba(0,0,0,0.1)}.list-unstyled{padding-left:0;list-style:none}.list-inline{padding-left:0;list-style:none}.list-inline-item{display:inline-block}.list-inline-item:not(:last-child){margin-right:.5rem}.img-fluid{max-width:100%;height:auto}
.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}

@media (min-width: 576px){.container{max-width:540px}}
@media (min-width: 768px){.container{max-width:720px}}
@media (min-width: 992px){.container{max-width:960px}}
@media (min-width: 1200px){.container{max-width:1215px}}
.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}
.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}
.no-gutters{margin-right:0;margin-left:0}
.no-gutters > .col,.no-gutters > [class*="col-"]{padding-right:0;padding-left:0}
.col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11,.col-12,.col,.col-auto,.col-sm-1,.col-sm-2,
.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm,.col-sm-auto,
.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,
.col-md,.col-md-auto,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,
.col-lg-11,.col-lg-12,.col-lg,.col-lg-auto,.col-xl-1,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,
.col-xl-9,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}
.col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}
.order-first{-ms-flex-order:-1;order:-1}.order-last{-ms-flex-order:13;order:13}
.order-0{-ms-flex-order:0;order:0}.order-1{-ms-flex-order:1;order:1}
.order-2{-ms-flex-order:2;order:2}.order-3{-ms-flex-order:3;order:3}
.order-4{-ms-flex-order:4;order:4}.order-5{-ms-flex-order:5;order:5}
.order-6{-ms-flex-order:6;order:6}.order-7{-ms-flex-order:7;order:7}
.order-8{-ms-flex-order:8;order:8}.order-9{-ms-flex-order:9;order:9}
.order-10{-ms-flex-order:10;order:10}.order-11{-ms-flex-order:11;order:11}
.order-12{-ms-flex-order:12;order:12}.offset-1{margin-left:8.333333%}
.offset-2{margin-left:16.666667%}.offset-3{margin-left:25%}.offset-4{margin-left:33.333333%}
.offset-5{margin-left:41.666667%}.offset-6{margin-left:50%}.offset-7{margin-left:58.333333%}
.offset-8{margin-left:66.666667%}.offset-9{margin-left:75%}.offset-10{margin-left:83.333333%}
.offset-11{margin-left:91.666667%}

@media (min-width: 576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;
flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;
flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.
col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-sm-first{-ms-flex-order:-1;order:-1}
.order-sm-last{-ms-flex-order:13;order:13}.order-sm-0{-ms-flex-order:0;order:0}
.order-sm-1{-ms-flex-order:1;order:1}.order-sm-2{-ms-flex-order:2;order:2}
.order-sm-3{-ms-flex-order:3;order:3}.order-sm-4{-ms-flex-order:4;order:4}
.order-sm-5{-ms-flex-order:5;order:5}.order-sm-6{-ms-flex-order:6;order:6}
.order-sm-7{-ms-flex-order:7;order:7}.order-sm-8{-ms-flex-order:8;order:8}
.order-sm-9{-ms-flex-order:9;order:9}.order-sm-10{-ms-flex-order:10;order:10}
.order-sm-11{-ms-flex-order:11;order:11}.order-sm-12{-ms-flex-order:12;order:12}
.offset-sm-0{margin-left:0}.offset-sm-1{margin-left:8.333333%}.offset-sm-2{margin-left:16.666667%}
.offset-sm-3{margin-left:25%}.offset-sm-4{margin-left:33.333333%}.offset-sm-5{margin-left:41.666667%}
.offset-sm-6{margin-left:50%}.offset-sm-7{margin-left:58.333333%}.offset-sm-8{margin-left:66.666667%}
.offset-sm-9{margin-left:75%}.offset-sm-10{margin-left:83.333333%}.offset-sm-11{margin-left:91.666667%}}

@media (min-width: 768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;
max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-md-first{-ms-flex-order:-1;order:-1}
.order-md-last{-ms-flex-order:13;order:13}.order-md-0{-ms-flex-order:0;order:0}
.order-md-1{-ms-flex-order:1;order:1}.order-md-2{-ms-flex-order:2;order:2}
.order-md-3{-ms-flex-order:3;order:3}.order-md-4{-ms-flex-order:4;order:4}
.order-md-5{-ms-flex-order:5;order:5}.order-md-6{-ms-flex-order:6;order:6}
.order-md-7{-ms-flex-order:7;order:7}.order-md-8{-ms-flex-order:8;order:8}
.order-md-9{-ms-flex-order:9;order:9}.order-md-10{-ms-flex-order:10;order:10}
.order-md-11{-ms-flex-order:11;order:11}.order-md-12{-ms-flex-order:12;order:12}
.offset-md-0{margin-left:0}.offset-md-1{margin-left:8.333333%}.offset-md-2{margin-left:16.666667%}
.offset-md-3{margin-left:25%}.offset-md-4{margin-left:33.333333%}.offset-md-5{margin-left:41.666667%}
.offset-md-6{margin-left:50%}.offset-md-7{margin-left:58.333333%}.offset-md-8{margin-left:66.666667%}
.offset-md-9{margin-left:75%}.offset-md-10{margin-left:83.333333%}.offset-md-11{margin-left:91.666667%}}

@media (min-width: 992px){.col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-lg-first{-ms-flex-order:-1;order:-1}
.order-lg-last{-ms-flex-order:13;order:13}.order-lg-0{-ms-flex-order:0;order:0}
.order-lg-1{-ms-flex-order:1;order:1}.order-lg-2{-ms-flex-order:2;order:2}
.order-lg-3{-ms-flex-order:3;order:3}.order-lg-4{-ms-flex-order:4;order:4}
.order-lg-5{-ms-flex-order:5;order:5}.order-lg-6{-ms-flex-order:6;order:6}
.order-lg-7{-ms-flex-order:7;order:7}.order-lg-8{-ms-flex-order:8;order:8}
.order-lg-9{-ms-flex-order:9;order:9}.order-lg-10{-ms-flex-order:10;order:10}
.order-lg-11{-ms-flex-order:11;order:11}.order-lg-12{-ms-flex-order:12;order:12}
.offset-lg-0{margin-left:0}.offset-lg-1{margin-left:8.333333%}.offset-lg-2{margin-left:16.666667%}
.offset-lg-3{margin-left:25%}.offset-lg-4{margin-left:33.333333%}.offset-lg-5{margin-left:41.666667%}
.offset-lg-6{margin-left:50%}.offset-lg-7{margin-left:58.333333%}.offset-lg-8{margin-left:66.666667%}
.offset-lg-9{margin-left:75%}.offset-lg-10{margin-left:83.333333%}.offset-lg-11{margin-left:91.666667%}}

@media (min-width: 1200px){.col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-xl-first{-ms-flex-order:-1;order:-1}
.order-xl-last{-ms-flex-order:13;order:13}.order-xl-0{-ms-flex-order:0;order:0}
.order-xl-1{-ms-flex-order:1;order:1}.order-xl-2{-ms-flex-order:2;order:2}
.order-xl-3{-ms-flex-order:3;order:3}.order-xl-4{-ms-flex-order:4;order:4}
.order-xl-5{-ms-flex-order:5;order:5}.order-xl-6{-ms-flex-order:6;order:6}
.order-xl-7{-ms-flex-order:7;order:7}.order-xl-8{-ms-flex-order:8;order:8}
.order-xl-9{-ms-flex-order:9;order:9}.order-xl-10{-ms-flex-order:10;order:10}
.order-xl-11{-ms-flex-order:11;order:11}.order-xl-12{-ms-flex-order:12;order:12}
.offset-xl-0{margin-left:0}.offset-xl-1{margin-left:8.333333%}.offset-xl-2{margin-left:16.666667%}
.offset-xl-3{margin-left:25%}.offset-xl-4{margin-left:33.333333%}.offset-xl-5{margin-left:41.666667%}
.offset-xl-6{margin-left:50%}.offset-xl-7{margin-left:58.333333%}.offset-xl-8{margin-left:66.666667%}
.offset-xl-9{margin-left:75%}.offset-xl-10{margin-left:83.333333%}.offset-xl-11{margin-left:91.666667%}}

.clearfix::after{display:block;clear:both;content:""}.d-none{display:none}.d-inline{display:inline}
.d-inline-block{display:inline-block}.d-block{display:block}.d-table{display:table}.d-table-row{display:table-row}
.d-table-cell{display:table-cell}.d-flex{display:-ms-flexbox;display:flex}
.d-inline-flex{display:-ms-inline-flexbox;display:inline-flex}

@media (min-width: 576px){.d-sm-none{display:none}.d-sm-inline{display:inline}.d-sm-inline-block{display:inline-block}
.d-sm-block{display:block}.d-sm-table{display:table}.d-sm-table-row{display:table-row}.d-sm-table-cell{display:table-cell}
.d-sm-flex{display:-ms-flexbox;display:flex}.d-sm-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 768px){.d-md-none{display:none}.d-md-inline{display:inline}.d-md-inline-block{display:inline-block}
.d-md-block{display:block}.d-md-table{display:table}.d-md-table-row{display:table-row}.d-md-table-cell{display:table-cell}
.d-md-flex{display:-ms-flexbox;display:flex}.d-md-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 992px){.d-lg-none{display:none}.d-lg-inline{display:inline}.d-lg-inline-block{display:inline-block}
.d-lg-block{display:block}.d-lg-table{display:table}.d-lg-table-row{display:table-row}.d-lg-table-cell{display:table-cell}
.d-lg-flex{display:-ms-flexbox;display:flex}.d-lg-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 1200px){.d-xl-none{display:none}.d-xl-inline{display:inline}.d-xl-inline-block{display:inline-block}
.d-xl-block{display:block}.d-xl-table{display:table}.d-xl-table-row{display:table-row}.d-xl-table-cell{display:table-cell}
.d-xl-flex{display:-ms-flexbox;display:flex}.d-xl-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

.justify-content-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-end{-ms-flex-pack:end;
justify-content:flex-end}.justify-content-center{-ms-flex-pack:center;justify-content:center}
.justify-content-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-start{-ms-flex-align:start;align-items:flex-start}.align-items-end{-ms-flex-align:end;align-items:flex-end}
.align-items-center{-ms-flex-align:center;align-items:center}
.align-items-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-center{-ms-flex-line-pack:center;align-content:center}
.align-content-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-center{-ms-flex-item-align:center;align-self:center}
.align-self-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-stretch{-ms-flex-item-align:stretch;align-self:stretch}

@media (min-width: 576px){.justify-content-sm-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-sm-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-sm-center{-ms-flex-pack:center;justify-content:center}
.justify-content-sm-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-sm-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-sm-start{-ms-flex-align:start;align-items:flex-start}.align-items-sm-end{-ms-flex-align:end;align-items:flex-end}
.align-items-sm-center{-ms-flex-align:center;align-items:center}
.align-items-sm-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-sm-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-sm-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-sm-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-sm-center{-ms-flex-line-pack:center;align-content:center}
.align-content-sm-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-sm-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-sm-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-sm-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-sm-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-sm-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-sm-center{-ms-flex-item-align:center;align-self:center}
.align-self-sm-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-sm-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 768px){.justify-content-md-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-md-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-md-center{-ms-flex-pack:center;justify-content:center}
.justify-content-md-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-md-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-md-start{-ms-flex-align:start;align-items:flex-start}
.align-items-md-end{-ms-flex-align:end;align-items:flex-end}
.align-items-md-center{-ms-flex-align:center;align-items:center}
.align-items-md-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-md-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-md-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-md-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-md-center{-ms-flex-line-pack:center;align-content:center}
.align-content-md-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-md-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-md-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-md-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-md-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-md-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-md-center{-ms-flex-item-align:center;align-self:center}
.align-self-md-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-md-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 992px){.justify-content-lg-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-lg-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-lg-center{-ms-flex-pack:center;justify-content:center}
.justify-content-lg-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-lg-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-lg-start{-ms-flex-align:start;align-items:flex-start}.align-items-lg-end{-ms-flex-align:end;align-items:flex-end}
.align-items-lg-center{-ms-flex-align:center;align-items:center}
.align-items-lg-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-lg-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-lg-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-lg-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-lg-center{-ms-flex-line-pack:center;align-content:center}
.align-content-lg-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-lg-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-lg-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-lg-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-lg-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-lg-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-lg-center{-ms-flex-item-align:center;align-self:center}
.align-self-lg-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-lg-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 1200px){.justify-content-xl-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-xl-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-xl-center{-ms-flex-pack:center;justify-content:center}
.justify-content-xl-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-xl-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-xl-start{-ms-flex-align:start;align-items:flex-start}
.align-items-xl-end{-ms-flex-align:end;align-items:flex-end}
.align-items-xl-center{-ms-flex-align:center;align-items:center}
.align-items-xl-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-xl-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-xl-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-xl-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-xl-center{-ms-flex-line-pack:center;align-content:center}
.align-content-xl-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-xl-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-xl-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-xl-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-xl-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-xl-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-xl-center{-ms-flex-item-align:center;align-self:center}
.align-self-xl-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-xl-stretch{-ms-flex-item-align:stretch;align-self:stretch}}
.float-left{float:left}.float-right{float:right}.float-none{float:none}

@media (min-width: 576px){.float-sm-left{float:left}.float-sm-right{float:right}.float-sm-none{float:none}}

@media (min-width: 768px){.float-md-left{float:left}.float-md-right{float:right}.float-md-none{float:none}}

@media (min-width: 992px){.float-lg-left{float:left}.float-lg-right{float:right}.float-lg-none{float:none}}

@media (min-width: 1200px){.float-xl-left{float:left}.float-xl-right{float:right}.float-xl-none{float:none}}

.overflow-auto{overflow:auto}.overflow-hidden{overflow:hidden}.position-static{position:static}
.position-relative{position:relative}.position-absolute{position:absolute}.position-fixed{position:fixed}
.position-sticky{position:-webkit-sticky;position:sticky}.fixed-top{position:fixed;top:0;right:0;left:0;z-index:1030}
.fixed-bottom{position:fixed;right:0;bottom:0;left:0;z-index:1030}.w-25{width:25%}.w-50{width:50%}.w-75{width:75%}
.w-100{width:100%}.w-auto{width:auto}.h-25{height:25%}.h-50{height:50%}.h-75{height:75%}.h-100{height:100%}
.h-auto{height:auto}.mw-100{max-width:100%}.mh-100{max-height:100%}.min-vw-100{min-width:100vw}.min-vh-100{min-height:100vh}
.vw-100{width:100vw}.vh-100{height:100vh}.text-monospace{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}
.text-justify{text-align:justify}.text-wrap{white-space:normal}
.text-nowrap{white-space:nowrap}.text-truncate{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}
.text-left{text-align:left}.text-right{text-align:right}
.text-center{text-align:center}

@media (min-width: 576px){.text-sm-left{text-align:left}.text-sm-right{text-align:right}.text-sm-center{text-align:center}}

@media (min-width: 768px){.text-md-left{text-align:left}.text-md-right{text-align:right}.text-md-center{text-align:center}}

@media (min-width: 992px){.text-lg-left{text-align:left}.text-lg-right{text-align:right}.text-lg-center{text-align:center}}

@media (min-width: 1200px){.text-xl-left{text-align:left}.text-xl-right{text-align:right}.text-xl-center{text-align:center}}

.text-lowercase{text-transform:lowercase}.text-uppercase{text-transform:uppercase}.text-capitalize{text-transform:capitalize}
.font-weight-light{font-weight:300}.font-weight-lighter{font-weight:lighter}.font-weight-normal{font-weight:400}
.font-weight-bold{font-weight:700}.font-weight-bolder{font-weight:bolder}.font-italic{font-style:italic}
.text-white{color:#fff}


@font-face {
    font-family: 'OpenSansRegular';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.svg#OpenSansRegular') format('svg');
}
.OpenSansRegular{ font-family : 'OpenSansRegular'}
   .fs-sm-42px { font-size:42px;}
   

@font-face {
    font-family: 'OpenSansLight';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.svg#OpenSansLight') format('svg');
}
.OpenSansLight{ font-family : 'OpenSansLight'}
   .fs-sm-20px { font-size:20px;}

@font-face {
    font-family: 'OpenSansExtraBold';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.svg#OpenSansExtraBold') format('svg');
}
.OpenSansExtraBold{ font-family : 'OpenSansExtraBold'}
   .fs-sm-62px { font-size:62px;}
   
  @font-face {
    font-family: 'ArialTh';
    src: url('fonts/ArialTh.eot');
    src: url('fonts/ArialTh.eot') format('embedded-opentype'),
         url('fonts/ArialTh.woff2') format('woff2'),
         url('fonts/ArialTh.woff') format('woff'),
         url('fonts/ArialTh.ttf') format('truetype'),
         url('fonts/ArialTh.svg#ArialTh') format('svg');
}
.ArialTh{ font-family : 'ArialTh'}
   .fs-sm-24px { font-size:24px;}
   
section.whole_content h1{font-size: 40px;padding: 4% 0 2% 0;font-family : 'OpenSansRegular';font-weight: 600;}
section.whole_content p{font-size: 16px;font-family : 'OpenSansLight';}
section.whole_content h3{font-size: 25px;font-family : 'OpenSansRegular';font-weight: 600;padding: 2% 0;}
section.whole_content h4{font-size: 21px;font-family : 'OpenSansRegular';padding: 1% 0;}
section.whole_content h5{font-size: 20px;font-family : 'OpenSansRegular';padding: 1% 0;}
ul.deem {list-style-type: none;}
.whole_content ul li {font-size: 17px;padding: 5px 0;}
.whole_content ul.deem li {font-size: 17px;padding: 5px 0;font-family : 'OpenSansLight'}
section.footer {background: #34495E;}
section.footer p {color: white;text-align: center;padding: 1% 0;}
section.footer p a{color: white;text-decoration: none;}
section.header {position: fixed;width: 100%;top: 0;}
.header {background-color: #fff;margin-bottom: 0;border: 0;-webkit-box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.2)}
section.whole_content {padding: 6% 0;}
.logo {padding: 1% 0 1% 4%;text-align: right;}

@media screen and (max-width: 767px) {
	.logo {padding: 1% 0 1% 4%;text-align: initial;}
}
</style>
</head>
<body>

<section class="header">

		<div class="col-12 col-md-12 col-sm-12 col-lg-12 top_head">
			<div class="row">
				<div class="col-12 col-md-3 col-sm-3 logo">
					<a href="http://www.adsquotient.com/"><amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/mobile-logo.png" alt="mobile-logo.png" width="200" height="56" >
					</amp-img></a>
				</div>
				<div class="col-12 col-md-12 col-sm-12 col-lg-12">
				</div>
			</div>
		</div>
	
</section>

<section class="whole_content">
	<div class="container">
		<h1>Privacy Policy</h1>
		<p>Thank you for visiting adsquotient.com. We want you to know that your privacy is important to us. Our customers are at the heart of everything we do, and we strive to ensure your experience with adsquotient is one that you will want to repeat and share with your friends. Part of our commitment to you is to respect and protect the privacy of the personal information you provide to us.</p>
		<p>This document is designed to apprise you about the kind inform we collect, why we collect such information, and how we use the information we collect. This Privacy Policy is incorporated into our Terms of Service. We will share your personal information with third parties only in the ways that are described in this privacy statement. We do not sell your personal information to third parties.</p>
		<p>When you submit your personal information to us, you are giving us your consent to the collection, use, and disclosure of your information as set forth in this Privacy Policy. We are always available to discuss your questions or concerns regarding this Privacy Policy and our privacy practices. If you would like to speak to a customer service representative, please contact us via telephone at +91-44-42065648 or email at contact@adsquotient.com</p>
		<p>Your use of our Website constitutes your consent to this Privacy Policy, including any changes or updates. We may
		make additional features, functionality, offers, activities, or events (“opportunities”) available to you subject to additional or different privacy rules that we disclose in connection with those opportunities.</p>
		
		<h3>Collection and Use of Information</h3>
		<h4>General.</h4>
		<p>In general, you can browse our Website without telling us who you are or revealing any personal information about yourself. At various times, you may decide to provide us with your personal information. You should know that we receive and may store all personal information (whether written or oral) that you provide to us through whatever means you provide such information (through our Website, via email, over the telephone, etc.). Personal information means any information that may be used to identify an individual, including, but not limited to, a first and last name, home, billing or other physical address or email address as well as any information associated with the foregoing. In addition to your contact information, we may collect information about your purchases, billing address, shipping address, gender, occupation, marital status, anniversary, interests, phone number or other contact information, and credit card information.</p>
		
		<h4>Use of Your Personal Information</h4>
		<p>We may use the information we collect about you to:</p>
		<ul>
			<li>Facilitate your purchases and provide the services you request,</li>
			<li>Confirm and track your order,</li>
			<li>Respond to your inquiries and requests,</li>
			<li>Compare and review your personal information for errors, omissions and accuracy,</li>
			<li>Prevent and detect fraud or abuse,</li>
			<li>Improve our Website, service, product offerings, marketing and promotional efforts, and overall customer experience,</li>
			<li>Identify your product and service preferences,</li>
			<li>Understand our customer demographics, preferences, interests, and behavior, and</li>
			<li>Contact you (via email, postal mail, or telephone) regarding products and services (of adsquotient.com or our partners) that we believe may be of interest to you. </li>
		</ul>
		
		<h4>Personal Information that we May Share with Others</h4>
		<p>In certain circumstances, we may share your personal information with trusted partners.</p>
		<h5>Service Providers:</h5>
		<p>We use trusted third-party service providers to perform certain services on our behalf, including:<br>
		shipping, payment processing, data storage/management, webhosting, web analytics, fulfillment, assembly, marketing, mailing, emailing etc. These service providers only receive personal information if such information is needed to perform their function(s), and they are not authorized to use any personal information for any other purpose(s) other than the purpose(s) set forth by adsquotient.</p>
		
		<h4>Compliance with Law and Fraud Protection:</h4>
		<p>We may, and you authorize us to use and disclose any information, including personal information:</p>
		<ul class="deem">
			<li>1. We deem necessary, in our sole discretion, to comply with any applicable law, regulation, legal process, or governmental request;</li>
			<li>2. In order to investigate, prevent or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any personal or as otherwise required by law;</li>
			<li>3. To other companies and organizations for credit fraud protection and risk reduction; and</li>
			<li>4. To enforce any agreement we have with you.</li>
		</ul>
		
		<h4>Business Transfer:</h4>
		<p>Your personal information may be disclosed as part of any merger, sale of company assets or acquisition, as well as in the event of insolvency, bankruptcy or receivership, in which personal information would be transferred as one of the business assets of the Company.</p>
		
		<h4>Anonymous Information:</h4>
		<p>We may create anonymous records from personal information by excluding information (such as your name) that makes the information personally identifiable to you. We may use these records for internal purposes, such as analyzing usage patterns so that we may enhance our services, and we also reserve the right to use and disclose any information in such records at our discretion.</p>
		
		<h4>Comments:</h4>
		<p>We value your comments, feedback, testimonials, etc., which help us improve our website, products, and services. By making such submissions to us, you assign your rights in the submissions to us, as described in the “Submissions” section of our Terms of Service page.
		Other Uses of Your Information</p>
		
		<h4>IP Address:</h4>
		<p>When you visit our Website, adsquotient collects your IP address to, among other things, help diagnose problems with its server, administer and tune the operation of its Website, analyze trends, track traffic patterns, gather demographic information for aggregate use, and track the date and duration of each session within our Website. Your IP address may also be used in combination with your personal information to prevent fraud or abuse, customize your shopping experience, improve our website, customer service, products, and promotional efforts, and to understand your preferences, patterns and interests.</p>
		
		<h4>Data Collection Devices, such as Cookies and Web Beacons:</h4>
		<p>adsquotient collects data through cookies, web logs, web beacons (also known as pixel gifs or action tags) and other monitoring technologies to enhance your browsing and shopping experience on our website. “Cookies” are small pieces of information that are stored by your browser on your computer’s hard drive to collect information about your activities on our website. Clear gifs are tiny graphics with a unique identifier, similar in function to cookies, and are used to track the online movements of Web users or within an email for the purpose of transferring information and are not stored on your computer.</p>
		<p>We use cookies and web beacons to deliver our ads, improve and measure the usability, track visits from our affiliates and partners, performance and effectiveness of our Website, improve and measure the effectiveness of our marketing programs, learn how customers use our website, estimate our audience size, and to customize your shopping experience.</p>
		<p>Examples of the type of information that we collect through these collection devices, includes: total visitors to the website, pages viewed, unique visitors, time spent on our Website and on certain web pages, etc.</p>
		<p>We may authorize third parties to use cookies, web beacons and other monitoring technologies to compile information about the use of the Website or interaction with advertisements that appear on the Website. We do have access or control over these cookies.</p>
		<p>We do not link the information we store in cookies or collect from web beacons to any personally identifiable information you submit while on our site.</p>
		<p>We use session ID cookies and persistent cookies. A session ID cookie expires when you close your browser. A persistent cookie remains on your hard drive for an extended period of time.</p>
		<p>You are always free to decline cookies if your browser permits (You can remove persistent cookies by following directions provided in your Internet browser’s “help” file); although, by declining the use of cookies you may not be able to use certain features on the Website.</p>
		
		<h4>Third Party Personal information:</h4>
		<p>If you choose to use our referral service to tell a friend about our site, we will ask you for your friend’s email address. We will automatically send your friend a one-time email inviting him or her to visit the site. adsquotient stores this information for the sole purpose of sending this one-time email and tracking the success of our referral program. </p>
		<p>We also provide you with the opportunity to share a product or your wish list with a friend. If you choose to share a product or a wish list with your friend we will ask for your friend’s email address in order to automatically send them a one-time email containing the product or your wish list. adsquotient stores this information for the sole purpose of sending this one-time email.<br>If you wish to purchase an electronic gift card for your friend we will ask for your friend’s name and email address in order to send your friend their electronic gift card. adsquotient stores or does not store this information for the sole purpose of sending this email and tracking the redemption of said electronic gift card.
		You may also provide the shipping information of a friend in order to send them our products as a gift. We store this information for the purposes of shipping a product to your friend. You may update or remove this information by logging into your account and going to your saved addresses page. </p>
		<p>Your friend may contact us at contact@adsquotient.com to request that we remove any of this information from our database.</p>
		
		<h4>Customer Stories</h4>
		<p>We post customer stories on our web site which may contain personally identifiable information. We do obtain the customer’s consent via email prior to posting the testimonial to post their name along with their testimonial. If you wish to request the removal of your customer story you may contact us at contact@adsquotient.com</p>
		
		<h4>Public Forums</h4>
		<p>Our Web site offers publicly accessible blogs or community forums. You should be aware that any information you provide in these areas may be read, collected, and used by others who access them. To request removal of your personal information from our blog or community forum, contact us at contact@adsquotient.com In some cases, we may not be able to remove your personal information, in which case we will let you know if we are unable to do so and why.</p>
		
		<h4>Children under 13</h4>
		<p>adsquotient does not knowingly collect personal information from children under the age of 13. If we learn that we have collected personally identifiable information from a child under the age of 13, we will delete that data from our systems. Please visit the FTC’s website at www.ftc.gov for tips on protecting children’s privacy online.</p>
		
		<h4>Links</h4>
		<p>Our Website may provide links to other third-party Websites which are outside our control and not covered by this privacy policy. We encourage you to review the privacy policies posted on these (and all) Websites.</p>
		
		<h4>Security</h4>
		<p>adsquotient is committed to the protection of the personally identifiable information that you share with us. We utilize a combination of physical and electronic security technologies, procedures, and organizational measures to help protect your personally identifiable information from unauthorized access, use or disclosure. When we transfer sensitive personal information (for example, credit card information) over the Internet, we protect it using Secure Sockets Layer (SSL) encryption technology. While we strive to safeguard your personal information once we receive it, no transmission of data over the Internet or any other public network can be guaranteed to be 100% secure and, accordingly, we cannot guarantee or warrant the security of any information you disclose or transmit to us.</p>
		
		<h4>Accessing and Updating Your Information</h4>
		<p>If the personally identifiable information adsquotient.com has gathered from you changes or you would like to access, correct, or delete such information, we will gladly provide you access to, correct, or delete (to the extent allowed by law) any personal information we have collected about you. To request access to, a correction to, or deletion of your personal information, please send an e-mail to to contact@adsquotient.com. If you have registered for an account on our site you may update or remove your personally identifiable information by logging into your account and going to your account management page. We will retain your information for as long as your account is active or as needed to provide you services. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.</p>
		
		<h4>Choice/Opt-Out</h4>
		<p>We want to communicate with you only if you want to continue to hear from us.</p>
		<p>To opt out of receiving email promotions that we send you:</p>
		<ul>
			<li> select “unsubscribe” in emails that we send you; or</li>
			<li>contact Customer Service at +91-44-42065648. To opt out of telephone promotions, you can tell us when we call you.</li>
		</ul>
		<p>Opting out of communication with us does not affect our communications with you via telephone or email related to your orders with us. It also does not affect our use of your non-personally identifiable information. </p>
		<p>We will send you strictly service-related announcements on rare occasions when it is necessary to do so. For instance, if our service is temporarily suspended for maintenance, we might send you an email.</p>
		<p>Generally, you may not opt-out of these communications, which are not promotional in nature. If you do not wish to receive them, you have the option to deactivate your account.</p>
	</div>
</section>

<section class="footer">
	<p><a href="https://www.socialbeat.in/">PRODUCT OF SOCIAL BEAT</a></p>
</section>


		
	
</body>
</html>