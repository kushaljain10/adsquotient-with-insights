<?php
/**
 * Template Name:  Terms&conditions
 * @package  SocialBeat Landiing Page Template
 */
?>
<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<!doctype html>
<html amp lang="en">
	<head>
	<meta charset="utf-8">
	<title>Ads-quotient</title>
	<link rel="canonical" href="https://lifeonline.co/apps-lp/">
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/images/Favicon.png">
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<style amp-boilerplate>
	body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}
	@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
*,::before,::after{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:rgba(0,0,0,0)}article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}
body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;
background-color:#fff}[tabindex="-1"]:focus{outline:0}hr{box-sizing:content-box;height:0;overflow:visible}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}abbr[title],abbr[data-original-title]{text-decoration:underline;-webkit-text-decoration:underline dotted;
text-decoration:underline dotted;cursor:help;border-bottom:0;-webkit-text-decoration-skip-ink:none;text-decoration-skip-ink:none}address{margin-bottom:1rem;font-style:normal;line-height:inherit}ol,ul,dl{margin-top:0;margin-bottom:1rem}ol ol,ul ul,ol ul,ul ol{margin-bottom:0}dt{font-weight:700}
dd{margin-bottom:.5rem;margin-left:0}blockquote{margin:0 0 1rem}b,strong{font-weight:bolder}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}a{color:#007bff;text-decoration:none;background-color:transparent}
a:hover{color:#0056b3;text-decoration:underline}a:not([href]):not([tabindex]){color:inherit;text-decoration:none}a:not([href]):not([tabindex]):hover,a:not([href]):not([tabindex]):focus{color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus{outline:0}pre,code,kbd,
samp{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;font-size:1em}pre{margin-top:0;margin-bottom:1rem;overflow:auto}figure{margin:0 0 1rem}img{vertical-align:middle;border-style:none}svg{overflow:hidden;vertical-align:middle}table{border-collapse:collapse}
caption{padding-top:.75rem;padding-bottom:.75rem;color:#6c757d;text-align:left;caption-side:bottom}th{text-align:inherit}label{display:inline-block;margin-bottom:.5rem}button{border-radius:0}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}input,button,select,optgroup,
textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}select{word-wrap:normal}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button:not(:disabled),[type="button"]:not(:disabled),
[type="reset"]:not(:disabled),[type="submit"]:not(:disabled){cursor:pointer}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{padding:0;border-style:none}input[type="radio"],input[type="checkbox"]{box-sizing:border-box;padding:0}input[type="date"],
input[type="time"],input[type="datetime-local"],input[type="month"]{-webkit-appearance:listbox}textarea{overflow:auto;resize:vertical}fieldset{min-width:0;padding:0;margin:0;border:0}legend{display:block;width:100%;max-width:100%;padding:0;margin-bottom:.5rem;font-size:1.5rem;
line-height:inherit;color:inherit;white-space:normal}progress{vertical-align:baseline}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{outline-offset:-2px;
-webkit-appearance:none}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}output{display:inline-block}summary{display:list-item;cursor:pointer}template{display:none}[hidden]{display:none}
h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1,.h1{font-size:2.5rem}h2,.h2{font-size:2rem}h3,.h3{font-size:1.75rem}h4,.h4{font-size:1.5rem}h5,.h5{font-size:1.25rem}h6,.h6{font-size:1rem}hr{margin-top:1rem;margin-bottom:1rem;
border:0;border-top:1px solid rgba(0,0,0,0.1)}.list-unstyled{padding-left:0;list-style:none}.list-inline{padding-left:0;list-style:none}.list-inline-item{display:inline-block}.list-inline-item:not(:last-child){margin-right:.5rem}.img-fluid{max-width:100%;height:auto}
.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}

@media (min-width: 576px){.container{max-width:540px}}
@media (min-width: 768px){.container{max-width:720px}}
@media (min-width: 992px){.container{max-width:960px}}
@media (min-width: 1200px){.container{max-width:1215px}}
.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}
.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}
.no-gutters{margin-right:0;margin-left:0}
.no-gutters > .col,.no-gutters > [class*="col-"]{padding-right:0;padding-left:0}
.col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11,.col-12,.col,.col-auto,.col-sm-1,.col-sm-2,
.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm,.col-sm-auto,
.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,
.col-md,.col-md-auto,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,
.col-lg-11,.col-lg-12,.col-lg,.col-lg-auto,.col-xl-1,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,
.col-xl-9,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}
.col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}
.order-first{-ms-flex-order:-1;order:-1}.order-last{-ms-flex-order:13;order:13}
.order-0{-ms-flex-order:0;order:0}.order-1{-ms-flex-order:1;order:1}
.order-2{-ms-flex-order:2;order:2}.order-3{-ms-flex-order:3;order:3}
.order-4{-ms-flex-order:4;order:4}.order-5{-ms-flex-order:5;order:5}
.order-6{-ms-flex-order:6;order:6}.order-7{-ms-flex-order:7;order:7}
.order-8{-ms-flex-order:8;order:8}.order-9{-ms-flex-order:9;order:9}
.order-10{-ms-flex-order:10;order:10}.order-11{-ms-flex-order:11;order:11}
.order-12{-ms-flex-order:12;order:12}.offset-1{margin-left:8.333333%}
.offset-2{margin-left:16.666667%}.offset-3{margin-left:25%}.offset-4{margin-left:33.333333%}
.offset-5{margin-left:41.666667%}.offset-6{margin-left:50%}.offset-7{margin-left:58.333333%}
.offset-8{margin-left:66.666667%}.offset-9{margin-left:75%}.offset-10{margin-left:83.333333%}
.offset-11{margin-left:91.666667%}

@media (min-width: 576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;
flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;
flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.
col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-sm-first{-ms-flex-order:-1;order:-1}
.order-sm-last{-ms-flex-order:13;order:13}.order-sm-0{-ms-flex-order:0;order:0}
.order-sm-1{-ms-flex-order:1;order:1}.order-sm-2{-ms-flex-order:2;order:2}
.order-sm-3{-ms-flex-order:3;order:3}.order-sm-4{-ms-flex-order:4;order:4}
.order-sm-5{-ms-flex-order:5;order:5}.order-sm-6{-ms-flex-order:6;order:6}
.order-sm-7{-ms-flex-order:7;order:7}.order-sm-8{-ms-flex-order:8;order:8}
.order-sm-9{-ms-flex-order:9;order:9}.order-sm-10{-ms-flex-order:10;order:10}
.order-sm-11{-ms-flex-order:11;order:11}.order-sm-12{-ms-flex-order:12;order:12}
.offset-sm-0{margin-left:0}.offset-sm-1{margin-left:8.333333%}.offset-sm-2{margin-left:16.666667%}
.offset-sm-3{margin-left:25%}.offset-sm-4{margin-left:33.333333%}.offset-sm-5{margin-left:41.666667%}
.offset-sm-6{margin-left:50%}.offset-sm-7{margin-left:58.333333%}.offset-sm-8{margin-left:66.666667%}
.offset-sm-9{margin-left:75%}.offset-sm-10{margin-left:83.333333%}.offset-sm-11{margin-left:91.666667%}}

@media (min-width: 768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;
max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-md-first{-ms-flex-order:-1;order:-1}
.order-md-last{-ms-flex-order:13;order:13}.order-md-0{-ms-flex-order:0;order:0}
.order-md-1{-ms-flex-order:1;order:1}.order-md-2{-ms-flex-order:2;order:2}
.order-md-3{-ms-flex-order:3;order:3}.order-md-4{-ms-flex-order:4;order:4}
.order-md-5{-ms-flex-order:5;order:5}.order-md-6{-ms-flex-order:6;order:6}
.order-md-7{-ms-flex-order:7;order:7}.order-md-8{-ms-flex-order:8;order:8}
.order-md-9{-ms-flex-order:9;order:9}.order-md-10{-ms-flex-order:10;order:10}
.order-md-11{-ms-flex-order:11;order:11}.order-md-12{-ms-flex-order:12;order:12}
.offset-md-0{margin-left:0}.offset-md-1{margin-left:8.333333%}.offset-md-2{margin-left:16.666667%}
.offset-md-3{margin-left:25%}.offset-md-4{margin-left:33.333333%}.offset-md-5{margin-left:41.666667%}
.offset-md-6{margin-left:50%}.offset-md-7{margin-left:58.333333%}.offset-md-8{margin-left:66.666667%}
.offset-md-9{margin-left:75%}.offset-md-10{margin-left:83.333333%}.offset-md-11{margin-left:91.666667%}}

@media (min-width: 992px){.col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-lg-first{-ms-flex-order:-1;order:-1}
.order-lg-last{-ms-flex-order:13;order:13}.order-lg-0{-ms-flex-order:0;order:0}
.order-lg-1{-ms-flex-order:1;order:1}.order-lg-2{-ms-flex-order:2;order:2}
.order-lg-3{-ms-flex-order:3;order:3}.order-lg-4{-ms-flex-order:4;order:4}
.order-lg-5{-ms-flex-order:5;order:5}.order-lg-6{-ms-flex-order:6;order:6}
.order-lg-7{-ms-flex-order:7;order:7}.order-lg-8{-ms-flex-order:8;order:8}
.order-lg-9{-ms-flex-order:9;order:9}.order-lg-10{-ms-flex-order:10;order:10}
.order-lg-11{-ms-flex-order:11;order:11}.order-lg-12{-ms-flex-order:12;order:12}
.offset-lg-0{margin-left:0}.offset-lg-1{margin-left:8.333333%}.offset-lg-2{margin-left:16.666667%}
.offset-lg-3{margin-left:25%}.offset-lg-4{margin-left:33.333333%}.offset-lg-5{margin-left:41.666667%}
.offset-lg-6{margin-left:50%}.offset-lg-7{margin-left:58.333333%}.offset-lg-8{margin-left:66.666667%}
.offset-lg-9{margin-left:75%}.offset-lg-10{margin-left:83.333333%}.offset-lg-11{margin-left:91.666667%}}

@media (min-width: 1200px){.col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-xl-first{-ms-flex-order:-1;order:-1}
.order-xl-last{-ms-flex-order:13;order:13}.order-xl-0{-ms-flex-order:0;order:0}
.order-xl-1{-ms-flex-order:1;order:1}.order-xl-2{-ms-flex-order:2;order:2}
.order-xl-3{-ms-flex-order:3;order:3}.order-xl-4{-ms-flex-order:4;order:4}
.order-xl-5{-ms-flex-order:5;order:5}.order-xl-6{-ms-flex-order:6;order:6}
.order-xl-7{-ms-flex-order:7;order:7}.order-xl-8{-ms-flex-order:8;order:8}
.order-xl-9{-ms-flex-order:9;order:9}.order-xl-10{-ms-flex-order:10;order:10}
.order-xl-11{-ms-flex-order:11;order:11}.order-xl-12{-ms-flex-order:12;order:12}
.offset-xl-0{margin-left:0}.offset-xl-1{margin-left:8.333333%}.offset-xl-2{margin-left:16.666667%}
.offset-xl-3{margin-left:25%}.offset-xl-4{margin-left:33.333333%}.offset-xl-5{margin-left:41.666667%}
.offset-xl-6{margin-left:50%}.offset-xl-7{margin-left:58.333333%}.offset-xl-8{margin-left:66.666667%}
.offset-xl-9{margin-left:75%}.offset-xl-10{margin-left:83.333333%}.offset-xl-11{margin-left:91.666667%}}

@font-face {
    font-family: 'OpenSansRegular';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.svg#OpenSansRegular') format('svg');
}
.OpenSansRegular{ font-family : 'OpenSansRegular'}
   .fs-sm-42px { font-size:42px;}
   .fs-sm-17px { font-size:17px;}
   

@font-face {
    font-family: 'OpenSansLight';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.svg#OpenSansLight') format('svg');
}
.OpenSansLight{ font-family : 'OpenSansLight'}
   .fs-sm-20px { font-size:20px;}

@font-face {
    font-family: 'OpenSansExtraBold';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.svg#OpenSansExtraBold') format('svg');
}
.OpenSansExtraBold{ font-family : 'OpenSansExtraBold'}
   .fs-sm-62px { font-size:62px;}
   
  @font-face {
    font-family: 'ArialTh';
    src: url('fonts/ArialTh.eot');
    src: url('fonts/ArialTh.eot') format('embedded-opentype'),
         url('fonts/ArialTh.woff2') format('woff2'),
         url('fonts/ArialTh.woff') format('woff'),
         url('fonts/ArialTh.ttf') format('truetype'),
         url('fonts/ArialTh.svg#ArialTh') format('svg');
}
.ArialTh{ font-family : 'ArialTh'}
   .fs-sm-24px { font-size:24px;}
   
section.whole_content h1{font-size: 40px;padding: 4% 0 2% 0;font-family : 'OpenSansRegular';font-weight: 600;}
section.whole_content h2{font-size: 40px;padding: 4% 0 2% 0;font-family : 'OpenSansRegular';font-weight: 600;}
section.whole_content p{font-size: 16px;font-family : 'OpenSansRegular';}
section.whole_content h3{font-size: 25px;font-family : 'OpenSansRegular';font-weight: 600;padding: 2% 0;}
section.whole_content h4{font-size: 21px;font-family : 'OpenSansRegular';padding: 1% 0;}
section.whole_content h5{font-size: 20px;font-family : 'OpenSansRegular';padding: 1% 0;}
ul.deem {list-style-type: none;}
.whole_content ul li {font-size: 17px;padding: 5px 0;}
.whole_content ul.deem li {font-size: 17px;padding: 5px 0;font-family : 'OpenSansRegular'}
section.footer {background: #34495E;}
section.footer p {color: white;text-align: center;padding: 1% 0;}
section.footer p a{color: white;text-decoration: none;}
section.header {position: fixed;width: 100%;top: 0;}
.header {background-color: #fff;margin-bottom: 0;border: 0;-webkit-box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.2)}
section.whole_content {padding: 6% 0;}
.logo {padding: 1% 0 1% 4%;text-align: right;}
section.whole_content ul {list-style-type: none;}


@media screen and (max-width: 767px) {
	.logo {padding: 1% 0 1% 4%;text-align: initial;}
}
</style>
</head>
<body>

<section class="header">

		<div class="col-12 col-md-12 col-sm-12 col-lg-12 top_head">
			<div class="row">
				<div class="col-12 col-md-3 col-sm-3 logo">
					<a href="http://www.adsquotient.com/"><amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/mobile-logo.png" alt="mobile-logo.png" width="200" height="56" >
					</amp-img></a>
				</div>
				<div class="col-12 col-md-12 col-sm-12 col-lg-12">
				</div>
			</div>
		</div>
	
</section>

<section class="whole_content">
	<div class="container">
	<h1>Terms And Conditions</h1>
		<h2>For Adsquotient</h2>
		<ul>
			<li> 1.<a href="https://www.adsquotient.com">Adsquotient.com</a> connects brands with bloggers and online influencers (hereby referred to as bloggers). Selection of the bloggers is at the sole discretion of the brand in collaboration with Adsquotient.com and will be final.</li>
			<li>2. The blogger should be 18 years of age or older.</li>
			<li>3. Bloggers registered on <a href="https://www.adsquotient.com">Adsquotient.com</a> will be showcased to brands for potential campaigns.</li>
			<li>4. The blogger will ensure that no post or action on social media affects the brand negatively. Any such breach will void the agreement and the blogger will be liable for appropriate compensation to the brand.</li>
			<li>5. The blogger should not participate in a campaign by his or her employer, employer's competitor or any campaign, which he or she has direct interest or conflict in.</li>
			<li>6. The blogger on registration should share accurate information about his blog and social media presence and their traffic. Any misrepresentation will void any agreement with <a href="https://www.adsquotient.com">Adsquotient.com</a></li>
			<li>7. Payment to the blogger will be made after: 
				<ul>
					<li> I) The blog and social media content has been take live by the blogger and are approved by <a href="https://www.adsquotient.com">Adsquotient.com</a> in writing. </li>
					<li> II) Analytics are shared within one week of taking the post live showing the traction. `</li>
				</ul>
			</li>
			<p>Incase the blog is not approved for any reason by the brand or <a href="https://www.adsquotient.com">Adsquotient.com</a>, the blogger will be instructed to take the blog down and no payment will be due from <a href="https://www.adsquotient.com">Adsquotient.com</a> to the blogger.</p>
			<li>8. Payment will be made within 4 weeks from the time the <a href="https://www.adsquotient.com">Adsquotient.com</a> receives the analytics from the blogger.</li>
			<li> 9. All taxes as applicable by the blogger will be the sole responsibility of the blogger. Tax Deducted at Source (TDS), where applicable will be deducted by <a href="https://www.adsquotient.com">Adsquotient.com</a>. For payments above the threshold limit, Pan Card details would need to be shared as required prior to the payment.</li>
			<li>10. Payment will be made either through bank transfer, when bank details are shared along with ID proof or through online shopping vouchers (e.g. Amazon, Flipkart) as finalized by mutual consent prior to the post being taken live.</li>
			<li>11. The blogger agrees to not solicit any campaign directly by the brand. If the blogger does solicit campaigns, the blogger account will be deactivated and not eligible for any future campaigns.</li>
			<li>12. In case of any complaints or requests, email to contact@Adsquotient.com</li>
			<li> 13. The engagement with the blogger is done in good faith. Any manipulation of data shared (traffic, reach) manually or through bots would void any agreement with <a href="https://www.adsquotient.com">Adsquotient.com</a> and no payments shall be made.</li>
			<li>14. Disclosure: All promotions and blogs incentivized in cash or otherwise should clearly mention "Sponsored in collaboration with <a href="https://www.adsquotient.com">Adsquotient.com</a>" or "Ad in collaborate with <a href="https://www.adsquotient.com">Adsquotient.com</a>" in the blog and social media posts.</li>
			<li> 15. In case of any dispute, disputes will be under the jurisdiction of the High Court of Chennai, not considering the location of the blogger or brand.</li>
			<li>16. The terms and conditions can be updated at the sole discretion of <a href="https://www.adsquotient.com">Adsquotient.com</a></li>
		</ul>
		
		<h2>For Brands</h2>
		<p><a href="https://www.adsquotient.com">Adsquotient.com</a> is a platform enabling brands to engage with adsquotient and bloggers across India. A brand can launch a campaign on Adsquotient after paying the appropriate management fee. A brand launching a campaign agrees to the below terms and condition:</p>
		<ul>
			<li>1. The Brand will provide accurate information about the products and services prior to launching the campaign.</li>
			<li> 2. The Brand undertakes to make the payment of the agreed upon incentive to the blogger upon shortlisting the blogger for engagement.</li>
			<li>3. The blogger incentive shall be paid out to the blogger by <a href="https://www.adsquotient.com">Adsquotient.com</a> on completion of the engagement.</li>
			<li> 4. In case of any dispute, Adsquotient.com holds the right to pay the blogger on behalf of the brand if it deems that the blogger has completed his or her engagement in totality.</li>
			<li> 5. The Brand will not falsify any information provided to the blogger or <a href="https://www.adsquotient.com">Adsquotient.com</a></li>
			<li>6. All applicable taxes are to be paid by the brand.</li>
		</ul>
	</div>	
</section>
<section class="footer">
	<p><a href="https://www.socialbeat.in/">PRODUCT OF SOCIAL BEAT</a></p>
</section>


		
	
</body>
</html>