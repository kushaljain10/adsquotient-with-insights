<?php
/**
 * Template Name: Connecting Accounts
 * @package  SocialBeat Landiing Page Template
 */
?>
<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<!doctype html>
<html amp lang="en">
	<head>
	<meta charset="utf-8">
	<title>Data Driven Performance Marketing, Ads Quotient</title>
	<meta name="description" content="Leverage your data and improve ROI, by analyzing audience insights, competitor insights and market insights. Use AI and programmatic advertising for real-time optimization.">
	<link rel="canonical" href="http://www.adsquotient.com">
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/images/Favicon.png">
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>

	<style amp-boilerplate>
	body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}
	@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
*,::before,::after{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:rgba(0,0,0,0)}article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}
body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;
background-color:#fff}[tabindex="-1"]:focus{outline:0}hr{box-sizing:content-box;height:0;overflow:visible}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}abbr[title],abbr[data-original-title]{text-decoration:underline;-webkit-text-decoration:underline dotted;
text-decoration:underline dotted;cursor:help;border-bottom:0;-webkit-text-decoration-skip-ink:none;text-decoration-skip-ink:none}address{margin-bottom:1rem;font-style:normal;line-height:inherit}ol,ul,dl{margin-top:0;margin-bottom:1rem}ol ol,ul ul,ol ul,ul ol{margin-bottom:0}dt{font-weight:700}
dd{margin-bottom:.5rem;margin-left:0}blockquote{margin:0 0 1rem}b,strong{font-weight:bolder}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}a{color:#007bff;text-decoration:none;background-color:transparent}
a:hover{color:#0056b3;text-decoration:underline}a:not([href]):not([tabindex]){color:inherit;text-decoration:none}a:not([href]):not([tabindex]):hover,a:not([href]):not([tabindex]):focus{color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus{outline:0}pre,code,kbd,
samp{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;font-size:1em}pre{margin-top:0;margin-bottom:1rem;overflow:auto}figure{margin:0 0 1rem}img{vertical-align:middle;border-style:none}svg{overflow:hidden;vertical-align:middle}table{border-collapse:collapse}
caption{padding-top:.75rem;padding-bottom:.75rem;color:#6c757d;text-align:left;caption-side:bottom}th{text-align:inherit}label{display:inline-block;margin-bottom:.5rem}button{border-radius:0}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}input,button,select,optgroup,
textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}select{word-wrap:normal}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button:not(:disabled),[type="button"]:not(:disabled),
[type="reset"]:not(:disabled),[type="submit"]:not(:disabled){cursor:pointer}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{padding:0;border-style:none}input[type="radio"],input[type="checkbox"]{box-sizing:border-box;padding:0}input[type="date"],
input[type="time"],input[type="datetime-local"],input[type="month"]{-webkit-appearance:listbox}textarea{overflow:auto;resize:vertical}fieldset{min-width:0;padding:0;margin:0;border:0}legend{display:block;width:100%;max-width:100%;padding:0;margin-bottom:.5rem;font-size:1.5rem;
line-height:inherit;color:inherit;white-space:normal}progress{vertical-align:baseline}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{outline-offset:-2px;
-webkit-appearance:none}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}output{display:inline-block}summary{display:list-item;cursor:pointer}template{display:none}[hidden]{display:none}
h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1,.h1{font-size:2.5rem}h2,.h2{font-size:2rem}h3,.h3{font-size:1.75rem}h4,.h4{font-size:1.5rem}h5,.h5{font-size:1.25rem}h6,.h6{font-size:1rem}hr{margin-top:1rem;margin-bottom:1rem;
border:0;border-top:1px solid rgba(0,0,0,0.1)}.list-unstyled{padding-left:0;list-style:none}.list-inline{padding-left:0;list-style:none}.list-inline-item{display:inline-block}.list-inline-item:not(:last-child){margin-right:.5rem}.img-fluid{max-width:100%;height:auto}
.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}

@media (min-width: 576px){.container{max-width:540px}}
@media (min-width: 768px){.container{max-width:720px}}
@media (min-width: 992px){.container{max-width:960px}}
@media (min-width: 1200px){.container{max-width:1215px}}
.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}
.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}
.no-gutters{margin-right:0;margin-left:0}
.no-gutters > .col,.no-gutters > [class*="col-"]{padding-right:0;padding-left:0}
.col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11,.col-12,.col,.col-auto,.col-sm-1,.col-sm-2,
.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm,.col-sm-auto,
.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,
.col-md,.col-md-auto,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,
.col-lg-11,.col-lg-12,.col-lg,.col-lg-auto,.col-xl-1,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,
.col-xl-9,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}
.col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}

@media (min-width: 576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;
flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;
flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.
col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-sm-first{-ms-flex-order:-1;order:-1}}

@media (min-width: 768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;
max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-md-first{-ms-flex-order:-1;order:-1}}

@media (min-width: 992px){.col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-lg-first{-ms-flex-order:-1;order:-1}}

@media (min-width: 1200px){.col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-xl-first{-ms-flex-order:-1;order:-1}}

.clearfix::after{display:block;clear:both;content:""}.d-none{display:none}.d-inline{display:inline}
.d-inline-block{display:inline-block}.d-block{display:block}.d-table{display:table}.d-table-row{display:table-row}
.d-table-cell{display:table-cell}.d-flex{display:-ms-flexbox;display:flex}
.d-inline-flex{display:-ms-inline-flexbox;display:inline-flex}

@media (min-width: 576px){.d-sm-none{display:none}.d-sm-inline{display:inline}.d-sm-inline-block{display:inline-block}
.d-sm-block{display:block}.d-sm-table{display:table}.d-sm-table-row{display:table-row}.d-sm-table-cell{display:table-cell}
.d-sm-flex{display:-ms-flexbox;display:flex}.d-sm-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 768px){.d-md-none{display:none}.d-md-inline{display:inline}.d-md-inline-block{display:inline-block}
.d-md-block{display:block}.d-md-table{display:table}.d-md-table-row{display:table-row}.d-md-table-cell{display:table-cell}
.d-md-flex{display:-ms-flexbox;display:flex}.d-md-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 992px){.d-lg-none{display:none}.d-lg-inline{display:inline}.d-lg-inline-block{display:inline-block}
.d-lg-block{display:block}.d-lg-table{display:table}.d-lg-table-row{display:table-row}.d-lg-table-cell{display:table-cell}
.d-lg-flex{display:-ms-flexbox;display:flex}.d-lg-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 1200px){.d-xl-none{display:none}.d-xl-inline{display:inline}.d-xl-inline-block{display:inline-block}
.d-xl-block{display:block}.d-xl-table{display:table}.d-xl-table-row{display:table-row}.d-xl-table-cell{display:table-cell}
.d-xl-flex{display:-ms-flexbox;display:flex}.d-xl-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}
.float-left{float:left}.float-right{float:right}.float-none{float:none}

@media (min-width: 576px){.float-sm-left{float:left}.float-sm-right{float:right}.float-sm-none{float:none}}

@media (min-width: 768px){.float-md-left{float:left}.float-md-right{float:right}.float-md-none{float:none}}

@media (min-width: 992px){.float-lg-left{float:left}.float-lg-right{float:right}.float-lg-none{float:none}}

@media (min-width: 1200px){.float-xl-left{float:left}.float-xl-right{float:right}.float-xl-none{float:none}}

.overflow-auto{overflow:auto}.overflow-hidden{overflow:hidden}.position-static{position:static}
.position-relative{position:relative}.position-absolute{position:absolute}.position-fixed{position:fixed}
.position-sticky{position:-webkit-sticky;position:sticky}.fixed-top{position:fixed;top:0;right:0;left:0;z-index:1030}
.fixed-bottom{position:fixed;right:0;bottom:0;left:0;z-index:1030}.w-25{width:25%}.w-50{width:50%}.w-75{width:75%}
.w-100{width:100%}.w-auto{width:auto}.h-25{height:25%}.h-50{height:50%}.h-75{height:75%}.h-100{height:100%}
.h-auto{height:auto}.mw-100{max-width:100%}.mh-100{max-height:100%}.min-vw-100{min-width:100vw}.min-vh-100{min-height:100vh}
.vw-100{width:100vw}.vh-100{height:100vh}.text-monospace{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}
.text-justify{text-align:justify}.text-wrap{white-space:normal}
.text-nowrap{white-space:nowrap}.text-truncate{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}
.text-left{text-align:left}.text-right{text-align:right}
.text-center{text-align:center}

@media (min-width: 576px){.text-sm-left{text-align:left}.text-sm-right{text-align:right}.text-sm-center{text-align:center}}

@media (min-width: 768px){.text-md-left{text-align:left}.text-md-right{text-align:right}.text-md-center{text-align:center}}

@media (min-width: 992px){.text-lg-left{text-align:left}.text-lg-right{text-align:right}.text-lg-center{text-align:center}}

@media (min-width: 1200px){.text-xl-left{text-align:left}.text-xl-right{text-align:right}.text-xl-center{text-align:center}}

.text-lowercase{text-transform:lowercase}.text-uppercase{text-transform:uppercase}.text-capitalize{text-transform:capitalize}
.font-weight-light{font-weight:300}.font-weight-lighter{font-weight:lighter}.font-weight-normal{font-weight:400}
.font-weight-bold{font-weight:700}.font-weight-bolder{font-weight:bolder}.font-italic{font-style:italic}
.text-white{color:#fff}




@font-face {
    font-family: 'OpenSansRegular';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansRegular.svg#OpenSansRegular') format('svg');
}
.OpenSansRegular{ font-family : 'OpenSansRegular'}
   .fs-sm-42px { font-size:42px;}


@font-face {
    font-family: 'OpenSansLight';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansLight.svg#OpenSansLight') format('svg');
}
.OpenSansLight{ font-family : 'OpenSansLight'}
   .fs-sm-20px { font-size:20px;}

@font-face {
    font-family: 'OpenSansExtraBold';
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot');
    src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.eot') format('embedded-opentype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff2') format('woff2'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.woff') format('woff'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.ttf') format('truetype'),
         url('<?php bloginfo('stylesheet_directory'); ?>/fonts/OpenSansExtraBold.svg#OpenSansExtraBold') format('svg');
}
.OpenSansExtraBold{ font-family : 'OpenSansExtraBold'}
   .fs-sm-62px { font-size:62px;}

 @font-face {
    font-family: 'Arialregular';
    src: url('fonts/Arialregular.eot');
    src: url('fonts/Arialregular.eot') format('embedded-opentype'),
         url('fonts/Arialregular.woff2') format('woff2'),
         url('fonts/Arialregular.woff') format('woff'),
         url('fonts/Arialregular.ttf') format('truetype'),
         url('fonts/Arialregular.svg#Arialregular') format('svg');
}
  .Arialregular{ font-family : 'Arialregular'}
   .fs-sm-24px { font-size:24px;}

.header{background-repeat: no-repeat;background-size: cover;position: relative;background-color: #0e1f51;}
.logo {padding: 4% 0 0 8%;}
section.second_position{padding: 1% 0 2% 0;}
section.second_position h1 {text-align:center;padding:3% 0 1% 0;font-size:42px;font-family : 'OpenSansRegular';text-transform: uppercase;color:#0e1f51;}
section.second_position p {text-align:center;padding:0% 0 3% 0;font-size:16px;font-family : 'OpenSansRegular';color:#0e1f51;}
section.last-part {background: #e6e8ed;padding: 3% 0 0 0;}
input#idName,input#idEmail,input#idPhone {padding: 1% 1%;margin: 1% 0;width: 55%;border-radius: 13px;border: 2px solid #a3acae;background: #e6e8ed;}
input#idSubmit {width: 69%;padding: 1% 1%;margin: 1% 0;border-radius: 13px;border: 2px solid #48d299;background: #48d299;color: white;background: #48d299;text-transform: uppercase;}
.what_do {padding: 7% 0 3% 0;}
input#idSubmit:hover {background: white;color: black;}
select {margin: 0 3% 0 5%;padding: 1.2% 0;width: 68%;}
input.user-valid.valid {margin: 0 1% 0 4%;width: 68%;padding: 1% 0;}
.form{width: 45%;text-align: center;}
input[type="text"] {margin: 0 2% 0 5%;width: 68%;padding: 1% 0%;}
h1.leverage{text-align: center;padding: 2% 0 0 0;color: #0e1f51;}
h2.your_accounts{text-align: center;color: #0e1f51;font-weight: 300;}
.block_line {border: 1px solid;text-align: center;border-radius: 10px;padding: 1% 0%;margin: 0 0 0 27px;}
h5.connect {background: #0e1f51;display: inline-block;color: #fff;padding: 2% 9%;font-size: 15px;
border-radius: 5px;}
.accounts {padding: 0% 0 5% 0;}
.social_img {padding: 3% 0 0 5%;}
section.footer {text-align: center;background: #0e1f51;color: white;padding: 1% 0;}
section.footer p {padding: 0% 20%;font-size:16px;}
section.footer h5 {font-size:20px;}
h5.connect a{color:white;text-decoration: none;}

@media screen and (max-width: 767px) {
.header{background-repeat: no-repeat;background-size: cover;position: relative;background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/mob-banner.png');height:600px;}
.logo{display:none;}
.mobile-logo {padding: 6% 0;text-align: left;}
section.second_position h1{font-size: 33px;}
.side-img{display:none;}
section.second_position {padding: 6% 0 6% 0;}
.form{text-align: center;width:100%;}
section.mobile-part {display: block;background:#e6e8ed;}
.sticky_button {position: fixed;bottom: 0px;margin-right: auto;margin-left: auto;width: 100%;}
a.enqire {width: 100%;float: left;text-align: center;background: #48d299;padding: 10px 0px;color: #fff;border-right: 1px solid #48d299;text-transform: uppercase;text-decoration: none;font-family: RalewayRegular;}
.click p{display:none;}
input#idName, input#idEmail, input#idPhone{width: 75%;margin: 3% 0;
padding: 1% 3%;}
input#idSubmit{width: 75%;margin: 3% 0;padding: 1% 3%;}
.mob-logo {border-right: 2px solid white;padding: 12px 9px 0 0;}
section.second_position p{font-size: 12px;}
.social_img{margin:6% 0% 0% 9%;width:77%;}
.block_line{margin: 0 0 11px 0px;padding: 11% 0 0 0;}
input[type="text"]{margin: 0;}
select{margin: 0;}
h2.your_accounts{font-size: 20px;}
h1.leverage{font-size: 20px;}
}

@media screen and (min-width: 768px) {
.mobile-logo{display:none;}
.side-img {padding: 0 0 0 3%;}
section.mobile-part {display: none;}
.sticky_button {display: none;}
.socialbeat-logo p{color: white;padding: 8px 0 0 0}
}
:root {--color-primary: #005AF0;--color-text-light: #fff;--color-text-dark: #000;--color-bg-light: #FAFAFC;--space-1: .5rem;  --space-2: 1rem; --space-3: 1.5rem; --space-4: 2rem;   --space-5: 3rem;}
.stepper.simple .content {height: 220px;display: flex;align-items: center;justify-content: center;}


amp-selector.poll [option][selected] {outline: none;}
amp-selector.poll [option] {display: flex;align-items: center;}
amp-selector.poll [option]:before {transition: background 0.25s ease-in-out;content: "";display: inline-block;width: var(--space-2);height: var(--space-2);margin: var(--space-1);border-radius: 100%;border: solid 1px var(--color-primary);font-size: 14px;line-height: var(--space-2);}
amp-selector.poll [option][selected]:before {text-align: center;content: "✓";color: #fff;background: var(--color-primary);}
amp-selector.poll [option][selected]:focus {outline: -webkit-focus-ring-color auto 5px;}
h2.preview-only {margin: var(--space-2);margin-top: var(--space-3);margin-bottom: 0;}
h2.preview-only + p {margin: var(--space-1) var(--space-2);}
</style>
</head>
<body>
<!-- Google Tag Manager -->
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-KBR6KWL&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>
<section class="header">
	<div class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12 logo">
			<div class="row">
				<div class="col-12 col-md-2 col-sm-2 desktop-logo">
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/mobile-logo.png" alt="mobile-logo.png" width="200" height="56" >
					</amp-img>
				</div>
				<div class="col-12 col-md-6 col-sm-6 col-lg-6"></div>
				<div class="col-12 col-md-4 col-sm-4 col-lg-4 socialbeat-logo">
				<p>A product of<a href="https://www.socialbeat.in/"><amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/SB logo for Ads Quotient.svg" alt="Ads-Quotient-Microsite.png" width="190" height="61"  style="vertical-align: middle;margin: 0 0 0 2%;"></a></p>
				</amp-img>
				</div>

			</div>
		</div>
		<div class="mobile-logo">
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 logos">
				<div class="row">
					<div class="col-6 col-md-2 col-sm-2 mob-logo">
						<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/Ads Quotient logo.svg" alt="Ads Quotient logo.svg" width="200" height="56" layout="responsive">
						</amp-img>
					</div>

					<div class="col-6 col-md-2 col-sm-2 col-lg-2 socialbeat-logo">
					<p style="color:white">A product of<a href="https://www.socialbeat.in/"><amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/SB logo for Ads Quotient.svg" alt="Ads-Quotient-Microsite.png" width="190" height="61" layout="responsive" style="vertical-align: middle;margin: 0 0 0 2%;"></a>
					</amp-img></p>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>


<section class="last-part">
		<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			<div class="container">
				<div class="stepper simple">
						<h1 class="leverage">LEVERAGE THE POWER OF YOUR DATA</h1>
						<div class="content">
							<form action="//www.adsquotient.com/sendtogravity.php" method="GET" target="_top" class="form">
								<p class="website">
								Website: <input type = "text" name = "Enter the URL" />
								</p>
								<p class="category">
								Category:
								<select>
								  <option value="">1</option>
								  <option value="">2</option>
								  <option value="">3</option>
								  <option value="">4</option>
								</select>
								</p>
								<p class="industry">
								Industry:
								<select>
								  <option value="">software</option>
								  <option value="">Marketing</option>
								</select>
							   </p>
							</form>
						</div>
						<div class="accounts">
							<h2 class="your_accounts">Connect any one of your accounts</h2>
								<div class="col-12 col-md-12 col-sm-12 col-lg-12 social_img">
										<div class="row">
												<div class="col-12 col-md-2 col-sm-2 col-lg-2"></div>
												<div class="col-12 col-md-2 col-sm-2 col-lg-2 block_line">
														<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/google-ads.png" alt="google-ads.png" width="78" height="78" ></amp-img><br><br>
															<p>Google AdWords</p>
															<h5 class="connect"><a href="#">CONNECT</a></h5>
												</div>

												<div class="col-12 col-md-2 col-sm-2 col-lg-2 block_line">
														<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/fb.png" alt="fb.png" width="40" height="78" ></amp-img><br><br>
															<p>Facebook</p>
															<h5 class="connect"><a href="#">CONNECT</a></h5>
												</div>

												<div class="col-12 col-md-2 col-sm-2 col-lg-2 block_line">
														<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/google-analtics.png" alt="google-analtics.png" width="78" height="78" ></amp-img><br><br>
															<p>Google Analytics</p>
															<h5 class="connect"><a href="#">CONNECT</a></h5>
												</div>
										</div>
								</div>
						</div>
						</div>
				</div>
			</div>
		</div>
</section>
<section class="footer">
		<h5>Your data is 100% secure with us </h5>
		<p>Don't worry. Ads quotient is a product of Socialbeat and we will not use or access your account for any other purposes, except for calculating the ads score. We do not share any information with third parties.</p>
</section>
</body>
</html>
