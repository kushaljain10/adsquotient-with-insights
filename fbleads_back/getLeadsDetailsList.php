<?php 
require_once "config.php";
require_once "dbConfig.php";
$conn = new mysqli($servername,$username,$password,$dbname);
/*Get project name list from database*/
$strProjectList = doGetAllProjectNameFromDb($conn);

header('Content-Type: application/json');
$formDetails = "";
if(isset($_REQUEST["form-id"]) && !empty($_REQUEST["form-id"])){
	//printArray($_SESSION);
	/*Get page Id and get page token from Page id to fetch list of forms*/
	$strFormId		=	$_REQUEST["form-id"];
	$access_token 	=	"";
	//printArray($_SESSION['userData']['accounts'][4]);
	$access_token 	=	$_SESSION['userData']['accounts'][4]['access_token'];
	/*Get page access token using page id*/
	try {
	  // Returns a `FacebookFacebookResponse` object
	  $formsResposnseObj = $FB->get(
	    '/'.$strFormId.'/?fields=leads_count,leads{created_time,field_data}',
        $access_token
	  );
	} catch(FacebookExceptionsFacebookResponseException $e) {
	  echo 'Graph returned an error: ' . $e->getMessage();
	  exit;
	} catch(FacebookExceptionsFacebookSDKException $e) {
	  echo 'Facebook SDK returned an error: ' . $e->getMessage();
	  exit;
	} catch(Exception $e){
		echo 'Assetz returned an error for form details: ' . $e->getMessage();
	  	exit;
	}
	/*Get FB Page access token */
	$formDetails = ($formsResposnseObj->getDecodedBody());
	
}else{
	echo "Invalid input";
}

/*Handle form lead details and convert the data format to push CRM */
if(!empty($formDetails)){
	//printArray($formDetails);
	$strLeadCount = !empty($formDetails["leads_count"]) ? $formDetails["leads_count"] : 0;
	//$strLeadS = 
	foreach ($formDetails["leads"]["data"] as $leads) {
		//printArray($leads);
		$leadId = $leadUserName = $leadUserEmail = $leadUserPhone = $leadCreatedTime = "";
		/*Creating array wilth leads details*/
		$leadId = $leads["id"];
		$leadCreatedTime = date("Y-m-d H:is",strtotime($leads["created_time"]));
		foreach ($leads['field_data'] as $userDetails) {
			if($userDetails['name']=="full_name"){
				$leadUserName = $userDetails['values'][0];
			}
			if($userDetails['name']=="email"){
				$leadUserEmail = $userDetails['values'][0];
			}
			if($userDetails['name']=="phone_number"){
				$leadUserPhone = $userDetails['values'][0];
			}
		}
		$aryLeads[] = array("id"=>$leadId, "name"=>$leadUserName,"email"=>$leadUserEmail,"phone_number"=>$leadUserPhone,"time"=>$leadCreatedTime);
	}
	//printArray($aryLeads);
}
/*Convert Leads details array to json formate to send*/
$strFirstName = $strPhone = $strEmail = $strSource = $strSubSource = $strProspectStage = $strProjectName = $strEnquiryType = "";
/*Predefine values*/
$strSource = "Social Media";
$strSubSource = "FB";
$strProspectStage = "Enquiry";
$strEnquiryType = "Digital";

$data_string = $result = "";

/*Source*/
$Source["Attribute"] = 	"Source";
$Source["Value"] =	"Social Media";
/*SubSource*/
$SubSource["Attribute"] = 	"mx_Sub_Source";
$SubSource["Value"] =	"FB";
/*ProspectStage*/
/*$ProspectStage["Attribute"] = 	"ProspectStage";
$ProspectStage["Value"] =	"Enquiry";*/
/*Enquiry type*/
$EnquiryType["Attribute"] = 	"mx_Enquiry_Type";
$EnquiryType["Value"] =	"Digital";

if(!empty($aryLeads)){
	$finalLeadData = $resultAry = array();
	foreach ($aryLeads as $leadsDetails) {
		/*Name*/
		$name["Attribute"] = 	"FirstName";
		$name["Value"] = 	$leadsDetails["name"];
		/*Email*/
		$email["Attribute"] = 	"EmailAddress";
		$email["Value"] = 	$leadsDetails["email"];
		/*Phone number*/
		$phone["Attribute"] = 	"Phone";
		$phone["Value"] = 	$leadsDetails["phone_number"];

		/*Project name*/
		$projectName["Attribute"] = 	"mx_Project_Names";
		$projectName["Value"] = 	"July - 38 & Banyan-Price Change";
		$resultAry = array($name, $phone, $email, $Source, $SubSource, $EnquiryType,$projectName);
		array_push($finalLeadData,$resultAry);
	}
	$finalEncodeLeadData = json_encode($finalLeadData);
}
//echo ($data_string);
if(!empty($finalEncodeLeadData)){
	/*send leads details to Leads square*/
	$accessKey = 'u$r2cf9c54eb2b672ea3ff620aa867c8ff6';
	$secretKey = '7edce9e8ff092e6c1a35374a622dc79013def4c7';
	$api_url_base = 'https://api.leadsquared.com/v2/LeadManagement.svc';
	//$url = $api_url_base . '/Lead/Bulk/Create?accessKey=' . $accessKey . '&secretKey=' . $secretKey;
	try
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $finalEncodeLeadData);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
			'Content-Length:'.strlen($finalEncodeLeadData)
			));
		$json_response = curl_exec($curl);
		echo ($json_response);
		curl_close($curl);
	} catch (Exception $ex) { 
		echo 'Curl error: ' . $e->getMessage();
		curl_close($curl);
	}
	exit;
}
?>