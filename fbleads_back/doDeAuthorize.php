<?php
require_once "config.php";
require_once "dbConfig.php";
$conn = new mysqli($servername,$username,$password,$dbname);

/*Check inputs*/
$formId = !empty($_POST['formId']) ? $_POST['formId'] : "";
$projectId = !empty($_POST['projectId']) ? $_POST['projectId'] : "";
$action = !empty($_POST['action']) ? $_POST['action'] : "";

$status = false;
// printArray($formId." <br>Project: ".$projectId." Action:".$action );
/*Check and validate input values and insert the new values*/
if(!empty($formId) && !empty($projectId) && !empty($action)){
	$status = doDeAuthorize($conn,$formId,$projectId);
	echo json_encode($status);
}
 ?>
