<?php
ini_set('max_execution_time', 300);
// phpinfo(); exit;
require_once "config.php";
error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('Asia/Kolkata');

/* Db Config */
$servername = "localhost";
$username = "assetyou_assetz";
$password = "P7~c0y2Hr7Ee";
$dbname="assetyou_lead_to_crm";

/*Convert Leads details array to json formate to send*/
/*Predefine values*/
/*Source*/
$Source["Attribute"] =  "Source";
$Source["Value"] =  "Social Media";
/*SubSource*/
$SubSource["Attribute"] =   "mx_Sub_Source";
$SubSource["Value"] =   "FB";
/*ProspectStage*/
/*$ProspectStage["Attribute"] =     "ProspectStage";
$ProspectStage["Value"] =   "Enquiry";*/
/*Enquiry type*/
$EnquiryType["Attribute"] =     "mx_Enquiry_Type";
$EnquiryType["Value"] = "Digital";

$conn = mysqli_connect($servername,$username,$password,$dbname);
$fb_page_token = $fb_page_id = $proj_name = $form_id = "";
// Get list of sync projects 
$syncSql = "SELECT page.`fb_page_id`, page.`fb_page_token`, proj.`name`, lead.`form_id` FROM `leads_details` AS lead, `fb_page_details` AS page, `project_List` AS proj WHERE lead.`fb_page_id` = page.`id` AND lead.`project_id` = proj.`id` ";
$result = $conn->query($syncSql);
if ($result->num_rows > 0){
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $time_start = microtime(true);
        $fb_page_token = $row['fb_page_token'];
        $fb_page_id = $row['fb_page_id'];
        $proj_name = $row['name'];
        $form_id = $row['form_id'];

        /*Get leads from form id and page id*/
        try {
          // Returns a `FacebookFacebookResponse` object
          /*$formsResposnseObj = $FB->get(
            '/'.$form_id.'/?fields=leads_count,leads{created_time,field_data}',
            $fb_page_token
          );*/
          $formsResposnseObj = $FB->get(
            '/'.$form_id.'/?fields=leads',
            $fb_page_token
          );
        } catch(FacebookExceptionsFacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(FacebookExceptionsFacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        } catch(Exception $e){
            echo 'Assetz returned an error for form details: ' . $e->getMessage();
            exit;
        }
        /*Get FB Page access token */
        $formDetails = ($formsResposnseObj->getDecodedBody());
        
        if(!empty($formDetails)){
            $strLeadCount = !empty($formDetails["leads_count"]) ? $formDetails["leads_count"] : 0;
            $aryLeads = $userDetails = array();
            foreach ($formDetails["leads"]["data"] as $key=>$leads) {
                $leadUserName = $leadUserEmail = $leadUserPhone = "";
                /*Creating array wilth leads details*/
                if($key < 20){
                    // echo "<br>Count of aryLeads array:".count($aryLeads["name"]);
                    foreach ($leads['field_data'] as $userDetails) {
                        // printArray($userDetails);
                        if($userDetails['name']=="full_name"){
                            $leadUserName = $userDetails['values'][0];
                        }
                        if($userDetails['name']=="email"){
                            $leadUserEmail = $userDetails['values'][0];
                        }
                        if($userDetails['name']=="phone_number"){
                            $leadUserPhone = $userDetails['values'][0];
                        }
                    }
                    // echo $k."<br>";
                    $aryLeads[] = array("name"=>$leadUserName,"email"=>$leadUserEmail,"phone_number"=>$leadUserPhone);
                }
            }
            $finalLeadData = $resultAry = array();
            foreach ($aryLeads as $leadsDetails) {
                /*Name*/
                $name["Attribute"] =    "FirstName";
                $name["Value"] =    $leadsDetails["name"];
                /*Email*/
                $email["Attribute"] =   "EmailAddress";
                $email["Value"] =   $leadsDetails["email"];
                /*Phone number*/
                $phone["Attribute"] =   "Phone";
				$mobile_number=str_replace("+91","+91-",$leadsDetails["phone_number"]);
                $phone["Value"] = $mobile_number;
                // printArray($phone["Value"]);
                /*Project name*/
                $projectName["Attribute"]   =   "mx_Project_Names";
                $projectName["Value"]       =   $proj_name;
                
                // printArray($projectName["projectName"]);
                $resultAry = array($name, $phone, $email, $Source, $SubSource, $EnquiryType,$projectName);
                array_push($finalLeadData,$resultAry);
            }
            // printArray($finalLeadData);
            $finalEncodeLeadData = json_encode($finalLeadData);
            // printArray($finalEncodeLeadData);
            $strLeadPushResult = doPushLeadData($finalEncodeLeadData);
            $leadPushResult =  (json_decode($strLeadPushResult, true) );
            // printArray($leadPushResult);
            $time_end = microtime(true);
            $time = $time_end - $time_start ;
            echo 'Execution time : ' . $time . ' seconds<br>' ;
        }
        sleep(3);
    }    
}

function doPushLeadData($finalEncodeLeadData){
    $response = "";
    if(!empty($finalEncodeLeadData)){
        /*send leads details to Leads square*/
        $accessKey = 'u$r2cf9c54eb2b672ea3ff620aa867c8ff6';
        $secretKey = '7edce9e8ff092e6c1a35374a622dc79013def4c7';
        $api_url_base = 'https://api.leadsquared.com/v2/LeadManagement.svc';
        $url = $api_url_base . '/Lead/Bulk/CreateOrUpdate?accessKey=' . $accessKey . '&secretKey=' . $secretKey;
        try
        {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $finalEncodeLeadData);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type:application/json',
                'Content-Length:'.strlen($finalEncodeLeadData)
                ));
            $json_response = curl_exec($curl);
            $response =  ($json_response);
            curl_close($curl);
            return $response;
        } catch (Exception $ex) { 
            echo 'Curl error: ' . $e->getMessage();
            curl_close($curl);
        }
    }
}












function printArray($str){
    echo "<pre>";
    print_r($str);
    echo "</pre>";
}