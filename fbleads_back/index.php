<?php
if(!session_id()) {
    session_start();
}
require_once "dbConfig.php";
$conn = new mysqli($servername,$username,$password,$dbname);
	if (!isset($_SESSION['access_token'])) {
		header('Location: login.php');
		exit();
	}else{ 
		/*echo "<pre>";
		print_r($_SESSION);
		echo "</pre>";*/
		$fbUserId = isset($_SESSION["userData"]) ? $_SESSION["userData"]["id"] : "";
		if(!empty($fbUserId)){
			// Add or update fb profile details
			$updateUserInfo = doUpdateUserInfo($conn,$_SESSION);
		}
		?>
	<?php /*?><a href="logout.php">logout</a>	<?php */?>
	<?php }
	/*echo "<pre>";
	print_r($_SESSION);
	echo "</pre>"; */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Profile</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
	<div class="container" style="margin-top: 20px">
    <div class="col-md-12 col-sm-12 col-lg-12" >
    <div class="col-md-3 col-lg-3" >
            <img src="assetz-logo.png" width="200">
            <br/><br/>
            </div>
            </div>
		<div class="row justify-content-center">
			<div class="col-md-3">
				<img style="border-radius: 50%;" src="<?php echo $_SESSION['userData']['picture']['url'] ?>">
			</div>

			<div class="col-md-9">
				<table class="table table-hover table-bordered">
					<tbody>
						<tr>
							<td>ID</td>
							<td><?php echo $_SESSION['userData']['id'] ?></td>
						</tr>
						<tr>
							<td>Name</td>
							<td><?php echo $_SESSION['userData']['name'] ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?php echo $_SESSION['userData']['email'] ?></td>
						</tr>
                        </tbody>
						</table>
			</div>
            
		</div>
        <div>
        <?php /*?><h2>List of your Facebook Pages        </h2><?php */?>
        <h4>Choose any one from the list to get leads</h4>
        </div>
        <div class="row">
        <table class="table table-hover table-bordered">
					<tbody>
						
                        
            	<?php if(!empty($_SESSION['userData']['accounts'])){
						foreach($_SESSION['userData']['accounts'] as $acc){	
						?>
                             
                                <tr>
                                <td>
                                    <img src="<?php echo $acc['picture']['url']; ?>" height="25" alt="<?php echo $acc["name"]; ?>"></td>
<td><a href="getformslist.php?page_id=<?php echo $acc["id"]; ?>" ><?php echo $acc["name"]; ?></a> </td>                   
                                    <td><?php echo $acc["fan_count"]; ?></td>
                             </tr>
						
                        <?php } } ?>
                        
                      
                        </tbody>
						</table>
            </div>
	</div>
</body>
</html>
<script type="text/javascript">
function doUpdateAccessToken(accessToken){
	alert(accessToken);
}
</script>
